package com.Chirag.illumeclinica.listeners;

public interface ClickListenerForDialogBox {

    public void dialogPositiveButton();

    public void dialogNegativeButton();

}
