package com.Chirag.illumeclinica.listeners;

import android.view.View;

public interface ItemClickListener {

    void itemClickListener(int position, View view);

    void itemMedicineClickListener(int medicinePosition, int intakePosition,int daysPosition);

    void itemClickOfDropDown(int type, String title, boolean isChecked);

    void itemMedicince(String medicine, String intake, int daysPosition);

    void selectedItems(String selectedItems);
}
