package com.Chirag.illumeclinica.listeners;

import android.view.View;

public interface ItemRemoveClickListener {
    void itemRemoveClickListener(int position, View view, int type);
}
