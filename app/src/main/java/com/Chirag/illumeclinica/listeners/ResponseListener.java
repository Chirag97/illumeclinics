package com.Chirag.illumeclinica.listeners;

import com.android.volley.error.VolleyError;


/**
 * Response listener for API.
 *
 * @param <T>
 */
public interface ResponseListener<T> {
    void getResponse(String tag, T response);

    void getError(String tag, VolleyError error, boolean showToast);
}
