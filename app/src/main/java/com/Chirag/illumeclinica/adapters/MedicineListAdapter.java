package com.Chirag.illumeclinica.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.Tessting.CustomTextView;
import com.Chirag.illumeclinica.models.VisitDetails;

import java.util.List;

public class MedicineListAdapter extends RecyclerView.Adapter<MedicineListAdapter.ViewHolder> {

    private Activity context;
    private List<VisitDetails.DataBean.MedicineListBean> medicineList;

    public MedicineListAdapter(Activity context, List<VisitDetails.DataBean.MedicineListBean> medicineList) {
        this.context = context;
        this.medicineList = medicineList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.medicine_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        VisitDetails.DataBean.MedicineListBean medicineIds = medicineList.get(position);
        viewHolder.intakeTime.setText(medicineIds.getIntake_name());
        viewHolder.medicineName.setText(medicineIds.getMedecine_name());
        if (medicineIds.getDuration().equals("1"))
            viewHolder.days.setText(medicineIds.getDuration() + " Day");
        else viewHolder.days.setText(medicineIds.getDuration() + " Days");
    }

    @Override
    public int getItemCount() {
        if (medicineList != null && medicineList.size() > 0)
            return medicineList.size();
        else return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView medicineName, intakeTime, days;
        ImageView deletion;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            medicineName = itemView.findViewById(R.id.medicineName);
            intakeTime = itemView.findViewById(R.id.intakeTime);
            days = itemView.findViewById(R.id.days);
            deletion = itemView.findViewById(R.id.delete);
            deletion.setVisibility(View.INVISIBLE);
        }
    }
}

