package com.Chirag.illumeclinica.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.VisitDetails;

import java.util.List;

public class DiagnoseVisitDetailsAdapter extends RecyclerView.Adapter<DiagnoseVisitDetailsAdapter.ViewHolder> {

    private Activity context;
    private List<VisitDetails.DataBean.TestListBean> testList;

    public DiagnoseVisitDetailsAdapter(Activity context, List<VisitDetails.DataBean.TestListBean> testList) {
        this.context = context;
        this.testList = testList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.diagnose_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        VisitDetails.DataBean.TestListBean listBean = testList.get(i);
        viewHolder.diagnoseName.setText(listBean.getTest_name());
    }

    @Override
    public int getItemCount() {
        if (testList != null && testList.size() > 0)
            return testList.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView diagnoseName;
        ImageView delete;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            diagnoseName = itemView.findViewById(R.id.diagnoseName);
            delete = itemView.findViewById(R.id.delete);
            delete.setVisibility(View.GONE);

        }
    }


}


