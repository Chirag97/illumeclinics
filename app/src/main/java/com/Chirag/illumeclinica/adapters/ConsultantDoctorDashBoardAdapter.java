package com.Chirag.illumeclinica.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.InvoiceActivity;
import com.Chirag.illumeclinica.activities.VisitDetailsActivity;
import com.Chirag.illumeclinica.models.ConsvisitList;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;

import java.util.List;

public class ConsultantDoctorDashBoardAdapter extends RecyclerView.Adapter<ConsultantDoctorDashBoardAdapter.Viewholder> {
    private Context context;
    private List<ConsvisitList> list;

    public ConsultantDoctorDashBoardAdapter(Context context, List<ConsvisitList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.dashboard_items, viewGroup, false);
        return new Viewholder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final Viewholder viewholder, int i) {
        final ConsvisitList item = list.get(i);

        viewholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (item.getVisitStatus() != null)
                    if (item.getVisitStatus().equalsIgnoreCase("2")) {
                        Intent intent = new Intent(context, VisitDetailsActivity.class);
                        intent.putExtra("visit_data", item);
                        intent.putExtra("consultant", item.getConsultantDoctor());
                        context.startActivity(intent);
                    } else if (item.getVisitStatus().equalsIgnoreCase("3")) {
                        Intent intent = new Intent(context, VisitDetailsActivity.class);
                        intent.putExtra("visit_data", item);
                        intent.putExtra("consultant", item.getConsultantDoctor());
                        context.startActivity(intent);
                    } else if (item.getVisitStatus().equalsIgnoreCase("4")) {
                        Intent intent = new Intent(context, InvoiceActivity.class);
                        intent.putExtra("visit_id", item.getVisitId());
                        context.startActivity(intent);
                    }


            }
        });

        if (item.getVisitStatus() != null)
            if (item.getVisitStatus().equalsIgnoreCase("1")) {
                viewholder.invoice.setText("Patient Problem Pending");

            } else if (item.getVisitStatus().equalsIgnoreCase("2")) {
                viewholder.invoice.setText("Prescription Pending");
                viewholder.invoice.setTextColor(Color.RED);
                viewholder.view.setBackgroundColor(Color.RED);

            } else if (item.getVisitStatus().equalsIgnoreCase("3")) {
                viewholder.invoice.setText("Prescription Generated");
                viewholder.view.setBackgroundColor(context.getResources().getColor(R.color.orange123));
                viewholder.invoice.setTextColor(context.getResources().getColor(R.color.orange123));
            } else if (item.getVisitStatus().equalsIgnoreCase("4")) {
                viewholder.invoice.setText("Invoice Generated");
                viewholder.view.setBackgroundColor(context.getResources().getColor(R.color.green));
                viewholder.invoice.setTextColor(context.getResources().getColor(R.color.green));
            }
        if (item.getGender() != null)
            if(item.getGender().equalsIgnoreCase("Male")){
                viewholder.iv.setImageResource(R.drawable.mal);
            }
            else {
                viewholder.iv.setImageResource(R.drawable.femal);
            }

        viewholder.id.setText(item.getPatientId());
        String pateint_capital = item.getPatientName();
        viewholder.patient_name.setText(ConstantClass.toTitleCase(pateint_capital));
        viewholder.date.setText(AndroidAppUtils.dateTimeFormat(item.getInvoiceDateTime()));
        viewholder.problemname.setText(item.getProblemName());
        viewholder.symptomname.setText(item.getSymptom());
        viewholder.visit_id.setText(item.getVisitId());
        viewholder.consultantdctorname.setText(item.getConsultantDoctor());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        View view;
        ImageView iv;
        TextView visit_id, patient_id, id, invoice, patient_name, date, time, problem, problemname, symptom, symptomname, consultantdoctor, consultantdctorname;

        private Viewholder(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.gender_image);
            view=itemView.findViewById(R.id.view);
            visit_id=itemView.findViewById(R.id.visit_id);
            patient_id = itemView.findViewById(R.id.pat);
            id = itemView.findViewById(R.id.id);
            invoice = itemView.findViewById(R.id.invoice_status);
            patient_name = itemView.findViewById(R.id.name);
            date = itemView.findViewById(R.id.dateandtime);
            time = itemView.findViewById(R.id.time);
            problem = itemView.findViewById(R.id.problem);
            problemname = itemView.findViewById(R.id.problems);
            symptom = itemView.findViewById(R.id.symptom);
            symptomname = itemView.findViewById(R.id.symptoms);
            consultantdoctor = itemView.findViewById(R.id.consult);
            consultantdctorname = itemView.findViewById(R.id.consultants);

        }
    }


}
