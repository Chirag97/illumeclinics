package com.Chirag.illumeclinica.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.InvoiceActivity;
import com.Chirag.illumeclinica.activities.VisitDetailsActivity;
import com.Chirag.illumeclinica.models.DoctorVisitList;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;

import java.util.List;

public class DoctorDashboardAdapter extends RecyclerView.Adapter<DoctorDashboardAdapter.ViewHolder> {

    private Context context;
    private List<DoctorVisitList> list;


    public DoctorDashboardAdapter(Context context, List<DoctorVisitList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.main_doctor_dashboard_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final DoctorVisitList doctorVisitList = list.get(i);
        viewHolder.id.setText(doctorVisitList.getPatientId());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (doctorVisitList.getInvoiceStatus() != null)
                    if (doctorVisitList.getInvoiceStatus().equalsIgnoreCase("2")) {
                        Intent intent = new Intent(context, VisitDetailsActivity.class);
                        intent.putExtra("visit_data", doctorVisitList);
                        intent.putExtra("consultant", doctorVisitList.getConsultantDoctor());
                        context.startActivity(intent);
                    } else if (doctorVisitList.getInvoiceStatus().equalsIgnoreCase("3")) {
                        Intent intent = new Intent(context, VisitDetailsActivity.class);
                        intent.putExtra("visit_data", doctorVisitList);
                        intent.putExtra("consultant", doctorVisitList.getConsultantDoctor());
                        context.startActivity(intent);
                    } else if (doctorVisitList.getInvoiceStatus().equalsIgnoreCase("4")) {
                        Intent intent = new Intent(context, InvoiceActivity.class);
                        intent.putExtra("visit_id", doctorVisitList.getVisitId());
                        context.startActivity(intent);
                    }
            }
        });
        if (doctorVisitList.getInvoiceStatus() != null)
            if (doctorVisitList.getInvoiceStatus().equalsIgnoreCase("1")) {
                viewHolder.invoice.setText("Patient Problem Pending");
            } else if (doctorVisitList.getInvoiceStatus().equalsIgnoreCase("2")) {
                viewHolder.invoice.setText("Prescription Pending");
                viewHolder.invoice.setTextColor(Color.RED);
                viewHolder.view.setBackgroundColor(Color.RED);
            } else if (doctorVisitList.getInvoiceStatus().equalsIgnoreCase("3")) {
                viewHolder.invoice.setText("Prescription Generated");
                viewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.orange123));
                viewHolder.invoice.setTextColor(context.getResources().getColor(R.color.orange123));
            } else if (doctorVisitList.getInvoiceStatus().equalsIgnoreCase("4")) {
                viewHolder.invoice.setText("Invoice Generated");
                viewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.green));
                viewHolder.invoice.setTextColor(context.getResources().getColor(R.color.green));
            }

        if (doctorVisitList.getGender() != null)
            if (doctorVisitList.getGender().equalsIgnoreCase("Male")) {
                viewHolder.iv.setImageResource(R.drawable.mal);
            } else {
                viewHolder.iv.setImageResource(R.drawable.femal);
            }

        String name = doctorVisitList.getPatientName();
        viewHolder.patient_name.setText(ConstantClass.toTitleCase(name));
        viewHolder.date.setText(AndroidAppUtils.dateTimeFormat(doctorVisitList.getInvoiceDateTime()));
        viewHolder.problemname.setText(doctorVisitList.getProblemName());
        viewHolder.symptomname.setText(doctorVisitList.getSymptom());
        viewHolder.consultantdctorname.setText(doctorVisitList.getConsultantDoctor());
        viewHolder.visit_id.setText(doctorVisitList.getVisitId());
        viewHolder.hospitalName.setText(doctorVisitList.getClinic_hospital_name());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView visit_id, patient_id, id, invoice, patient_name, date, time, problem, problemname, symptom, symptomname, consultantdoctor, consultantdctorname,hospital,hospitalName;
        ImageView iv;
        View view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.gender_image);
            view = itemView.findViewById(R.id.view);
            visit_id = itemView.findViewById(R.id.visit_id);
            patient_id = itemView.findViewById(R.id.pat);
            id = itemView.findViewById(R.id.id);
            invoice = itemView.findViewById(R.id.invoice_status);
            patient_name = itemView.findViewById(R.id.name);
            date = itemView.findViewById(R.id.dateandtime);
            time = itemView.findViewById(R.id.time);
            problem = itemView.findViewById(R.id.problem);
            problemname = itemView.findViewById(R.id.problems);
            symptom = itemView.findViewById(R.id.symptom);
            symptomname = itemView.findViewById(R.id.symptoms);
            consultantdoctor = itemView.findViewById(R.id.consult);
            consultantdctorname = itemView.findViewById(R.id.consultants);
            hospital=itemView.findViewById(R.id.hospital);
            hospitalName=itemView.findViewById(R.id.hospitalName);

        }
    }
}
