package com.Chirag.illumeclinica.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.DraweListModel;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;

import java.util.List;


public class DrawerListAdapter extends RecyclerView.Adapter<DrawerListAdapter.MyViewHolder> {

    private List<DraweListModel> drawerListItems;
    private AdapterClickListener adapterClickListener;
    private Context context;
    private String TAG = DrawerListAdapter.class.getSimpleName();


    public DrawerListAdapter(Context context, List<DraweListModel> drawerListItems, AdapterClickListener adapterClickListener) {
        this.context = context;
        this.drawerListItems = drawerListItems;
        this.adapterClickListener = adapterClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drawer_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DraweListModel draweListModel = drawerListItems.get(position);
        holder.itemText.setText(draweListModel.getItemName());
        if (draweListModel.isSelected()) {
            holder.itemText.setSelected(true);
            holder.itemText.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        } else {
            holder.itemText.setSelected(false);
            holder.itemText.setTextColor(context.getResources().getColor(R.color.gray));
        }
    }

    @Override
    public int getItemCount() {
        return drawerListItems.size();
    }

    /**
     * interface to handle click listener in respective fragment of adapter
     */
    public interface AdapterClickListener {
        void itemClicked(View view, int position);
    }

    /**
     * view holder class for this Adapter
     */
    class MyViewHolder extends RecyclerView.ViewHolder/* implements View.OnClickListener */ {
        private TextView itemText;

        MyViewHolder(View view) {
            super(view);
            itemText = view.findViewById(R.id.drawer_item_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    AndroidAppUtils.showLog(TAG, "cilcked item is:: " + v.getTag());
                    if (adapterClickListener != null) {
                        adapterClickListener.itemClicked(v, position);
                        if (drawerListItems.size() - 1 != position) {
                            for (int i = 0; i < drawerListItems.size(); i++) {
                                drawerListItems.get(i).setSelected(false);
                            }
                            drawerListItems.get(position).setSelected(true);
                            notifyDataSetChanged();
                        }
                    }
                }
            });
        }

    }
}