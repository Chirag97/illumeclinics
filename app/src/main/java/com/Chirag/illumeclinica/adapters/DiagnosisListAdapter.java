package com.Chirag.illumeclinica.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.Tessting.CustomTextView;
import com.Chirag.illumeclinica.listeners.ItemClickListener;
import com.Chirag.illumeclinica.models.MedicineModel;

import java.util.List;

public class DiagnosisListAdapter extends RecyclerView.Adapter<DiagnosisListAdapter.ViewHolder> {

    private Activity context;
    private List<MedicineModel.DataBean.TestListBean> testList;
    private ItemClickListener itemClickListener;

    public DiagnosisListAdapter(Activity context, List<MedicineModel.DataBean.TestListBean> testList, ItemClickListener itemClickListener) {
        this.context = context;
        this.testList = testList;
        this.itemClickListener = itemClickListener;
    }

    public void RefreshList(List<MedicineModel.DataBean.TestListBean> testList) {
        this.testList = testList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.diagnose_select_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        MedicineModel.DataBean.TestListBean listBean = testList.get(i);
        viewHolder.diagnoseName.setText(listBean.getTest_name());

    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView diagnoseName;
        RelativeLayout root;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            diagnoseName = itemView.findViewById(R.id.diagnoseName);
            root = itemView.findViewById(R.id.root);
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null)
                        itemClickListener.itemClickListener(getAdapterPosition(), itemView);
                }
            });

        }
    }
}


