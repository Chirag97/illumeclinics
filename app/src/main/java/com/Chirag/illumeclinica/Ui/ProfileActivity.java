package com.Chirag.illumeclinica.Ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.Chirag.illumeclinica.Adapter.ProfileAdapter;
import com.Chirag.illumeclinica.BaseActivity;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.ProfileItem;

import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends BaseActivity {

    TextView tv;
    RecyclerView recyclerView;
    ProfileAdapter adapter;
    List<ProfileItem> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSoftKeyboard();
        setContentView(R.layout.activity_profile);





        tv=findViewById(R.id.textview);
        tv.setText("Profile");

        list=new ArrayList<>();
        recyclerView= findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        list.add(new ProfileItem("Dr. Bernadette Carr","M.Sc - Anatomy","General Physician , 12 Years Experience",
                "Dr. Bernaddete has served in variety of clinical branches and has extensive clinical experience She has worked in the Dept. of surgery, Dept. of Gynacology and Dept. of Medi more",
                "84 recommendation favour"));
        list.add(new ProfileItem("Chirag","Btech graduate","frehser","nothing to say","no fabour"));

        adapter=new ProfileAdapter(this, list);
        recyclerView.setAdapter(adapter);

    }
}
