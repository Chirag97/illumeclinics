package com.Chirag.illumeclinica.Ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import android.util.Log;
import android.widget.TextView;

import com.Chirag.illumeclinica.BaseActivity;
import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.fragments.NewPatient;
import com.Chirag.illumeclinica.fragments.OldPatients;
import com.Chirag.illumeclinica.Networking.ConsultantDoctor_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.ConsultantDoctor;

import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVisitsActivity extends BaseActivity {


    TextView tv;  /*for Toolbar*/

    ConsultantDoctor_API consultantDoctor_api;
    SegmentedButtonGroup buttonGroup;
    SegmentedButton sb1,sb2;


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSoftKeyboard();
        setContentView(R.layout.activity_add_visits);
        init();
        consultantDoctor_api= Utils.getConsultantAPI();
    }

    @SuppressLint("SetTextI18n")
    void init(){
        tv=findViewById(R.id.textview);
        tv.setTypeface(ConstantClass.getFont(getApplicationContext()));
        tv.setText("Add Visit");
        sb1=findViewById(R.id.sb1);
        sb2=findViewById(R.id.sb2);
        sb1.setTypeface(ConstantClass.getBoldfont(getApplicationContext()));
        sb2.setTypeface(ConstantClass.getBoldfont(getApplicationContext()));

        buttonGroup=findViewById(R.id.segmentedButtonGroup);
        buttonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {

                Fragment fragment;
                if(position==1){
                    final Call<ConsultantDoctor> doctorCall= consultantDoctor_api.consultantdoctor();
                    doctorCall.enqueue(new Callback<ConsultantDoctor>() {
                        @Override
                        public void onResponse(Call<ConsultantDoctor> call, Response<ConsultantDoctor> response) {
                            if(response.isSuccessful()){
                                ConsultantDoctor doctor= response.body();
                                String item[]=new String[doctor.getDoctorData().getDoctors().size()];
                                for(int i=0;i<doctor.getDoctorData().getDoctors().size();i++){
                                    item[i]=doctor.getDoctorData().getDoctors().get(i).getDoctorId();

                                    Intent intent=new Intent();
                                    intent.putExtra("doctorid",item[i]);
                                    Log.d("illume","data :"+intent);

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ConsultantDoctor> call, Throwable t) {

                        }
                    });

                    fragment= new NewPatient();
                    FragmentManager fragmentManager= getSupportFragmentManager();
                    FragmentTransaction ft= fragmentManager.beginTransaction();
                    ft.replace(R.id.fragments, fragment);
                    ft.commit();
                }
                else {
                    fragment= new OldPatients();

                    FragmentManager fragmentManager= getSupportFragmentManager();
                    FragmentTransaction ft= fragmentManager.beginTransaction();
                    ft.replace(R.id.fragments, fragment);
                    ft.commit();

                }
            }
        });




    }


//    public void entries(View view) {
//        Fragment fragment;
//        if(view==new_visit){
//
//            final Call<ConsultantDoctor> doctorCall= consultantDoctor_api.consultantdoctor();
//           doctorCall.enqueue(new Callback<ConsultantDoctor>() {
//               @Override
//               public void onResponse(Call<ConsultantDoctor> call, Response<ConsultantDoctor> response) {
//                   if(response.isSuccessful()){
//                    ConsultantDoctor doctor= response.body();
//                    String item[]=new String[doctor.getDoctorData().getDoctors().size()];
//                    for(int i=0;i<doctor.getDoctorData().getDoctors().size();i++){
//                       item[i]=doctor.getDoctorData().getDoctors().get(i).getDoctorId();
////                        Log.d("illume","Doctor Id "+item[i]);
//
//                        Intent intent=new Intent();
//                        intent.putExtra("doctorid",item[i]);
//                        Log.d("illume","data :"+intent);
//
//                    }
//                   }
//               }
//
//               @Override
//               public void onFailure(Call<ConsultantDoctor> call, Throwable t) {
//
//               }
//           });
////
////           Intent intent = getIntent();
////            String userid= intent.getStringExtra("userid");
////            Toast.makeText(getApplicationContext(),"data: "+userid, Toast.LENGTH_SHORT).show();
//
//
//            fragment= new NewPatient();
//            FragmentManager fragmentManager= getSupportFragmentManager();
//            FragmentTransaction ft= fragmentManager.beginTransaction();
//            ft.replace(R.id.fragments, fragment);
//            ft.commit();
//        }
//
//        if(view== old_patient){
//                fragment= new OldPatients();
//
//            FragmentManager fragmentManager= getSupportFragmentManager();
//            FragmentTransaction ft= fragmentManager.beginTransaction();
//            ft.replace(R.id.fragments, fragment);
//            ft.commit();
//        }
//    }
}
