package com.Chirag.illumeclinica.Ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.Adapter.MedicineAdaptor;
import com.Chirag.illumeclinica.Adapter.DiagnosisAdapter;
import com.Chirag.illumeclinica.BaseActivity;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.DoctorHomeActivity;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.models.DoctorVisitList;
import com.Chirag.illumeclinica.models.VisitList;
import com.Chirag.illumeclinica.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;


public class CreatePrescription extends BaseActivity {

    TextView tv;
    Button submittion;
    ImageView addition, addMed;
    RecyclerView recyclerView, medicineRecyclerView;

    List<Integer> list;
    List<Integer> list1;
    DiagnosisAdapter adapter;
    MedicineAdaptor adapter1;
    private VisitList visitListData;
    private DoctorVisitList doctorVisitList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSoftKeyboard();
        setContentView(R.layout.activity_create_prescription);
        Intent intent = getIntent();
        if (intent.hasExtra("visit_data")) {
            if (PreferenceManager.getInstance(CreatePrescription.this).getUserType().equalsIgnoreCase("hospital")) {
                visitListData = (VisitList) intent.getSerializableExtra("visit_data");
            } else {
                doctorVisitList = (DoctorVisitList) intent.getSerializableExtra("visit_data");
            }
        }
        init();
    }

    void init() {
        tv = findViewById(R.id.textview);
        tv.setText("Create Prescription");


        submittion = findViewById(R.id.submit);
        submittion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                gotoprofile();
                DialogUtils.showAlertBox(CreatePrescription.this, false, "", "Comming Soon.", R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                    @Override
                    public void dialogPositiveButton() {
                        startActivity(new Intent(CreatePrescription.this, DoctorHomeActivity.class));
                        finishAffinity();
                    }

                    @Override
                    public void dialogNegativeButton() {

                    }
                });
            }
        });

        list = new ArrayList<>();
        list1 = new ArrayList<>();

//        adapter = new DiagnosisAdapter(this, list);
//        adapter1 = new MedicineAdaptor(this, list1, intakeList, medicineList);

        addMed = findViewById(R.id.addmedicine);
        addition = findViewById(R.id.additions);
        addMed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                adapter1.addItem();
                Toast.makeText(getApplicationContext(), "New Medicine Entry", Toast.LENGTH_SHORT).show();
            }
        });

        addition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                adapter.additem();
                Toast.makeText(CreatePrescription.this, "New Diagnose Entry", Toast.LENGTH_SHORT).show();
            }
        });


        recyclerView = findViewById(R.id.diagnoserecyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        medicineRecyclerView = findViewById(R.id.medicinerecyclerview);
        medicineRecyclerView.setHasFixedSize(true);
        medicineRecyclerView.setLayoutManager(new LinearLayoutManager(this));

//        adapter1 = new MedicineAdaptor(/this, list1, intakeList, medicineList);
        medicineRecyclerView.setAdapter(adapter1);
//
//        list.add(new DiagnoseItem("hello"));
//        list.add(new DiagnoseItem("hi"));
//        list.add(new DiagnoseItem("hey"));
//        list.add(new DiagnoseItem("hno"));


//        adapter = new DiagnosisAdapter(this, list);
        recyclerView.setAdapter(adapter);


    }
}

