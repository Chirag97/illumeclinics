package com.Chirag.illumeclinica;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.Networking.Problems_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.models.ProblemList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TestingActivity extends AppCompatActivity {

AutoCompleteTextView autoCompleteTextView;
TextView textView;

    private Problems_API problems_api;

String[] data={"one","two","three","thirteen","four"};
    ArrayList<String> Problems= new ArrayList<>();

String problem="",ids="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        problems_api= Utils.getProblesAPI();
        callProblem();

        autoCompleteTextView=findViewById(R.id.auto);
        textView=findViewById(R.id.tv);


    }
    private void callProblem(){
        final Call<ProblemList> listCall= problems_api.getProblemlist();
        listCall.enqueue(new Callback<ProblemList>() {
            @Override
            public void onResponse(Call<ProblemList> call, Response<ProblemList> response) {

                if (response.isSuccessful()){
                    final ProblemList problemList=response.body();
                    for (int i = 0; i < problemList.getProblemData().getProblems().size(); i++) {
                        Problems.add(problemList.getProblemData().getProblems().get(i).getProblemName());
                        ids=problemList.getProblemData().getProblems().get(i).getProblemId();



                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, Problems);
                        autoCompleteTextView.setAdapter(adapter);

                        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String show = autoCompleteTextView.getText().toString();
                                String oka= problemList.getProblemData().getProblems().get(position).getProblemId();
                                textView.setText(show+" "+oka);

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<ProblemList> call, Throwable t) {

            }
        });
    }
}

