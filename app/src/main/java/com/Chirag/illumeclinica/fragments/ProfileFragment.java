package com.Chirag.illumeclinica.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.Profile_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.DoctorProfileData;
import com.Chirag.illumeclinica.models.HospitalProfileData;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {


    Profile_API profile_api;
    View view;


    @BindView(R.id.username)
    TextView name;

    @BindView(R.id.phonenumber)
    TextView phonenumber;

    @BindView(R.id.emailaddress)
    TextView emailaddress;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.speciality)
    TextView specialityofdoctor;

    @BindView(R.id.specialityname)
    TextView specialityname;

    @BindView(R.id.textview)
    TextView tv;

    @BindView(R.id.dr)
    TextView doc;

    @BindView(R.id.basic)
    TextView basic;


    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_profile, container, false);

        ButterKnife.bind(this, view);
        profile_api = Utils.receptionistProfileAPI();
        onBackPressed(view);

        //For toolbar
        tv.setText("Profile");

        //Getting user_ID  and User_Type from preferences manager
        String user_id = PreferenceManager.getInstance(getActivity()).getUserID();
        String usertype = PreferenceManager.getInstance(getActivity()).getUserType();

        //If condition is user for checking login userType
        if (usertype.equalsIgnoreCase("hospital")) {
            doc.setVisibility(View.GONE);

            //This API is used for getting Clinic(Hospital) profile data
            Call<HospitalProfileData> hospitalProfileDataCall = profile_api.hospitaldata(user_id);
            hospitalProfileDataCall.enqueue(new Callback<HospitalProfileData>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<HospitalProfileData> call, Response<HospitalProfileData> response) {

                    if (response.isSuccessful()) {
                        assert response.body() != null;

                        basic.setText("Hospital's Basic Info");
                        name.setText(ConstantClass.toTitleCase(response.body().getData().getProfileDetails().getUserName()));
                        phonenumber.setText(response.body().getData().getProfileDetails().getPhoneNo());
                        emailaddress.setText(response.body().getData().getProfileDetails().getEmailId());
                        address.setText(response.body().getData().getProfileDetails().getAddress());
                    }

                }

                @Override
                public void onFailure(Call<HospitalProfileData> call, Throwable t) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {

            //This API is used for Getting doctor profile data

            Call<DoctorProfileData> doctorProfileDataCall = profile_api.doctordata(user_id);
            doctorProfileDataCall.enqueue(new Callback<DoctorProfileData>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<DoctorProfileData> call, Response<DoctorProfileData> response) {
                    if (response.isSuccessful()) {

                        basic.setText("Doctor's Basic Info");
                        specialityofdoctor.setVisibility(View.VISIBLE);
                        specialityname.setVisibility(View.VISIBLE);
                        assert response.body() != null;
                        name.setText(ConstantClass.toTitleCase(response.body().getData().getProfileDetails().getName()));
                        phonenumber.setText(response.body().getData().getProfileDetails().getPhoneNo());
                        emailaddress.setText(response.body().getData().getProfileDetails().getEmailId());
                        address.setText(response.body().getData().getProfileDetails().getAddress());
                        specialityofdoctor.setText(response.body().getData().getProfileDetails().getSpecialityname());

                    }
                }

                @Override
                public void onFailure(Call<DoctorProfileData> call, Throwable t) {

                }
            });

        }
        return view;
    }
    public void onBackPressed(View view){
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                    builder.setMessage("Do you want to exit");
                    builder.setCancelable(true);
                    builder.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    getActivity().finish();
                                }
                            });

                    builder.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder.create();
                    alert11.show();

                    return true;
                }
                return false;
            }
        });

    }

}