package com.Chirag.illumeclinica.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.ConsultantDoctor_API;
import com.Chirag.illumeclinica.Networking.NewVisit_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.PatientProblemActivity;
import com.Chirag.illumeclinica.models.AddNewVisit;
import com.Chirag.illumeclinica.models.ConsultantDoctor;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class NewPatient extends Fragment {

    Button btn;
    Spinner consult, gender;
    Intent intent;
    NewVisit_API newVisit_api;
    ConsultantDoctor_API consultantDoctor_api;
    ConsultantDoctor consultantDoctor;
    String refered_to = "";
    String refered_by;
    Typeface typeface;
    String selectedgender = "";
    String[] gender_string = {"Male", "Female"};
    String patient_id = "";

    EditText patient_name, patient_mobile, patient_Age, patient_Gender, patient_Emailid, patient_Address, patient_referedby, consultant_doctor;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        hideSoftKeyboard();
        View view = inflater.inflate(R.layout.fragment_new_patient, container, false);

        refered_to = PreferenceManager.getInstance(getContext()).getConsultantDoctor();

        newVisit_api = Utils.getNewVisitAPI();
        consultantDoctor_api = Utils.getConsultantAPI();

        patient_name = view.findViewById(R.id.patientName);
        patient_name.setTypeface(ConstantClass.getFont(getContext()));

        patient_mobile = view.findViewById(R.id.patientNumber);
        patient_mobile.setTypeface(ConstantClass.getFont(getContext()));

        patient_Age = view.findViewById(R.id.patientAge);
        patient_Age.setTypeface(ConstantClass.getFont(getContext()));

        patient_Emailid = view.findViewById(R.id.patient_Email);
        //hello
        patient_Emailid.setTypeface(ConstantClass.getFont(getContext()));

        consultant_doctor = view.findViewById(R.id.consultant_doctor);
        consultant_doctor.setTypeface(ConstantClass.getFont(getContext()));

        patient_Address = view.findViewById(R.id.patientAddress);
        patient_Address.setTypeface(ConstantClass.getFont(getContext()));

        patient_referedby = view.findViewById(R.id.refferedbynew);
        patient_referedby.setTypeface(ConstantClass.getFont(getContext()));

        consult = view.findViewById(R.id.consultspinner_New);

        btn = view.findViewById(R.id.addvisits);
        btn.setTypeface(ConstantClass.getFont(getContext()));

        gender = view.findViewById(R.id.gender_spinner);
        genderSpinner();
        onBackPressed(view);

        consultant_doctor.setText(PreferenceManager.getInstance(getContext()).getConsultantDoctor());


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                String name = patient_name.getText().toString();
                if (name.isEmpty()) {
                    patient_name.setError("Enter name first");
                    patient_name.requestFocus();
                    return;
                }
                String mobile = patient_mobile.getText().toString();
                if (mobile.isEmpty()) {
                    patient_mobile.setError("Enter number first");
                    patient_mobile.requestFocus();
                    return;
                }
                if (mobile.length() != 10) {
                    patient_mobile.setError("Enter valid number");
                    patient_mobile.requestFocus();
                    return;
                }
                String emaillid = patient_Emailid.getText().toString();

                String age = patient_Age.getText().toString();
                String address = patient_Address.getText().toString();

                //For refered_by to we are using String refered_to//
                if (patient_referedby.getText().toString().equalsIgnoreCase("SELF")) {
                    refered_by = "2";
                }
                DialogUtils.showProgressDialog(getActivity(), "Please wait...", false);
                Call<AddNewVisit> call = newVisit_api.newvisit(PreferenceManager.getInstance(getActivity()).getUserID(), name, mobile, emaillid, age, selectedgender, address);
                call.enqueue(new Callback<AddNewVisit>() {
                    @Override
                    public void onResponse(Call<AddNewVisit> call, Response<AddNewVisit> response) {
                        DialogUtils.hideProgressDialog();
                        if (response.isSuccessful()) {
                            AndroidAppUtils.showLog(NewPatient.class.getSimpleName(), "response  :" + response.body().toString());
                            AddNewVisit visit = response.body();

                            patient_id = visit.getData().getPatientId().toString();

                            Log.d("illume", "data: " + visit.getData().toString());

                            intent = new Intent(getActivity(), PatientProblemActivity.class);
                            intent.putExtra("patient_id", patient_id);
                            getActivity().startActivity(intent);

                        } else {
                            Toast.makeText(getContext(), "Fill all value correctly", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddNewVisit> call, Throwable t) {
                        Toast.makeText(getContext(), "Enter correct parameters", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        return view;
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void genderSpinner() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, gender_string) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(typeface);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(typeface);
                return v;
            }
        };

        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        gender.setAdapter(adapter);
        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedgender = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void onBackPressed(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Do you want to exit");
                    builder.setCancelable(true);
                    builder.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    getActivity().finish();
                                }
                            });

                    builder.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder.create();
                    alert11.show();

                    return true;
                }
                return false;
            }
        });

    }

}