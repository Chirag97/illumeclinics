package com.Chirag.illumeclinica.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.Networking.ConsultantDoctorDashboard_API;
import com.Chirag.illumeclinica.Networking.DashboardVistList_API;
import com.Chirag.illumeclinica.Networking.DoctorDashboard_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.ClinicHomeActivity;
import com.Chirag.illumeclinica.adapters.ConsultantDoctorDashBoardAdapter;
import com.Chirag.illumeclinica.adapters.DashBoardAdapter;
import com.Chirag.illumeclinica.adapters.DoctorDashboardAdapter;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.models.ConsultantDoctorLogin;
import com.Chirag.illumeclinica.models.ConsvisitList;
import com.Chirag.illumeclinica.models.DashboardDoctorsVisitList;
import com.Chirag.illumeclinica.models.DashboardVisitList;
import com.Chirag.illumeclinica.models.DoctorVisitList;
import com.Chirag.illumeclinica.models.VisitList;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardFragment extends Fragment {

    private View view;
    TabLayout tabLayout;
    TextView textView, textView1, textView2, errormessage, entry;
    ImageView addentry;

    public static List<VisitList> visitLists;
    public static List<DoctorVisitList> lists;
    public static List<ConsvisitList> consvisitLists;
    ViewPager viewPager;

    DashboardVistList_API dashboardVistList_api;
    DoctorDashboard_API doctorDashboard_api;
    ConsultantDoctorDashboard_API consultantDoctorDashboard_api;
    ViewPagerAdapter viewpagerAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_receptionist__dashboard, container, false);

        visitLists = new ArrayList<>();
        lists = new ArrayList<>();
        consvisitLists = new ArrayList<>();
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tab_layout);

        errormessage = view.findViewById(R.id.errormessage);
        addentry = view.findViewById(R.id.addentry);
        entry = view.findViewById(R.id.entry);


        viewpagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewpagerAdapter.addFragment(new FirstFragment(), "Prescription Pending");
        viewpagerAdapter.addFragment(new Secondfragment(), "Prescription Generated");
        viewpagerAdapter.addFragment(new ThirdsFragment(), "Invoice Generated");

        DialogUtils.showProgressDialog(getActivity(), getActivity().getResources().getString(R.string.please_wait), false);


        viewPager.setCurrentItem(0);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        apicall();
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private ArrayList<Fragment> fragments;
        private ArrayList<String> titles;


        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            this.fragments = new ArrayList<>();
            this.titles = new ArrayList<>();

        }
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            titles.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            @SuppressLint("InflateParams") View view = LayoutInflater.from(getContext()).inflate(R.layout.customtab, null);
            TextView tv = view.findViewById(R.id.tab_text);
            tv.setText(titles.get(position));
            return titles.get(position);
        }
    }

    @SuppressLint("SetTextI18n")
    private void tabTextFont() {

        @SuppressLint("InflateParams") View view = LayoutInflater.from(getContext()).inflate(R.layout.customtab, null);
        textView = view.findViewById(R.id.tab_text);
        textView.setText("Prescription Pending");
        tabLayout.getTabAt(0).setCustomView(view);


        @SuppressLint("InflateParams") View view1 = LayoutInflater.from(getContext()).inflate(R.layout.customtab, null);
        textView1 = view1.findViewById(R.id.tab_text);
        textView1.setText("Prescription Generated");
        tabLayout.getTabAt(1).setCustomView(view1);


        @SuppressLint("InflateParams") View view2 = LayoutInflater.from(getContext()).inflate(R.layout.customtab, null);
        textView2 = view2.findViewById(R.id.tab_text);
        textView2.setText("Invoice Generated");
        tabLayout.getTabAt(2).setCustomView(view2);
    }


    private void apicall(){
        if (PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("hospital")) {
            dashboardVistList_api = Utils.getDashboardVisitListAPI();
            Call<DashboardVisitList> call = dashboardVistList_api.dashboardvisitlist(PreferenceManager.getInstance(getActivity()).getUserID());
            call.enqueue(new Callback<DashboardVisitList>() {
                @Override
                public void onResponse(Call<DashboardVisitList> call, Response<DashboardVisitList> response) {
                    DialogUtils.hideProgressDialog();
                    if (response.isSuccessful()) {
                        DialogUtils.hideProgressDialog();
                        AndroidAppUtils.showLog(DashBoardFragment.class.getSimpleName(), "response  :" + response.body().toString());

                        DashboardVisitList visitList = response.body();
                        if (visitList != null && visitList.getDashboardData() != null && visitList.getDashboardData().getVisitList() != null && visitList.getDashboardData().getVisitList().size() > 0) {

                            errormessage.setVisibility(View.GONE);
                            addentry.setVisibility(View.GONE);
                            entry.setVisibility(View.GONE);

                           visitLists = visitList.getDashboardData().getVisitList();

                            viewPager.setAdapter(viewpagerAdapter);
                            viewpagerAdapter.notifyDataSetChanged();
                            tabLayout.setupWithViewPager(viewPager);
                            tabTextFont();


                        } else {

                                errormessage.setVisibility(View.VISIBLE);
                                addentry.setVisibility(View.VISIBLE);
                                entry.setVisibility(View.VISIBLE);
                            DialogUtils.showAlertBox(getActivity(), false, "", getActivity().getResources().getString(R.string.no_record_found), R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                                @Override
                                public void dialogPositiveButton() {

                                }

                                @Override
                                public void dialogNegativeButton() {

                                }
                            });
                            addentry.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ClinicHomeActivity.mInstance.replaceFragment(new AddVisitsFragment());
                                    ClinicHomeActivity.mInstance.setTitle("Add Visit");
                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<DashboardVisitList> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else if(PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("doctor")) {
            if (PreferenceManager.getInstance(getContext()).getDoctorType().equals("Main Doctor")){
                doctorDashboard_api = Utils.addDoctorDashBoardDoctorVisitListAPI();
                Call<DashboardDoctorsVisitList> dashboarddoctorcall = doctorDashboard_api.dashboardDoctorsVisitList(PreferenceManager.getInstance(getActivity()).getUserID());
                dashboarddoctorcall.enqueue(new Callback<DashboardDoctorsVisitList>() {
                    @Override
                    public void onResponse(Call<DashboardDoctorsVisitList> call, Response<DashboardDoctorsVisitList> response) {
                        DialogUtils.hideProgressDialog();
                        if (response.isSuccessful()) {

                            DialogUtils.hideProgressDialog();
                            DashboardDoctorsVisitList doctorlist = response.body();
                            if (doctorlist!=null&&doctorlist.getDashboardDoctorVisitListData()!=null) {
                                errormessage.setVisibility(View.GONE);
                                addentry.setVisibility(View.GONE);
                                entry.setVisibility(View.GONE);
                                lists = doctorlist.getDashboardDoctorVisitListData().getVisitList();

                                viewPager.setAdapter(viewpagerAdapter);
                                viewpagerAdapter.notifyDataSetChanged();
                                tabLayout.setupWithViewPager(viewPager);
                                tabTextFont();
                            }else {
                                addentry.setVisibility(View.GONE);
                                entry.setVisibility(View.GONE);
                                DialogUtils.showAlertBox(getActivity(), false, "", getActivity().getResources().getString(R.string.no_record_found), R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                                    @Override
                                    public void dialogPositiveButton() {

                                    }

                                    @Override
                                    public void dialogNegativeButton() {

                                    }
                                });
                                errormessage.setText("No Vists Found!!!");
                                errormessage.setVisibility(View.VISIBLE);


                            }

                        } else {

                            DialogUtils.showAlertBox(getActivity(), false, "", getActivity().getResources().getString(R.string.no_record_found), R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                                @Override
                                public void dialogPositiveButton() {

                                }

                                @Override
                                public void dialogNegativeButton() {

                                }
                            });
                            errormessage.setText("No Vists Found!!!");
                            errormessage.setVisibility(View.VISIBLE);


                        }
                    }
                    @Override
                    public void onFailure(Call<DashboardDoctorsVisitList> call, Throwable t) {
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }else if (PreferenceManager.getInstance(getContext()).getDoctorType().equals("Consultant Doctor")){
                consultantDoctorDashboard_api =Utils.consultantDoctorDashboard_api();
                Call<ConsultantDoctorLogin> consultantDoctorLoginCall= consultantDoctorDashboard_api.consultantdoctorLogin(PreferenceManager.getInstance(getActivity()).getUserID());
                consultantDoctorLoginCall.enqueue(new Callback<ConsultantDoctorLogin>() {
                    @Override
                    public void onResponse(Call<ConsultantDoctorLogin> call, Response<ConsultantDoctorLogin> response) {

                        DialogUtils.hideProgressDialog();
                        if (response.isSuccessful()) {
                            DialogUtils.hideProgressDialog();
                            ConsultantDoctorLogin consultantlist = response.body();

                            if(consultantlist!=null&&consultantlist.getData()!=null) {

                                errormessage.setVisibility(View.GONE);
                                addentry.setVisibility(View.GONE);
                                entry.setVisibility(View.GONE);

                                consvisitLists = consultantlist.getData().getConsvisitList();

                                viewPager.setAdapter(viewpagerAdapter);
                                viewpagerAdapter.notifyDataSetChanged();
                                tabLayout.setupWithViewPager(viewPager);
                                tabTextFont();
                            }
                            else {
                                addentry.setVisibility(View.GONE);
                                entry.setVisibility(View.GONE);
                                DialogUtils.showAlertBox(getActivity(), false, "", getActivity().getResources().getString(R.string.no_record_found), R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                                    @Override
                                    public void dialogPositiveButton() {

                                    }

                                    @Override
                                    public void dialogNegativeButton() {

                                    }
                                });
                                errormessage.setText("No Vists Found!!!");
                                errormessage.setVisibility(View.VISIBLE);
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<ConsultantDoctorLogin> call, Throwable t) {
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

        }
    }

}
