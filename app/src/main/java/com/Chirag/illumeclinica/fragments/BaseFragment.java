package com.Chirag.illumeclinica.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;


public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public boolean onBackPressed() {
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}
