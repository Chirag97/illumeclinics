package com.Chirag.illumeclinica.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.Chirag.illumeclinica.Networking.ConsultantDoctorDashboard_API;
import com.Chirag.illumeclinica.Networking.DashboardVistList_API;
import com.Chirag.illumeclinica.Networking.DoctorDashboard_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.Tessting.CustomTextView;
import com.Chirag.illumeclinica.adapters.ConsultantDoctorDashBoardAdapter;
import com.Chirag.illumeclinica.adapters.DashBoardAdapter;
import com.Chirag.illumeclinica.adapters.DoctorDashboardAdapter;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.models.ConsultantDoctorLogin;
import com.Chirag.illumeclinica.models.ConsvisitList;
import com.Chirag.illumeclinica.models.DashboardDoctorsVisitList;
import com.Chirag.illumeclinica.models.DashboardVisitList;
import com.Chirag.illumeclinica.models.DoctorVisitList;
import com.Chirag.illumeclinica.models.VisitList;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdsFragment extends Fragment {

 DashboardVistList_API dashboardVistList_api;
    DoctorDashboard_API doctorDashboard_api;
    ConsultantDoctorDashboard_API consultantDoctorDashboard_api;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.errormessage)
    CustomTextView errormessage;
    private DashBoardAdapter adapter;
    private DoctorDashboardAdapter doctorDashboardAdapter;
    private ConsultantDoctorDashBoardAdapter consultantDoctorDashBoardAdapter;

    Handler handler;
    Runnable runnable;
    int apiDelayed;
    DashboardVisitList visitList;
    DashboardDoctorsVisitList doctorlist;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_thirds, container, false);
        ButterKnife.bind(this, view);
        handler = new Handler();
        apiDelayed = 30*1000; //1 second=1000 milisecond, 30*1000=30seconds



        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        DialogUtils.showProgressDialog(getActivity(), getActivity().getResources().getString(R.string.please_wait), false);
        onBackPressed(view);

//        apicall();


//        if (PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("hospital")){
//            adapter = new DashBoardAdapter(getActivity(), getCliniclist(DashBoardFragment.visitLists));
//            recyclerView.setAdapter(adapter);
//            adapter.notifyDataSetChanged();
//        } else  if (PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("doctor")) {
//            doctorDashboardAdapter = new DoctorDashboardAdapter(getActivity(), getDoctorlist(DashBoardFragment.lists));
//            recyclerView.setAdapter(doctorDashboardAdapter);
//            doctorDashboardAdapter.notifyDataSetChanged();
//        } else {
//            consultantDoctorDashBoardAdapter = new ConsultantDoctorDashBoardAdapter(getActivity(), getConsUpdate(DashBoardFragment.consvisitLists));
//            recyclerView.setAdapter(consultantDoctorDashBoardAdapter);
//            consultantDoctorDashBoardAdapter.notifyDataSetChanged();
//        }

        if (PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("hospital")) {

            if (getCliniclist(DashBoardFragment.visitLists)!=null && getCliniclist(DashBoardFragment.visitLists).size()>0) {
                errormessage.setVisibility(View.GONE);
                adapter = new DashBoardAdapter(getActivity(), getCliniclist(DashBoardFragment.visitLists));
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                errormessage.setVisibility(View.VISIBLE);
            }

        } else if (PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("doctor")) {
            if (PreferenceManager.getInstance(getContext()).getDoctorType().equals("Main Doctor")) {
                if( getDoctorlist(DashBoardFragment.lists)!=null &&  getDoctorlist(DashBoardFragment.lists).size()>0){
                    errormessage.setVisibility(View.GONE);
                    doctorDashboardAdapter = new DoctorDashboardAdapter(getActivity(), getDoctorlist(DashBoardFragment.lists));
                    recyclerView.setAdapter(doctorDashboardAdapter);
                    doctorDashboardAdapter.notifyDataSetChanged();}
                else {
                    errormessage.setVisibility(View.VISIBLE);
                }
            }else if (PreferenceManager.getInstance(getContext()).getDoctorType().equals("Consultant Doctor")){
                if(getConsUpdate(DashBoardFragment.consvisitLists)!=null && getConsUpdate(DashBoardFragment.consvisitLists).size()>0) {
                    errormessage.setVisibility(View.GONE);
                    consultantDoctorDashBoardAdapter = new ConsultantDoctorDashBoardAdapter(getActivity(), getConsUpdate(DashBoardFragment.consvisitLists));
                    recyclerView.setAdapter(consultantDoctorDashBoardAdapter);
                    consultantDoctorDashBoardAdapter.notifyDataSetChanged();
                }
                else {
                    errormessage.setVisibility(View.VISIBLE);
                }
            }


        }
        else {
            errormessage.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        apicall();
//
//
//        handler.postDelayed( runnable = new Runnable() {
//            public void run() {
//                //do your function;
//                apicall();
//                handler.postDelayed(runnable, apiDelayed);
//            }
//        }, apiDelayed);
    }

    public void onBackPressed(View view){
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                    builder.setMessage("Do you want to exit");
                    builder.setCancelable(true);
                    builder.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    getActivity().finish();
                                }
                            });

                    builder.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder.create();
                    alert11.show();

                    return true;
                }
                return false;
            }
        });

    }

    List<VisitList> getCliniclist(List<VisitList> list){
        List<VisitList> visitLists = new ArrayList<>();
        for (int i=0;i<list.size();i++){
            if (list.get(i).getInvoiceStatus().equalsIgnoreCase("4")){
                visitLists.add(list.get(i));
            }
        }

        return visitLists;
    }

    List<DoctorVisitList> getDoctorlist( List<DoctorVisitList> lists){
        List<DoctorVisitList> doctorvisitlist= new ArrayList<>();
        for(int i=0;i<lists.size();i++){
            if(lists.get(i).getInvoiceStatus().equalsIgnoreCase("4")){
                doctorvisitlist.add(lists.get(i));
            }
        }
        return doctorvisitlist;
    }

    List<ConsvisitList> getConsUpdate(List<ConsvisitList> lists){
        List<ConsvisitList> consVisitList= new ArrayList<>();
        for(int i=0;i<lists.size();i++){
            if(lists.get(i).getVisitStatus().equalsIgnoreCase("4")){
                consVisitList.add(lists.get(i));
            }
        }
        return consVisitList;
    }

    private void apicall(){
        if (PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("hospital")) {
            dashboardVistList_api = Utils.getDashboardVisitListAPI();
            Call<DashboardVisitList> call = dashboardVistList_api.dashboardvisitlist(PreferenceManager.getInstance(getActivity()).getUserID());
            call.enqueue(new Callback<DashboardVisitList>() {
                @Override
                public void onResponse(Call<DashboardVisitList> call, Response<DashboardVisitList> response) {
                    DialogUtils.hideProgressDialog();
                    if (response.isSuccessful()) {
                        AndroidAppUtils.showLog(DashBoardFragment.class.getSimpleName(), "response  :" + response.body().toString());

                        DashboardVisitList visitList = response.body();
                        if (visitList != null && visitList.getDashboardData() != null && visitList.getDashboardData().getVisitList() != null && visitList.getDashboardData().getVisitList().size() > 0) {

                            adapter = new DashBoardAdapter(getActivity(), getCliniclist(visitList.getDashboardData().getVisitList()));

                            recyclerView.setAdapter(adapter);


                        } else {
                            DialogUtils.showAlertBox(getActivity(), false, "", getActivity().getResources().getString(R.string.no_record_found), R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                                @Override
                                public void dialogPositiveButton() {

                                }

                                @Override
                                public void dialogNegativeButton() {

                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<DashboardVisitList> call, Throwable t) {
                }
            });
        } else if(PreferenceManager.getInstance(getContext()).getUserType().equalsIgnoreCase("doctor")) {
            doctorDashboard_api = Utils.addDoctorDashBoardDoctorVisitListAPI();
            Call<DashboardDoctorsVisitList> dashboarddoctorcall = doctorDashboard_api.dashboardDoctorsVisitList(PreferenceManager.getInstance(getActivity()).getUserID());
            dashboarddoctorcall.enqueue(new Callback<DashboardDoctorsVisitList>() {
                @Override
                public void onResponse(Call<DashboardDoctorsVisitList> call, Response<DashboardDoctorsVisitList> response) {
                    DialogUtils.hideProgressDialog();
                    if (response.isSuccessful()) {
                        DashboardDoctorsVisitList doctorlist = response.body();
                        doctorDashboardAdapter = new DoctorDashboardAdapter(getActivity(), getDoctorlist(doctorlist.getDashboardDoctorVisitListData().getVisitList()));
                        recyclerView.setAdapter(doctorDashboardAdapter);
                    }
                }
                @Override
                public void onFailure(Call<DashboardDoctorsVisitList> call, Throwable t) {
                }
            });
        }
        else{
            consultantDoctorDashboard_api =Utils.consultantDoctorDashboard_api();
            Call<ConsultantDoctorLogin> consultantDoctorLoginCall= consultantDoctorDashboard_api.consultantdoctorLogin(PreferenceManager.getInstance(getActivity()).getUserType());
            consultantDoctorLoginCall.enqueue(new Callback<ConsultantDoctorLogin>() {
                @Override
                public void onResponse(Call<ConsultantDoctorLogin> call, Response<ConsultantDoctorLogin> response) {

                    DialogUtils.hideProgressDialog();
                    if (response.isSuccessful()) {
                        ConsultantDoctorLogin consultantlist = response.body();

                        if(consultantlist!=null) {
                            consultantDoctorDashBoardAdapter = new ConsultantDoctorDashBoardAdapter(getActivity(), getConsUpdate(consultantlist.getData().getConsvisitList()));
                            recyclerView.setAdapter(consultantDoctorDashBoardAdapter);
                        }
                        else{
                        }

                    }
                }

                @Override
                public void onFailure(Call<ConsultantDoctorLogin> call, Throwable t) {
                }
            });


        }
    }

}
