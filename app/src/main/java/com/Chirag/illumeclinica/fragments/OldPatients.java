package com.Chirag.illumeclinica.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.ConsultantDoctor_API;
import com.Chirag.illumeclinica.Networking.NewVisit_API;
import com.Chirag.illumeclinica.Networking.OldVisit_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.PatientProblemActivity;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.models.OldVisit;
import com.Chirag.illumeclinica.utils.DialogUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class OldPatients extends Fragment {

    EditText et;
    RelativeLayout first;
    LinearLayout second;
    Button button;
    OldVisit_API oldvisitAPI;
    ConsultantDoctor_API consultantDoctor_api;
    OldVisit oldvisitdata;


    EditText name, phone, Age, patientgender, email, address, referedby, consultantdoctor_name;
    NewVisit_API newVisit_api;
    private boolean needToShow = false;
    private boolean needToShow1 = false;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        hideSoftKeyboard();
        final View view = inflater.inflate(R.layout.fragment_old_patients, container, false);

        first = view.findViewById(R.id.rr1);
        second = view.findViewById(R.id.ll1);
        name = view.findViewById(R.id.userName);
        name.setTypeface(ConstantClass.getFont(getContext()));

        phone = view.findViewById(R.id.mobilenumber);
        phone.setTypeface(ConstantClass.getFont(getContext()));

        Age = view.findViewById(R.id.age);
        Age.setTypeface(ConstantClass.getFont(getContext()));

        patientgender = view.findViewById(R.id.gender);
        patientgender.setTypeface(ConstantClass.getFont(getContext()));

        email = view.findViewById(R.id.emailid);
        email.setTypeface(ConstantClass.getFont(getContext()));

        button = view.findViewById(R.id.addOldVisit);
        button.setTypeface(ConstantClass.getFont(getContext()));

        referedby = view.findViewById(R.id.refferedby);
        referedby.setTypeface(ConstantClass.getFont(getContext()));

        address = view.findViewById(R.id.addres);
        address.setTypeface(ConstantClass.getFont(getContext()));

        consultantdoctor_name = view.findViewById(R.id.consultantdoctorname);
        consultantdoctor_name.setTypeface(ConstantClass.getFont(getContext()));

        newVisit_api = Utils.getNewVisitAPI();
        onBackPressed(view);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String patint_id = oldvisitdata.getOldVisitData().getPatientDetails().get(0).getPatientId();
                Intent intent = new Intent(getActivity().getApplication(), PatientProblemActivity.class);
                intent.putExtra("oldpatient_id", patint_id);
                startActivity(intent);

//                startActivity(new Intent(getActivity().getApplication(),PatientProblemActivity.class));
            }

//                Call<AddNewVisit> call = newVisit_api.newvisit(PreferenceManager.getInstance(getActivity()).getUserID(),  patient_name, mobile, email_id, age, gender, patient_address);
//                call.enqueue(new Callback<AddNewVisit>() {
//                    @Override
//                    public void onResponse(Call<AddNewVisit> call, Response<AddNewVisit> response) {
//                        if (response.isSuccessful()) {
//                            AddNewVisit visit = response.body();
//                            Log.d("illume", "data" + visit);
////                            Toast.makeText(getContext(), "Values:   " + visit, Toast.LENGTH_SHORT).show();
//                            intent = new Intent(getActivity(), PatientProblemActivity.class);
//                            intent.putExtra("patient_id", visit.getData().getPatientId());
//                            intent.putExtra("visit_id", visit.getData().getUserId());
//                            getActivity().startActivity(intent);
////                            startActivity(new Intent(Objects.requireNonNull(getActivity()).getApplication(), AddPatientProblem.class));
//
//                        } else {
//                            Toast.makeText(getContext(), "Fill all value correctly", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<AddNewVisit> call, Throwable t) {
//                        Toast.makeText(getContext(), "Enter correct parameters", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
        });

        et = view.findViewById(R.id.enteroldpatientid);
        et.setTypeface(ConstantClass.getFont(getContext()));
        et.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    hideSoftKeyboard();

                    oldvisitAPI = Utils.getOldVisitAPI();
                    consultantDoctor_api = Utils.getConsultantAPI();


                    final String number = et.getText().toString();
                    if (number.length() > 0) {
                        DialogUtils.showProgressDialog(getActivity(), "Please wait...", false);
                        needToShow=false;
                        needToShow1=false;
                        Call<OldVisit> call = oldvisitAPI.oldvisit(number);
                        call.enqueue(new Callback<OldVisit>() {
                            @Override
                            public void onResponse(Call<OldVisit> call, Response<OldVisit> response) {
                                DialogUtils.hideProgressDialog();
                                if (response.isSuccessful()) {
                                    oldvisitdata = response.body();

                                    if (oldvisitdata != null && oldvisitdata.getOldVisitData() != null) {
                                        first.setVisibility(View.GONE);
                                        second.setVisibility(View.VISIBLE);


                                        consultantdoctor_name.setText(PreferenceManager.getInstance(getContext()).getConsultantDoctor());
                                        name.setText(oldvisitdata.getOldVisitData().getPatientDetails().get(0).getPatientName());
                                        phone.setText(oldvisitdata.getOldVisitData().getPatientDetails().get(0).getPatientMobileno());
                                        Age.setText(oldvisitdata.getOldVisitData().getPatientDetails().get(0).getAge());
                                        patientgender.setText(oldvisitdata.getOldVisitData().getPatientDetails().get(0).getGender());
                                        email.setText(oldvisitdata.getOldVisitData().getPatientDetails().get(0).getEmail());
                                        address.setText(oldvisitdata.getOldVisitData().getPatientDetails().get(0).getAddress());
                                    } else {
                                        if (!needToShow) {
                                            needToShow = true;
                                            DialogUtils.showAlertBox(getActivity(), false, "", "Invalid Patient ID/Mobile Number", R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                                                @Override
                                                public void dialogPositiveButton() {

                                                }

                                                @Override
                                                public void dialogNegativeButton() {

                                                }
                                            });
                                        }
                                    }
                                } else {
                                }
                            }

                            @Override
                            public void onFailure(Call<OldVisit> call, Throwable t) {
                                Toast.makeText(getActivity(), "Enter Valid id \n" + t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });


                    } else {
                        if (!needToShow1) {
                            needToShow1 = true;
                        DialogUtils.showAlertBox(getActivity(), false, "", "Please Enter valid Patient ID/Mobile Number", R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                            @Override
                            public void dialogPositiveButton() {

                            }

                            @Override
                            public void dialogNegativeButton() {

                            }
                        });
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
        return view;
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void onBackPressed(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Do you want to exit");
                    builder.setCancelable(true);
                    builder.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    getActivity().finish();
                                }
                            });

                    builder.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder.create();
                    alert11.show();

                    return true;
                }
                return false;
            }
        });

    }

}