package com.Chirag.illumeclinica.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.ConsultantDoctor_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.ConsultantDoctor;

import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVisitsFragment extends Fragment {
    private View view;
    TextView tv;  /*for Toolbar*/

    ConsultantDoctor_API consultantDoctor_api;
    SegmentedButtonGroup buttonGroup;
    SegmentedButton sb1,sb2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_add_visits, container, false);
        consultantDoctor_api = Utils.getConsultantAPI();
        init();
        return view;
    }


    void init(){
        tv=view.findViewById(R.id.textview);
        tv.setTypeface(ConstantClass.getFont(getActivity()));
        tv.setText("Add Visit");
        sb1=view.findViewById(R.id.sb1);
        sb2=view.findViewById(R.id.sb2);
        sb1.setTypeface(ConstantClass.getBoldfont(getActivity()));
        sb2.setTypeface(ConstantClass.getBoldfont(getActivity()));

        buttonGroup=view.findViewById(R.id.segmentedButtonGroup);
        buttonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {

                Fragment fragment;
                if(position==1){
                    final Call<ConsultantDoctor> doctorCall= consultantDoctor_api.consultantdoctor();
                    doctorCall.enqueue(new Callback<ConsultantDoctor>() {
                        @Override
                        public void onResponse(Call<ConsultantDoctor> call, Response<ConsultantDoctor> response) {
                            if(response.isSuccessful()){
                                ConsultantDoctor doctor= response.body();
                                String item[]=new String[doctor.getDoctorData().getDoctors().size()];
                                for(int i=0;i<doctor.getDoctorData().getDoctors().size();i++){
                                    item[i]=doctor.getDoctorData().getDoctors().get(i).getDoctorId();

                                    Intent intent=new Intent();
                                    intent.putExtra("doctorid",item[i]);
                                    Log.d("illume","data :"+intent);

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ConsultantDoctor> call, Throwable t) {

                        }
                    });

                    fragment= new NewPatient();
                    FragmentTransaction ft= getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragments, fragment);
                    ft.commit();
                }
                else {
                    fragment= new OldPatients();

                    FragmentTransaction ft= getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragments, fragment);
                    ft.commit();

                }
            }
        });




    }
}
