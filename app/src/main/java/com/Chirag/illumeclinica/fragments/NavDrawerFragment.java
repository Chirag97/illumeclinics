package com.Chirag.illumeclinica.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.ClinicHomeActivity;
import com.Chirag.illumeclinica.activities.DoctorHomeActivity;
import com.Chirag.illumeclinica.activities.SignInActivity;
import com.Chirag.illumeclinica.adapters.DrawerListAdapter;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.models.DraweListModel;
import com.Chirag.illumeclinica.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;


public class NavDrawerFragment extends BaseFragment implements DrawerListAdapter.AdapterClickListener, View.OnClickListener {
    private View view;
    private DrawerListAdapter mAdapter;
    private List<DraweListModel> drawerItemList = new ArrayList<>();
    private RecyclerView recyclerView;
    private DrawerLayout drawerLayout;
    private ImageView nav_back;

    public NavDrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.navigation_drawer_fragment, container, false);
        initviews();
        assignClicks();
        return view;
    }

    private void initviews() {
        recyclerView = view.findViewById(R.id.nav_items);
        nav_back = view.findViewById(R.id.nav_back);
        setNavigationDrawerItems();
        mAdapter = new DrawerListAdapter(getActivity(), drawerItemList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    private void assignClicks() {
        nav_back.setOnClickListener(this);
    }

    /**
     * getting the drawer layout instance from the activity
     *
     * @param mDrawerLayout
     */
    public void setUp(DrawerLayout mDrawerLayout) {
        if (mDrawerLayout != null)
            drawerLayout = mDrawerLayout;

    }

    @Override
    public void itemClicked(View view, int position) {
        switch (position) {
            case 0:
                if (drawerItemList.size() == 3) {
                    DoctorHomeActivity.mInstance.replaceFragment(new DashBoardFragment());
                    DoctorHomeActivity.mInstance.setTitle("IllumeClinics");

                } else {
                    ClinicHomeActivity.mInstance.replaceFragment(new DashBoardFragment());
                    ClinicHomeActivity.mInstance.setTitle("IllumeClinics");
                }
//                startActivity(new Intent(getActivity(), ProfileActivity.class));
                closeDrawer();
                break;
            case 1:
                if (drawerItemList.get(position).getItemName().equalsIgnoreCase("Add Visit")) {
//                    startActivity(new Intent(getActivity(), AddVisitsActivity.class));
                    ClinicHomeActivity.mInstance.replaceFragment(new AddVisitsFragment());
                    ClinicHomeActivity.mInstance.setTitle("Add Visit");
                } else {
                    DoctorHomeActivity.mInstance.replaceFragment(new ProfileFragment());
                    DoctorHomeActivity.mInstance.setTitle("Profile");
//                    startActivity(new Intent(getActivity(), ProfileActivity.class));
                }
                closeDrawer();
                break;
            case 2:
                if (drawerItemList.size() == 3) {
                    DialogUtils.showAlertBox(getActivity(), false, "", getActivity().getResources().getString(R.string.do_you_want_to_logout), R.string.ok, R.string.cancel, new ClickListenerForDialogBox() {
                        @Override
                        public void dialogPositiveButton() {
                            PreferenceManager.getInstance(getActivity()).logoutUser();
                            startActivity(new Intent(getActivity(), SignInActivity.class));
                            getActivity().finishAffinity();
                            closeDrawer();
                        }

                        @Override
                        public void dialogNegativeButton() {
                            closeDrawer();
                        }
                    });


                } else {
                    ClinicHomeActivity.mInstance.replaceFragment(new ProfileFragment());
                    ClinicHomeActivity.mInstance.setTitle("Profile");
                }
                closeDrawer();
                break;
            case 3:
                DialogUtils.showAlertBox(getActivity(), false, "", getActivity().getResources().getString(R.string.do_you_want_to_logout), R.string.ok, R.string.cancel, new ClickListenerForDialogBox() {
                    @Override
                    public void dialogPositiveButton() {
                        PreferenceManager.getInstance(getActivity()).logoutUser();
                        startActivity(new Intent(getActivity(), SignInActivity.class));
                        getActivity().finishAffinity();
                        closeDrawer();
                    }

                    @Override
                    public void dialogNegativeButton() {
                        closeDrawer();
                    }
                });
closeDrawer();
                break;

        }
    }

    /**
     * setting the navigation drawer list items
     */
    public void setNavigationDrawerItems() {
        if (PreferenceManager.getInstance(getActivity()).getUserType().equalsIgnoreCase("doctor")) {
            DraweListModel draweListModel = new DraweListModel();
            draweListModel.setItemName("Dashboard");
            draweListModel.setSelected(true);
            drawerItemList.add(draweListModel);
            DraweListModel draweListModel1 = new DraweListModel();
            draweListModel1.setItemName("My account");
            draweListModel1.setSelected(false);
            drawerItemList.add(draweListModel1);
            DraweListModel draweListModel2 = new DraweListModel();
            draweListModel2.setItemName("Logout");
            draweListModel2.setSelected(false);
            drawerItemList.add(draweListModel2);
        } else {
            DraweListModel draweListModel = new DraweListModel();
            draweListModel.setItemName("Dashboard");
            draweListModel.setSelected(true);
            drawerItemList.add(draweListModel);
            DraweListModel draweListModel3 = new DraweListModel();
            draweListModel3.setItemName("Add Visit");
            draweListModel3.setSelected(false);
            drawerItemList.add(draweListModel3);
            DraweListModel draweListModel1 = new DraweListModel();
            draweListModel1.setItemName("My account");
            draweListModel1.setSelected(false);
            drawerItemList.add(draweListModel1);
            DraweListModel draweListModel2 = new DraweListModel();
            draweListModel2.setItemName("Logout");
            draweListModel2.setSelected(false);
            drawerItemList.add(draweListModel2);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_back:
                closeDrawer();
                break;
        }
    }

    private void closeDrawer() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
}
