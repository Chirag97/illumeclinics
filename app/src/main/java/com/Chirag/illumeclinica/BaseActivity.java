package com.Chirag.illumeclinica;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.Chirag.illumeclinica.Ui.AddVisitsActivity;
import com.Chirag.illumeclinica.Ui.ProfileActivity;
import com.Chirag.illumeclinica.activities.CreatePrescriptionActivity;
import com.Chirag.illumeclinica.activities.PatientProblemActivity;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void gotoReceptionist_Dashboard() {
        startActivity(new Intent(this, Receptionist_DashboardActivity.class));
    }

    public void gotoAddVisits() {
        startActivity(new Intent(this, AddVisitsActivity.class));
    }

    public void gotoAddnewPatient() {
        startActivity(new Intent(this, PatientProblemActivity.class));
    }

    public void gotoCreatePrescription() {
        startActivity(new Intent(this, CreatePrescriptionActivity.class));
    }

    public void gotoprofile() {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
