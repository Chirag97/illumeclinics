package com.Chirag.illumeclinica.iHelper;

public interface AppHelper {

    int CAMERA_REQUEST = 1;
    int SELECT_IMAGE = 3;
    int GALLERY_IMAGE = 4;
    String DATA = "data";
    int CROP_PIC = 2;
    int STORAGE_REQUEST = 10;
    int LOCATION_REQUEST = 101;
    int MY_PERMISSIONS_REQUEST_CAMERA = 100;

}
