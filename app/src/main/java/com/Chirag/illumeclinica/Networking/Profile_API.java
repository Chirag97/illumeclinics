package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.DoctorProfileData;
import com.Chirag.illumeclinica.models.HospitalProfileData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Profile_API {

    @FormUrlEncoded
    @POST("getProfieDetails")
    Call<HospitalProfileData> hospitaldata(
            @Field("user_id") String user_id
    );


    @FormUrlEncoded
    @POST("getProfieDetails")
    Call<DoctorProfileData> doctordata(
            @Field("user_id") String user_id
    );
}
