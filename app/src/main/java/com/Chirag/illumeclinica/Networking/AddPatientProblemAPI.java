package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.AddPatientProblem;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddPatientProblemAPI {

    @FormUrlEncoded
    @POST("add_patient_problem")
    Call<AddPatientProblem> AddPatientProblem(
            @Field("user_id") String user_id,
            @Field("patient_id") String patient_id,
            @Field("problem_id") String problem_id,
            @Field("symptom_ids[]") String symptom_ids,
            @Field("duration") String duration,
            @Field("allergy_ids[]") String allergy_ids,
            @Field("runningTreatmentStatus") String runningTreatmentStatus,
            @Field("runningTreatmentDuration") String runningTreatmentDuration,
            @Field("runningTreatmentDoctor") String runningTreatmentDoctor,
            @Field("runningTreatmentMedicine") String runningTreatmentMedicine,
            @Field("refered_by_doctor") String refered_by_doctor,
            @Field("refered_to_doctor") String refered_to_doctor);
}
