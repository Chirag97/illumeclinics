package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.AddNewVisit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NewVisit_API {

    @FormUrlEncoded
    @POST("add_new_patient")
    Call<AddNewVisit> newvisit(
            @Field("user_id") String user_id,
            @Field("patient_name") String patient_name,
            @Field("mobile_number") String mobile_number,
            @Field("email") String email,
            @Field("age") String age,
            @Field("gender") String gender,
            @Field("address") String address
    );


}
