package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.CreateNewPrescription;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CreatePrescriptionAPI {
    @FormUrlEncoded
    @POST("create_new_prescription")
    Call<CreateNewPrescription> createNewPrescription(
            @Field("user_id") String user_id,
            @Field("patient_id") String patient_id,
            @Field("visit_id") String visit_id,
            @Field("test_ids[]") String test_ids,
            @Field("medicine_ids") String medicine_ids,
            @Field("diagnosis") String diagnosis

    );
}
