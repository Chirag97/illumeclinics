package com.Chirag.illumeclinica.networking;

import android.app.Activity;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.listeners.ResponseListener;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.ParseError;
import com.android.volley.error.ServerError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * NetworkManager helps to manage Volley GET/POST requests.
 */
public class NetworkManager {
    private static final String TAG = NetworkManager.class.getSimpleName();
    private static NetworkManager instance = null;
    private static Activity mContext;

    //for Volley API
    public RequestQueue requestQueue;

    private NetworkManager(Activity context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        mContext = context;
    }

    public static synchronized NetworkManager getInstance(Activity context) {
        mContext = context;
        if (null == instance)
            instance = new NetworkManager(context);
        return instance;
    }

    public void PostRequestWithJson(final String tag, String Url, final Map<String, String> param,
                                    final String requestBody, boolean showLoader,
                                    boolean isPost,
                                    final ResponseListener<String> listener) {

        AndroidAppUtils.showLog(TAG, "<<<< response request for " + tag + " is : " + Url + " << == >> " + requestBody);
        try {
            if (mContext != null) {

                if (showLoader) {
                    DialogUtils.showProgressDialog(mContext, "please wait", false);
                }
            }

            final StringRequest stringRequest = new StringRequest(
                    isPost ? Request.Method.POST : Request.Method.PUT, Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    DialogUtils.hideProgressDialog();
                    AndroidAppUtils.showLog(TAG, "" + response);

                    if (null != response) {
                        listener.getResponse(tag, response);

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    DialogUtils.hideProgressDialog();
                    AndroidAppUtils.showLog(TAG, "error :" + error.getMessage());

                    error.printStackTrace();
                    /* Error handling */
                    if (/*error instanceof TimeoutError ||*/ error instanceof NoConnectionError) {
                        listener.getError(tag, error, false);
                        return;
                    } else if (error instanceof TimeoutError) {
                        listener.getError(tag, error, false);
                    } else if (error instanceof AuthFailureError) {
//                        AndroidAppUtils.showToast(mContext, mContext.getResources().getString(R.string.please_enter_valid_inputs));
                        listener.getError(tag, error, false);
                        return;
                    } else if (error instanceof ServerError) {
                        AndroidAppUtils.showToast(mContext, mContext.getResources().getString(R.string.somthing_went_wrong));
                        listener.getError(tag, error, false);
                        return;
                    } else if (error instanceof NetworkError) {
                        AndroidAppUtils.showToast(mContext, mContext.getResources().getString(R.string.somthing_went_wrong));
                        listener.getError(tag, error, false);
                        return;
                    } else if (error instanceof ParseError) {
                        AndroidAppUtils.showToast(mContext, mContext.getResources().getString(R.string.somthing_went_wrong));
                        listener.getError(tag, error, false);
                        return;
                    }

                    /* Sending the callback to respected activity*/
                    if (null != error.networkResponse) {
                        listener.getError(tag, error, true);
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {

                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        uee.printStackTrace();
                        AndroidAppUtils.showToast(mContext, mContext.getResources().getString(R.string.somthing_went_wrong));
                        listener.getError(tag, null, false);
                        return null;
                    }

                }


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();

                    if (headers != null && param != null)
                        headers.putAll(param);

                    return headers;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {

                    return super.parseNetworkError(volleyError);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    // take the statusCode here.

                    return super.parseNetworkResponse(response);
                }

                @Override
                public Priority getPriority() {
                    if (tag.equalsIgnoreCase("content_option")) {
                        return Priority.HIGH;
                    } else {
                        return Priority.NORMAL;
                    }
                }
            };

            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}