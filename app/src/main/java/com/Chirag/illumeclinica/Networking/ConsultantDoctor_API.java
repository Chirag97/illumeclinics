package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.ConsultantDoctor;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ConsultantDoctor_API {

    @GET("getAllConsultantDoctorList")
    Call<ConsultantDoctor> consultantdoctor();
}
