package com.Chirag.illumeclinica.Networking;

public class Utils {

    public static String Illume_URL = "http://api.illumeclinics.com/illumeclinics/Api/";

    private Utils() {
    }

    public static LoginApi getLoginAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(LoginApi.class);
    }

    public static Allergy_API getAllergyAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(Allergy_API.class);
    }

    public static OldVisit_API getOldVisitAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(OldVisit_API.class);
    }

    public static ConsultantDoctor_API getConsultantAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(ConsultantDoctor_API.class);
    }

    public static NewVisit_API getNewVisitAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(NewVisit_API.class);
    }

    public static Problems_API getProblesAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(Problems_API.class);
    }

    public static Symptoms_API getSymptomAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(Symptoms_API.class);
    }

    public static DashboardVistList_API getDashboardVisitListAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(DashboardVistList_API.class);
    }

    public static VisitDetails_API getVisitDetailsAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(VisitDetails_API.class);
    }

    public static DoctorDashboard_API addDoctorDashBoardDoctorVisitListAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(DoctorDashboard_API.class);
    }

    public static Profile_API receptionistProfileAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(Profile_API.class);
    }

    public static GetMedicineAPI getMedicineAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(GetMedicineAPI.class);
    }

    public static CreatePrescriptionAPI createPrescriptionAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(CreatePrescriptionAPI.class);
    }

    public static AddPatientProblemAPI addPatientProblemAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(AddPatientProblemAPI.class);
    }

    public static ImageUploadAPI imageUploadAPI() {
        return RetrofitConnectivity.getClient(Illume_URL).create(ImageUploadAPI.class);
    }

    public static ConsultantDoctor_API consultantDoctor_api() {
        return RetrofitConnectivity.getClient(Illume_URL).create(ConsultantDoctor_API.class);

    }

    public static ConsultantDoctorDashboard_API consultantDoctorDashboard_api() {
        return RetrofitConnectivity.getClient(Illume_URL).create(ConsultantDoctorDashboard_API.class);
    }

    public static InvoiceAPI invoice_API() {
        return RetrofitConnectivity.getClient(Illume_URL).create(InvoiceAPI.class);
    }
}
