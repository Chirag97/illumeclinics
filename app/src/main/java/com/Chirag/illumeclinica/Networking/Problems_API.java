package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.ProblemList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Problems_API {

    @GET("getProblemList")
    Call<ProblemList> getProblemlist();
}
