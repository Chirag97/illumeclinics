package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.VisitDetails;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface VisitDetails_API {
    @FormUrlEncoded
    @POST("getVisitDetails")
    Call<VisitDetails> getvisitdetails(
            @Field("visit_id")String visit_id
    );
}
