package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.ImageModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ImageUploadAPI {
    /**
     * Upload Profile Image to Server
     *
     * @param image
     * @return
     */
    @Multipart
    @POST("upload_image")
    Call<ImageModel> postImage(@Part MultipartBody.Part image,
                               @Part("visit_id") RequestBody id);
}
