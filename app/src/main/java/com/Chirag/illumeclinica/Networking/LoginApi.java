package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.LoginData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApi {

    @FormUrlEncoded
    @POST("login")
    Call<LoginData> login(
            @Field("email") String email,
            @Field("password") String password
  );
}

