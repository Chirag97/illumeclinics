package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.InvoiceModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by arpit on 25/4/19.
 */

public interface InvoiceAPI {
    @FormUrlEncoded
    @POST("generate_invoice")
    Call<InvoiceModel> invoiceAPI(
            @Field("visit_id") String visit_id);
}