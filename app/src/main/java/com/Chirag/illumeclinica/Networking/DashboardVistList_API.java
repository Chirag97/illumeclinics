package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.DashboardVisitList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DashboardVistList_API {

    @FormUrlEncoded
    @POST("getDashboardVisitList")
    Call<DashboardVisitList> dashboardvisitlist(
            @Field("user_id") String user_id);
}
