package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.AllergiesData;

import retrofit2.Call;

import retrofit2.http.GET;

public interface Allergy_API {

    @GET("getAllergyList")
    Call<AllergiesData> allergydata();

}
