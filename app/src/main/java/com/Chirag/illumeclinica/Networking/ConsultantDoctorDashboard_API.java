package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.ConsultantDoctorLogin;
import com.Chirag.illumeclinica.models.DashboardDoctorsVisitList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ConsultantDoctorDashboard_API {
    @FormUrlEncoded
    @POST("getDashboardConsultantDoctorVisitList")
    Call<ConsultantDoctorLogin> consultantdoctorLogin(
            @Field("consultant_doctor_id") String consultant_doctor_id);
}
