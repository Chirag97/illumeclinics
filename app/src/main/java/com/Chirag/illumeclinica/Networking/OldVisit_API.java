package com.Chirag.illumeclinica.Networking;


import com.Chirag.illumeclinica.models.OldVisit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OldVisit_API {

    @FormUrlEncoded
        @POST("getPatientDetails")
    Call<OldVisit> oldvisit(
            @Field("patient_id") String patient_id
    );
}
