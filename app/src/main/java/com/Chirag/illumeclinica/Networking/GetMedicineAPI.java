package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.MedicineModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetMedicineAPI {
    @GET("getAllMasterDetails")
    Call<MedicineModel> getMedicine();
}