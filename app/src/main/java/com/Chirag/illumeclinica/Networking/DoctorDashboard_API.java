package com.Chirag.illumeclinica.Networking;

import com.Chirag.illumeclinica.models.DashboardDoctorsVisitList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DoctorDashboard_API {


    @FormUrlEncoded
    @POST("getDashboardDoctorVisitList")
    Call<DashboardDoctorsVisitList> dashboardDoctorsVisitList(
            @Field("user_id") String user_id);
}
