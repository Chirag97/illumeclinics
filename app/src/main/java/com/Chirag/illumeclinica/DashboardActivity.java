package com.Chirag.illumeclinica;


import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class DashboardActivity extends BaseActivity {

    TextView tv;
    TextView registernewpatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        init();
    }
    void init(){
        tv=findViewById(R.id.textview);
        tv.setText("Dashboard");

        registernewpatient=findViewById(R.id.txt_register_patients);

    }

}
