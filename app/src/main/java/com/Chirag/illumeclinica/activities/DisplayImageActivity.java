package com.Chirag.illumeclinica.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.Chirag.illumeclinica.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.jsibbold.zoomage.ZoomageView;

import java.io.File;

/**
 * Created by arpit on 24/4/19.
 */

public class DisplayImageActivity extends AppCompatActivity {
    private ImageView ic_cross;
    private ZoomageView iv_image;
    private File file;
    private RequestOptions mImgUserOptions;
    private String url = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_image_activity);
        iv_image = findViewById(R.id.iv_image);
        ic_cross = findViewById(R.id.ic_cross);
        ic_cross.setVisibility(View.GONE);
        mImgUserOptions = new RequestOptions()

//                .placeholder(R.drawable.img_place_holder)
                .error(R.drawable.img_place_holder)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        if (getIntent().hasExtra("img")) {
            file = (File) getIntent().getExtras().get("img");
        }
        if (getIntent().hasExtra("img_url")) {
            url = getIntent().getStringExtra("img_url");
        }
        if (url.isEmpty()) {
            Glide.with(DisplayImageActivity.this)
                    .load(file)
                    .apply(mImgUserOptions)
                    .into(iv_image);
        } else {
            Glide.with(DisplayImageActivity.this)
                    .load(url)
                    .apply(mImgUserOptions)
                    .into(iv_image);
        }

        ic_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
