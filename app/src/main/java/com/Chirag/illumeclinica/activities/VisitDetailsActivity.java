package com.Chirag.illumeclinica.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.BaseActivity;
import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Networking.VisitDetails_API;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.Tessting.CustomTextView;
import com.Chirag.illumeclinica.adapters.DiagnoseVisitDetailsAdapter;
import com.Chirag.illumeclinica.adapters.MedicineListAdapter;
import com.Chirag.illumeclinica.models.ConsvisitList;
import com.Chirag.illumeclinica.models.DoctorVisitList;
import com.Chirag.illumeclinica.models.MedicineList;
import com.Chirag.illumeclinica.models.TestList;
import com.Chirag.illumeclinica.models.VisitDetails;
import com.Chirag.illumeclinica.models.VisitList;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitDetailsActivity extends BaseActivity implements View.OnClickListener {
    TextView tv;
    Button btn;
    ImageView imageView;
    TextView id, dateTime, name, ageandgender, problems, symptoms, consultants;


    VisitDetails_API visitDetails_api;
    List<TestList> testlist;
    List<MedicineList> list;
    RecyclerView medicineview, diagnoseview;
    VisitList visitListData;
    DoctorVisitList doctorVisitList;
    VisitDetails visitDetails;

    ConsvisitList consvisitList;

    private ArrayList<String> imageList = new ArrayList();


    @BindView(R.id.backbutton)
    ImageView backbutton;
    @BindView(R.id.visitdetail)
    CustomTextView visitdetail;
    @BindView(R.id.tool)
    AppBarLayout tool;
    @BindView(R.id.rr1)
    LinearLayout rr1;
    @BindView(R.id.pat)
    CustomTextView pat;

    @BindView(R.id.invoice_status)
    CustomTextView dateandtime;

    @BindView(R.id.problem)
    CustomTextView problem;

    @BindView(R.id.symptom)
    CustomTextView symptom;

    @BindView(R.id.listofproblem)
    CustomTextView listofproblem;
    @BindView(R.id.ll4)
    LinearLayout ll4;
    @BindView(R.id.ll5)
    LinearLayout ll5;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.reffered12)
    CustomTextView reffered12;
    @BindView(R.id.doctor_name)
    TextView doctorName;

    @BindView(R.id.injection)
    ImageView injection;
    @BindView(R.id.ll7)
    LinearLayout ll7;
    @BindView(R.id.diagnoseRecview)
    RecyclerView diagnoseRecview;
    @BindView(R.id.rr5)
    RelativeLayout rr5;
    @BindView(R.id.medicine)
    ImageView medicine;
    @BindView(R.id.ll8)
    LinearLayout ll8;
    @BindView(R.id.ll11)
    LinearLayout ll11;
    @BindView(R.id.medicineRecview)
    RecyclerView medicineRecview;
    @BindView(R.id.rr6)
    RelativeLayout rr6;
    @BindView(R.id.visit_detailsbutton)
    Button visitDetailsbutton;
    @BindView(R.id.scroll)
    ScrollView scroll;
    @BindView(R.id.noPhoto)
    CustomTextView noPhoto;
    @BindView(R.id.img_1)
    ImageView img1;
    @BindView(R.id.img_2)
    ImageView img2;
    @BindView(R.id.img_3)
    ImageView img3;
    @BindView(R.id.img_4)
    ImageView img4;
    @BindView(R.id.img_5)
    ImageView img5;
    @BindView(R.id.img_6)
    ImageView img6;
    @BindView(R.id.dateandtime)
    TextView getAgeandgender;
    @BindView(R.id.allergies)
    TextView allergies;

    @BindView(R.id.patientid)
    CustomTextView patientid;
    @BindView(R.id.createdDate)
    CustomTextView createdDate;
    @BindView(R.id.patientName)
    CustomTextView patientName;
    @BindView(R.id.patientAge)
    CustomTextView patientAge;
    @BindView(R.id.patientGender)
    CustomTextView patientGender;
    @BindView(R.id.patientProblem)
    CustomTextView patientProblem;
    @BindView(R.id.patientSymtomps)
    CustomTextView patientSymtomps;
    @BindView(R.id.patientallergy)
    CustomTextView patientAllergies;
    @BindView(R.id.no_medicine_txt)
    CustomTextView no_medicine_txt;
    @BindView(R.id.no_test_txt)
    CustomTextView no_test_txt;

    @BindView(R.id.diagnose_text)
    CustomTextView diagnose_text;
    @BindView(R.id.txt_suffering_from)
    CustomTextView txt_suffering_from;
    @BindView(R.id.countPhoto)
    CustomTextView countPhoto;



    String p_name = "";
    String doctor_Name = "", doctorname = "";
    private VisitDetailsActivity mActivity;
    private DiagnoseVisitDetailsAdapter diagnosisAdapter;
    private MedicineListAdapter medicineAdaptor;
    private RequestOptions mImgUserOptions;
    private String consultantDoctorName="";
    private String consultantDoctorNumber="";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = VisitDetailsActivity.this;
        hideSoftKeyboard();
        setContentView(R.layout.activity_visit_details);
        ButterKnife.bind(this);
        doctor_Name = PreferenceManager.getInstance(getApplicationContext()).getConsultantDoctor();
        doctorname = PreferenceManager.getInstance(getApplicationContext()).getConsultantDoctorName();



        DialogUtils.showProgressDialog(mActivity, "Please wait...", false);
        Intent intent = getIntent();
        if (intent.hasExtra("visit_data")) {
            if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("hospital")) {
                visitListData = (VisitList) intent.getSerializableExtra("visit_data");
            } else if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("doctor")) {
                if (PreferenceManager.getInstance(mActivity).getDoctorType().equals("Main Doctor")){
                    doctorVisitList = (DoctorVisitList) intent.getSerializableExtra("visit_data");

                }else if (PreferenceManager.getInstance(mActivity).getDoctorType().equals("Consultant Doctor")){
                    consvisitList = (ConsvisitList) intent.getSerializableExtra("visit_data");
                }
            }
        }
        init();
        getVisitDetail();
        mImgUserOptions = new RequestOptions()
                .centerCrop()
                .override(100, 120)
                .placeholder(R.drawable.img_place_holder)
                .error(R.drawable.img_place_holder)
                .diskCacheStrategy(DiskCacheStrategy.ALL);

        if (visitListData != null) {
            patientid.setText(visitListData.getPatientId());
            createdDate.setText(AndroidAppUtils.dateTimeFormat(visitListData.getInvoiceDateTime()));
            p_name = visitListData.getPatientName();
            patientName.setText(ConstantClass.toTitleCase(p_name));
//            doctorName.setText(ConstantClass.toTitleCase(doctor_Name));

        } else if (doctorVisitList != null) {
            patientid.setText(doctorVisitList.getPatientId());
            createdDate.setText(AndroidAppUtils.dateTimeFormat(doctorVisitList.getInvoiceDateTime()));
            p_name = doctorVisitList.getPatientName();
            patientName.setText(ConstantClass.toTitleCase(p_name));
//            doctorName.setText(ConstantClass.toTitleCase(doctorname));

            //name.setText(doctorVisitList.getPatientName());
        } else {
            patientid.setText(consvisitList.getPatientId());
            createdDate.setText(AndroidAppUtils.dateTimeFormat(consvisitList.getInvoiceDateTime()));
            p_name = consvisitList.getPatientName();
            patientName.setText(ConstantClass.toTitleCase(p_name));
//            doctorName.setText(ConstantClass.toTitleCase(doctorname));

        }

        if (visitListData != null && visitListData.getProblemName() != null) {
            if (visitListData.getProblemName().equals(""))
                patientProblem.setText("NA");
            else patientProblem.setText(visitListData.getProblemName());
        } else if (doctorVisitList != null && doctorVisitList.getProblemName() != null) {
            if (doctorVisitList.getProblemName().equals(""))
                patientProblem.setText("NA");
            else patientProblem.setText(doctorVisitList.getProblemName());
        } else if (consvisitList != null && consvisitList.getProblemName() != null) {
            if (consvisitList.getProblemName().equals("")) {
                patientProblem.setText("NA");
            } else {
                patientProblem.setText(consvisitList.getPatientName());
            }
        } else problem.setText("NA");

        if (visitListData != null && visitListData.getSymptom() != null) {
            if (visitListData.getSymptom().equals(""))
                patientSymtomps.setText("NA");
            else patientSymtomps.setText(visitListData.getSymptom());
        } else if (doctorVisitList != null && doctorVisitList.getSymptom() != null) {
            if (doctorVisitList.getSymptom().equals(""))
                patientSymtomps.setText("NA");
            else patientSymtomps.setText(doctorVisitList.getSymptom());

        } else if (consvisitList != null && consvisitList.getSymptom() != null) {
            if (consvisitList.getSymptom().equals(""))
                patientSymtomps.setText("NA");
            else patientSymtomps.setText(consvisitList.getSymptom());
        } else {
            patientSymtomps.setText("NA");
        }
        //consultants.setText(consultant_doctor);
    }

    @SuppressLint("SetTextI18n")
    void init() {
        tv = findViewById(R.id.visitdetail);
        tv.setText("Visit Details");
        imageView = findViewById(R.id.backbutton);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
//                    startActivity(new Intent(VisitDetailsActivity.this, ClinicHomeActivity.class));
                    //gotoReceptionist_Dashboard();
                    finish();
                } else {
//                    startActivity(new Intent(VisitDetailsActivity.this, ClinicHomeActivity.class));
                    //gotoReceptionist_Dashboard();
                    finish();
                }


            }
        });


        id = findViewById(R.id.id);
        dateTime = findViewById(R.id.invoice_status);

        name = findViewById(R.id.name);
        ageandgender = findViewById(R.id.dateandtime);

        problems = findViewById(R.id.problems);
        symptoms = findViewById(R.id.symptoms);
        //consultants=findViewById(R.id.consultants);


        btn = findViewById(R.id.visit_detailsbutton);
        if (visitListData != null) {
            if (visitListData.getInvoiceStatus().equals("1")) {
                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setText("Add Patient Problem");
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                } else {
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    btn.setVisibility(View.GONE);
                }
            } else if (visitListData.getInvoiceStatus().equals("2")) {

                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setVisibility(View.GONE);
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                        ll11.setVisibility(View.GONE);
                        diagnose_text.setVisibility(View.GONE);
                } else {
                    btn.setText("Create Prescription");
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                }
            } else if (visitListData.getInvoiceStatus().equals("3")) {

                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setText("Generate Invoice");
                } else {
                    btn.setText("Generate Invoice");
                }
            }
        } else if (doctorVisitList != null) {
            if (doctorVisitList.getInvoiceStatus().equals("1")) {
                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setText("Add Patient Problem");
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                } else {
                    btn.setVisibility(View.GONE);
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                }
            } else if (doctorVisitList.getInvoiceStatus().equals("2")) {

                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setVisibility(View.GONE);
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                } else {
                    btn.setText("Create Prescription");
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                }
            } else if (doctorVisitList.getInvoiceStatus().equals("3")) {

                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setText("Generate Invoice");
                } else {
                    btn.setText("Generate Invoice");
                }
            }
        }else if (consvisitList != null) {
            if (consvisitList.getVisitStatus().equals("1")) {
                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setText("Add Patient Problem");
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                } else {
                    btn.setVisibility(View.GONE);
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                }
            } else if (consvisitList.getVisitStatus().equals("2")) {

                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setVisibility(View.GONE);
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                } else {
                    btn.setText("Create Prescription");
                    ll7.setVisibility(View.GONE);
                    ll8.setVisibility(View.GONE);
                    rr5.setVisibility(View.GONE);
                    rr6.setVisibility(View.GONE);
                    ll11.setVisibility(View.GONE);
                    diagnose_text.setVisibility(View.GONE);
                }
            } else if (consvisitList.getVisitStatus().equals("3")) {

                if (PreferenceManager.getInstance(VisitDetailsActivity.this).getUserType().equalsIgnoreCase("hospital")) {
                    btn.setText("Generate Invoice");
                } else {
                    btn.setText("Generate Invoice");
                }
            }
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (visitListData != null) {
                    if (visitListData.getInvoiceStatus().equals("1")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, PatientProblemActivity.class);

                        if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("hospital")) {
                            intent.putExtra("patient_id", visitListData.getPatientId());
                            intent.putExtra("visit_id", visitListData.getVisitId());
                            intent.putExtra("allergy", visitDetails.getData().getVisit_list().get(0).getAllergy());

                        } else {
                            intent.putExtra("patient_id", doctorVisitList.getPatientId());
                            intent.putExtra("visit_id", doctorVisitList.getVisitId());
                            intent.putExtra("allergy", visitDetails.getData().getVisit_list().get(0).getAllergy());
                        }

                        startActivity(intent);
                    } else if (visitListData.getInvoiceStatus().equals("2")) {

                        Intent intent = new Intent(VisitDetailsActivity.this, CreatePrescriptionActivity.class);

                        if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("hospital")) {
                            intent.putExtra("visit_data", visitListData);
                        } else {
                            intent.putExtra("visit_data", doctorVisitList);
                        }

                        intent.putStringArrayListExtra("imageList", imageList);
                        startActivity(intent);

                    } else if (visitListData.getInvoiceStatus().equals("3")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, InvoiceActivity.class);
                        if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("hospital")) {
                            intent.putExtra("visit_id", visitListData.getVisitId());
                        } else {
                            intent.putExtra("visit_id", doctorVisitList.getVisitId());
                        }
                        startActivity(intent);
                        finish();
                    }

                } else if (doctorVisitList != null) {
                    if (doctorVisitList.getInvoiceStatus().equals("1")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, PatientProblemActivity.class);

                        if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("hospital")) {
                            intent.putExtra("patient_id", visitListData.getPatientId());
                            intent.putExtra("visit_id", visitListData.getVisitId());
                        } else {
                            intent.putExtra("patient_id", doctorVisitList.getPatientId());
                            intent.putExtra("visit_id", doctorVisitList.getVisitId());
                        }

                        startActivity(intent);
                    } else if (doctorVisitList.getInvoiceStatus().equals("2")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, CreatePrescriptionActivity.class);

                        if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("hospital")) {
                            intent.putExtra("visit_data", visitListData);
                        } else {
                            intent.putExtra("visit_data", doctorVisitList);
                        }
                        intent.putExtra("consultantDoctorName",consultantDoctorName);
                        intent.putExtra("consultantDoctorNumber",consultantDoctorNumber);
                        intent.putStringArrayListExtra("imageList", imageList);
                        startActivity(intent);
                    } else if (doctorVisitList.getInvoiceStatus().equals("3")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, InvoiceActivity.class);
                        if (PreferenceManager.getInstance(mActivity).getUserType().equalsIgnoreCase("hospital")) {
                            intent.putExtra("visit_id", visitListData.getVisitId());
                        } else {
                            intent.putExtra("visit_id", doctorVisitList.getVisitId());
                        }
                        startActivity(intent);
                        finish();

                    }
                }else if (consvisitList != null) {
                    if (consvisitList.getVisitStatus().equals("1")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, PatientProblemActivity.class);
                            intent.putExtra("patient_id", consvisitList.getPatientId());
                            intent.putExtra("visit_id", consvisitList.getVisitId());
                        startActivity(intent);
                    } else if (consvisitList.getVisitStatus().equals("2")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, CreatePrescriptionActivity.class);
                            intent.putExtra("visit_data", consvisitList);
                            intent.putExtra("consultantDoctorName",consultantDoctorName);
                        intent.putExtra("consultantDoctorNumber",consultantDoctorNumber);
                        intent.putStringArrayListExtra("imageList", imageList);
                        startActivity(intent);
                    } else if (consvisitList.getVisitStatus().equals("3")) {
                        Intent intent = new Intent(VisitDetailsActivity.this, InvoiceActivity.class);
                            intent.putExtra("visit_id", consvisitList.getVisitId());
                        startActivity(intent);
                        finish();

                    }
                }

            }
        });
        btn.setTypeface(ConstantClass.getFont(getApplicationContext()));

        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        img5.setOnClickListener(this);
        img6.setOnClickListener(this);

    }

    private void getVisitDetail() {
        visitDetails_api = Utils.getVisitDetailsAPI();
        Call<VisitDetails> visitDetailsCall = null;
        if (visitListData != null) {
            visitDetailsCall = visitDetails_api.getvisitdetails(visitListData.getVisitId());


        } else if (doctorVisitList != null) {
            visitDetailsCall = visitDetails_api.getvisitdetails(doctorVisitList.getVisitId());

        }else if (consvisitList != null) {
            visitDetailsCall = visitDetails_api.getvisitdetails(consvisitList.getVisitId());

        }
        visitDetailsCall.enqueue(new Callback<VisitDetails>() {
            @Override
            public void onResponse(Call<VisitDetails> call, Response<VisitDetails> response) {
                DialogUtils.hideProgressDialog();
                if (response.isSuccessful()) {
                    diagnoseRecview.setLayoutManager(new LinearLayoutManager(VisitDetailsActivity.this));
                    medicineRecview.setLayoutManager(new LinearLayoutManager(VisitDetailsActivity.this));
                    if (response.body().getData().getVisit_list().get(0).getDiagnosis()!=null&&!response.body().getData().getVisit_list().get(0).getDiagnosis().isEmpty()){
                        diagnose_text.setText(response.body().getData().getVisit_list().get(0).getDiagnosis());
                    }else {
                        diagnose_text.setText("No diagnosis provided");
                    }
                    if (response.body().getData().getVisit_list().get(0).getSuffering_from()!=null&&!response.body().getData().getVisit_list().get(0).getSuffering_from().isEmpty()){
                        txt_suffering_from.setText(response.body().getData().getVisit_list().get(0).getSuffering_from());
                        if (doctorVisitList!=null){
                            doctorVisitList.setSufferingFrom(response.body().getData().getVisit_list().get(0).getSuffering_from());
                        }else if (consvisitList!=null){
                            consvisitList.setSufferingFrom(response.body().getData().getVisit_list().get(0).getSuffering_from());
                        }
                    }else {
                        txt_suffering_from.setText("N/A");
                        if (doctorVisitList!=null){
                            doctorVisitList.setSufferingFrom("N/A");
                        }else if (consvisitList!=null){
                            consvisitList.setSufferingFrom("N/A");
                        }
                    }
                    String age = response.body().getData().getVisit_list().get(0).getAge();
                    if (visitListData != null)
                        visitListData.setAge(age);
                    if (doctorVisitList != null)
                        doctorVisitList.setAge(age);
                    if (consvisitList != null)
                        consvisitList.setAge(age);
                    if (!age.isEmpty())
                        patientAge.setText(age + "  Years");
                    else patientAge.setText("N/A");
                    String gender = response.body().getData().getVisit_list().get(0).getGender();
                    patientGender.setText(gender);
                    String allergy;
                    if (response.body().getData().getVisit_list().get(0).getAllergy()!=null&&!response.body().getData().getVisit_list().get(0).getAllergy().isEmpty()) {
                        allergy = response.body().getData().getVisit_list().get(0).getAllergy();
                    }else {
                        allergy="N/A";
                    }

                    patientAllergies.setText(allergy);
                    if (doctorVisitList!=null){
                        doctorVisitList.setAllergy(allergy);
                    }else if (consvisitList!=null){
                        consvisitList.setAllergy(allergy);
                    }
                    diagnosisAdapter = new DiagnoseVisitDetailsAdapter(VisitDetailsActivity.this, response.body().getData().getTest_list());
                    diagnoseRecview.setAdapter(diagnosisAdapter);
                    diagnosisAdapter.notifyDataSetChanged();
                    if (response.body().getData().getTest_list().size() > 0 || response.body().getData().getVisit_list().get(0).getVisit_status().equalsIgnoreCase("2")) {
                        no_test_txt.setVisibility(View.GONE);
                    }else {
                        no_test_txt.setVisibility(View.VISIBLE);
                    }
                    doctorName.setText(response.body().getData().getVisit_list().get(0).getConsultant_doctor_name());
                    consultantDoctorName=response.body().getData().getVisit_list().get(0).getConsultant_doctor_name();
                    consultantDoctorNumber=response.body().getData().getVisit_list().get(0).getMobile_no();

                    medicineAdaptor = new MedicineListAdapter(VisitDetailsActivity.this, response.body().getData().getMedicine_list());
                    medicineRecview.setAdapter(medicineAdaptor);
                    medicineAdaptor.notifyDataSetChanged();
                    if (response.body().getData().getMedicine_list().size() > 0  || response.body().getData().getVisit_list().get(0).getVisit_status().equalsIgnoreCase("2")) {
                        no_medicine_txt.setVisibility(View.GONE);
                    }else {
                        no_medicine_txt.setVisibility(View.VISIBLE);
                    }
                    if (response.body().getData().getImage_list() != null && response.body().getData().getImage_list().size() > 0) {
                        countPhoto.setVisibility(View.VISIBLE);
                        countPhoto.setText(": "+response.body().getData().getImage_list().size());
                        if (response.body().getData().getImage_list().size() <= 3) {
                            ll4.setVisibility(View.VISIBLE);
                            noPhoto.setVisibility(View.GONE);
                        } else {
                            ll4.setVisibility(View.VISIBLE);
                            noPhoto.setVisibility(View.GONE);
                            ll5.setVisibility(View.VISIBLE);
                        }
                        for (int i = 0; i < response.body().getData().getImage_list().size(); i++) {
                            imageList.add(response.body().getData().getImage_list().get(i).getImage_path());
                            switch (i) {
                                case 0:
                                    Glide.with(mActivity).clear(img1);
                                    Glide.with(mActivity)
                                            .load(response.body().getData().getImage_list().get(0).getImage_path())
                                            .apply(mImgUserOptions)
                                            .into(img1);
                                    break;
                                case 1:
                                    Glide.with(mActivity).clear(img2);
                                    Glide.with(mActivity)
                                            .load(response.body().getData().getImage_list().get(1).getImage_path())
                                            .apply(mImgUserOptions)
                                            .into(img2);
                                    break;
                                case 2:
                                    Glide.with(mActivity).clear(img3);
                                    Glide.with(mActivity)
                                            .load(response.body().getData().getImage_list().get(2).getImage_path())
                                            .apply(mImgUserOptions)
                                            .into(img3);
                                    break;
                                case 3:
                                    Glide.with(mActivity).clear(img4);
                                    Glide.with(mActivity)
                                            .load(response.body().getData().getImage_list().get(3).getImage_path())
                                            .apply(mImgUserOptions)
                                            .into(img4);
                                    break;
                                case 4:
                                    Glide.with(mActivity).clear(img5);
                                    Glide.with(mActivity)
                                            .load(response.body().getData().getImage_list().get(4).getImage_path())
                                            .apply(mImgUserOptions)
                                            .into(img5);
                                    break;
                                case 5:
                                    Glide.with(mActivity).clear(img6);
                                    Glide.with(mActivity)
                                            .load(response.body().getData().getImage_list().get(5).getImage_path())
                                            .apply(mImgUserOptions)
                                            .into(img6);
                                    break;
                            }
                        }
                    } else {

                    }

                }
            }

            @Override
            public void onFailure(Call<VisitDetails> call, Throwable t) {
                Toast.makeText(VisitDetailsActivity.this, "Error :" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(VisitDetailsActivity.this, DisplayImageActivity.class);
        switch (view.getId()) {
            case R.id.img_1:
                if (imageList.size() > 0) {
                    intent.putExtra("img_url", imageList.get(0));
                    startActivity(intent);
                }
                break;
            case R.id.img_2:
                if (imageList.size() > 1) {
                    intent.putExtra("img_url", imageList.get(1));
                    startActivity(intent);
                }
                break;
            case R.id.img_3:
                if (imageList.size() > 2) {
                    intent.putExtra("img_url", imageList.get(2));
                    startActivity(intent);
                }
                break;
            case R.id.img_4:
                if (imageList.size() > 3) {
                    intent.putExtra("img_url", imageList.get(3));
                    startActivity(intent);
                }
                break;
            case R.id.img_5:
                if (imageList.size() > 4) {
                    intent.putExtra("img_url", imageList.get(4));
                    startActivity(intent);
                }
                break;
            case R.id.img_6:
                if (imageList.size() > 5) {
                    intent.putExtra("img_url", imageList.get(5));
                    startActivity(intent);
                }
                break;
        }
    }
}
