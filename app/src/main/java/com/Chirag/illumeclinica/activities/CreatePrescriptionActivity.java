package com.Chirag.illumeclinica.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.Adapter.DiagnosisAdapter;
import com.Chirag.illumeclinica.Adapter.MedicineAdaptor;
import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.CreatePrescriptionAPI;
import com.Chirag.illumeclinica.Networking.GetMedicineAPI;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.Tessting.CustomTextView;
import com.Chirag.illumeclinica.Ui.CreatePrescription;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.listeners.ItemClickListener;
import com.Chirag.illumeclinica.listeners.ItemRemoveClickListener;
import com.Chirag.illumeclinica.models.ConsvisitList;
import com.Chirag.illumeclinica.models.CreateNewPrescription;
import com.Chirag.illumeclinica.models.DoctorVisitList;
import com.Chirag.illumeclinica.models.MedicineModel;
import com.Chirag.illumeclinica.models.MedicinesIds;
import com.Chirag.illumeclinica.models.VisitList;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePrescriptionActivity extends Activity implements ItemClickListener, ItemRemoveClickListener, View.OnClickListener {
    @BindView(R.id.backbutton)
    ImageView backbutton;
    @BindView(R.id.patientid)
    CustomTextView patientid;
    @BindView(R.id.createdDate)
    CustomTextView createdDate;
    @BindView(R.id.patientName)
    CustomTextView patientName;
    @BindView(R.id.patientAge)
    CustomTextView patientAge;
    @BindView(R.id.patientGender)
    CustomTextView patientGender;
    @BindView(R.id.patientProblem)
    CustomTextView patientProblem;
    @BindView(R.id.patientSymtomps)
    CustomTextView patientSymtomps;
    @BindView(R.id.doctorName)
    TextView doctorName;
    @BindView(R.id.call)
    ImageView call;
    @BindView(R.id.message)
    ImageView message;
    @BindView(R.id.ic_test_add)
    ImageView icTestAdd;
    @BindView(R.id.diagnosis_recycler_view)
    RecyclerView diagnosisRecyclerView;
    @BindView(R.id.ic_medicine_add)
    ImageView icMedicineAdd;
    @BindView(R.id.medicine_recycler_view)
    RecyclerView medicineRecyclerView;
    @BindView(R.id.submit)
    Button submit;

    @BindView(R.id.diagnose_text)
    AppCompatAutoCompleteTextView diagnose_text;

    @BindView(R.id.noPhoto)
    CustomTextView noPhoto;
    @BindView(R.id.img_1)
    ImageView img1;
    @BindView(R.id.img_2)
    ImageView img2;
    @BindView(R.id.img_3)
    ImageView img3;
    @BindView(R.id.img_4)
    ImageView img4;
    @BindView(R.id.img_5)
    ImageView img5;
    @BindView(R.id.img_6)
    ImageView img6;
    @BindView(R.id.ll8)
    LinearLayout ll8;
    @BindView(R.id.ll7)
    LinearLayout ll7;
    @BindView(R.id.txt_suffering_from)
    CustomTextView txt_suffering_from;
    @BindView(R.id.patientallergy)
    CustomTextView patientAllergies;
@BindView(R.id.iv1)
ImageView iv1;
    @BindView(R.id.countPhoto)
    CustomTextView countPhoto;

    private VisitList visitListData;
    private DoctorVisitList doctorVisitList;
    private GetMedicineAPI getMedicineAPI;
    private CreatePrescriptionAPI createPrescriptionAPI;
    private String TAG = CreatePrescriptionActivity.class.getSimpleName();
    private List<MedicineModel.DataBean.TestListBean> testList = new ArrayList<>();
    private List<MedicineModel.DataBean.IntakeListBean> intakeList = new ArrayList<>();
    private List<MedicineModel.DataBean.MedicineListBean> medicineList = new ArrayList<>();
    private List<MedicineModel.DataBean.DiagnosisListBean> diagnosisList = new ArrayList<>();
    private List<MedicineModel.DataBean.TestListBean> selectedTestList = new ArrayList<>();
    private List<MedicinesIds> medicinesIdsList = new ArrayList<>();
    private DiagnosisAdapter diagnosisAdapter;
    private MedicineAdaptor medicineAdaptor;
    String diagnosis = "";
    String p_number = "";
    String doctor_Name = "", doctorname = "";

    ArrayList<String> medicine_type = new ArrayList<>();
    ArrayList<String> intake_type = new ArrayList<>();

    String allergy;

    private ArrayList<String> imageList = new ArrayList<>();
    private Activity mActivity;
    private RequestOptions mImgUserOptions;
    private Typeface typeface;
    private ConsvisitList consvisitList;
    private String consultantDoctorName="";
    private String consultantDoctorNumber="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_prescription_activity);
        ButterKnife.bind(this);

        mActivity = CreatePrescriptionActivity.this;
        mImgUserOptions = new RequestOptions()
                .centerCrop()
                .override(100, 120)
                .placeholder(R.drawable.img_place_holder)
                .error(R.drawable.img_place_holder)
                .diskCacheStrategy(DiskCacheStrategy.ALL);


        getMedicineAPI = Utils.getMedicineAPI();
        createPrescriptionAPI = Utils.createPrescriptionAPI();
        getIntentData();
        initViews();
        getMedicineData();

        doctor_Name = PreferenceManager.getInstance(getApplicationContext()).getConsultantDoctor();
        diagnose_text.setTypeface(ConstantClass.getFont(getApplicationContext()));

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callConsultantDoctor();
            }
        });
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtils.showAlertBox(mActivity, false, "", "Coming soon ...", R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                    @Override
                    public void dialogPositiveButton() {

                    }

                    @Override
                    public void dialogNegativeButton() {

                    }
                });
            }
        });
        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        img5.setOnClickListener(this);
        img6.setOnClickListener(this);

    }

    private void getMedicineData() {
        Call<MedicineModel> medicine = getMedicineAPI.getMedicine();
        medicine.enqueue(new Callback<MedicineModel>() {
            @Override
            public void onResponse(Call<MedicineModel> call, Response<MedicineModel> response) {
                AndroidAppUtils.showLog(TAG, "response ::" + response.body());
                if (response.isSuccessful()) {
                    MedicineModel medicineModel = response.body();
                    testList = medicineModel.getData().getTest_list();
                    intakeList = medicineModel.getData().getIntake_list();
                    medicineList = medicineModel.getData().getMedicine_list();
                    diagnosisList = medicineModel.getData().getDiagnosis_list();

                    ArrayList<String> diagnosisString = new ArrayList<>();
                    for (int i = 0; i < diagnosisList.size(); i++) {
                        diagnosisString.add(diagnosisList.get(i).getDiagnosis());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, diagnosisString) {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                            TextView v = (TextView) super.getView(position, convertView, parent);
                            v.setTypeface(typeface);
                            return v;
                        }

                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            TextView v = (TextView) super.getView(position, convertView, parent);
                            v.setTypeface(typeface);
                            return v;
                        }
                    };
                    diagnose_text.setAdapter(adapter);

                    diagnose_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                            d = diagnose_text.getText().toString();
                        }
                    });
                    iv1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            diagnose_text.showDropDown();
                        }
                    });
                    diagnosisAdapter = new DiagnosisAdapter(CreatePrescriptionActivity.this, CreatePrescriptionActivity.this);
                    diagnosisRecyclerView.setAdapter(diagnosisAdapter);

                    medicineAdaptor = new MedicineAdaptor(CreatePrescriptionActivity.this, CreatePrescriptionActivity.this, intakeList, medicineList);
                    medicineRecyclerView.setAdapter(medicineAdaptor);
                }
            }

            @Override
            public void onFailure(Call<MedicineModel> call, Throwable t) {

            }
        });
    }

    private void getIntentData() {
        Intent intent = getIntent();
        allergy = intent.getStringExtra("allergy");

        if (intent.hasExtra("visit_data")) {
            if (PreferenceManager.getInstance(this).getUserType().equalsIgnoreCase("hospital")) {
                visitListData = (VisitList) intent.getSerializableExtra("visit_data");

            } else {
                if (PreferenceManager.getInstance(mActivity).getDoctorType().equals("Main Doctor")){
                    doctorVisitList = (DoctorVisitList) intent.getSerializableExtra("visit_data");

                }else if (PreferenceManager.getInstance(mActivity).getDoctorType().equals("Consultant Doctor")) {
                    consvisitList = (ConsvisitList) intent.getSerializableExtra("visit_data");

                }
                consultantDoctorName=intent.getStringExtra("consultantDoctorName");
                consultantDoctorNumber=intent.getStringExtra("consultantDoctorNumber");
            }
        }
        if (intent.hasExtra("imageList")) {
            imageList = intent.getStringArrayListExtra("imageList");
        }
    }

    private void initViews() {
        if (imageList.size() != 0) {
            countPhoto.setVisibility(View.VISIBLE);
            countPhoto.setText(": "+imageList.size());
            if (imageList.size() <= 3) {
                ll7.setVisibility(View.VISIBLE);
                noPhoto.setVisibility(View.GONE);
            } else {
                ll7.setVisibility(View.VISIBLE);
                noPhoto.setVisibility(View.GONE);
                ll8.setVisibility(View.VISIBLE);
            }
        }
        for (int i = 0; i < imageList.size(); i++) {
            switch (i) {
                case 0:
                    Glide.with(mActivity).clear(img1);
                    Glide.with(mActivity)
                            .load(imageList.get(0))
                            .apply(mImgUserOptions)
                            .into(img1);
                    break;
                case 1:
                    Glide.with(mActivity).clear(img2);
                    Glide.with(mActivity)
                            .load(imageList.get(1))
                            .apply(mImgUserOptions)
                            .into(img2);
                    break;
                case 2:
                    Glide.with(mActivity).clear(img3);
                    Glide.with(mActivity)
                            .load(imageList.get(2))
                            .apply(mImgUserOptions)
                            .into(img3);
                    break;
                case 3:
                    Glide.with(mActivity).clear(img4);
                    Glide.with(mActivity)
                            .load(imageList.get(3))
                            .apply(mImgUserOptions)
                            .into(img4);
                    break;
                case 4:
                    Glide.with(mActivity).clear(img5);
                    Glide.with(mActivity)
                            .load(imageList.get(4))
                            .apply(mImgUserOptions)
                            .into(img5);
                    break;
                case 5:
                    Glide.with(mActivity).clear(img6);
                    Glide.with(mActivity)
                            .load(imageList.get(5))
                            .apply(mImgUserOptions)
                            .into(img6);
                    break;
            }
        }

        diagnosisRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        medicineRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        submit.setTypeface(ConstantClass.getFont(getApplicationContext()));
        if (PreferenceManager.getInstance(this).getUserType().equalsIgnoreCase("hospital")) {
            patientid.setText(visitListData.getPatientId());
            String namee = visitListData.getPatientName();
            patientName.setText(ConstantClass.toTitleCase(visitListData.getPatientName()));
            //patientName.setText(visitListData.getPatientName());
            if (visitListData.getAge().isEmpty())
                patientAge.setText("N/A");
            else patientAge.setText(visitListData.getAge() + " Years");
            patientGender.setText(visitListData.getGender());
            createdDate.setText((AndroidAppUtils.dateTimeFormat(visitListData.getInvoiceDateTime())));
            //createdDate.setText(visitListData.getInvoiceDateTime());
            patientProblem.setText(visitListData.getProblemName());
            patientSymtomps.setText(visitListData.getSymptom());
            doctorname = PreferenceManager.getInstance(getApplicationContext()).getConsultantDoctorName();
            doctorName.setText(doctorname);

        } else {
            if (PreferenceManager.getInstance(mActivity).getDoctorType().equals("Main Doctor")){
                patientid.setText(doctorVisitList.getPatientId());
                patientName.setText(ConstantClass.toTitleCase(doctorVisitList.getPatientName()));
                if (doctorVisitList.getAge().isEmpty())
                    patientAge.setText("N/A");
                else patientAge.setText(doctorVisitList.getAge() + " Years");
                patientGender.setText(doctorVisitList.getGender());
                createdDate.setText((AndroidAppUtils.dateTimeFormat(doctorVisitList.getInvoiceDateTime())));
                //createdDate.setText(doctorVisitList.getInvoiceDateTime());
                patientProblem.setText(doctorVisitList.getProblemName());
                patientSymtomps.setText(doctorVisitList.getSymptom());
                doctorName.setText(consultantDoctorName);
                txt_suffering_from.setText(doctorVisitList.getSufferingFrom());
                patientAllergies.setText(doctorVisitList.getAllergy());
            }else if (PreferenceManager.getInstance(mActivity).getDoctorType().equals("Consultant Doctor")){
                patientid.setText(consvisitList.getPatientId());
                patientName.setText(ConstantClass.toTitleCase(consvisitList.getPatientName()));
                if (consvisitList.getAge().isEmpty())
                    patientAge.setText("N/A");
                else patientAge.setText(consvisitList.getAge() + " Years");
                patientGender.setText(consvisitList.getGender());
                createdDate.setText((AndroidAppUtils.dateTimeFormat(consvisitList.getInvoiceDateTime())));
                patientProblem.setText(consvisitList.getProblemName());
                patientSymtomps.setText(consvisitList.getSymptom());
                doctorName.setText(consultantDoctorName);
                txt_suffering_from.setText(consvisitList.getSufferingFrom());
                patientAllergies.setText(consvisitList.getAllergy());
            }


        }
    }

    @OnClick({R.id.backbutton, R.id.ic_test_add, R.id.ic_medicine_add, R.id.submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backbutton:
                onBackPressed();
                break;
            case R.id.ic_test_add:
                DialogUtils.showSelectDiagnosisPopUp(CreatePrescriptionActivity.this, testList, this);
                break;
            case R.id.ic_medicine_add:
                DialogUtils.showSelectMedicinePopUp(CreatePrescriptionActivity.this, medicineList, intakeList, this);
                break;
            case R.id.submit:
                createPrescription();
                break;
        }
    }

    private void createPrescription() {
        Call<CreateNewPrescription> createNewPrescriptionCall = null;
        StringBuilder test_ids = new StringBuilder();
        for (int i = 0; i < selectedTestList.size(); i++) {
            if (i == selectedTestList.size() - 1) {
                test_ids.append(selectedTestList.get(i).getId());
            } else
                test_ids.append(selectedTestList.get(i).getId()).append(",");
        }

        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < medicinesIdsList.size(); i++) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("medicine_id", medicinesIdsList.get(i).getMedicine_id());
                jsonObject.put("intake_id", medicinesIdsList.get(i).getIntake_id());
                jsonObject.put("duration", medicinesIdsList.get(i).getDuration());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        diagnosis = diagnose_text.getText().toString();
        if (visitListData != null) {
            createNewPrescriptionCall = createPrescriptionAPI.createNewPrescription(PreferenceManager.getInstance(CreatePrescriptionActivity.this).getUserID(), visitListData.getPatientId(), visitListData.getVisitId(), test_ids.toString(), jsonArray.toString(), diagnosis);

        } else if (doctorVisitList != null) {
            createNewPrescriptionCall = createPrescriptionAPI.createNewPrescription(PreferenceManager.getInstance(CreatePrescriptionActivity.this).getUserID(), doctorVisitList.getPatientId(), doctorVisitList.getVisitId(), test_ids.toString(), jsonArray.toString(), diagnosis);

        }else if(consvisitList != null) {
            createNewPrescriptionCall = createPrescriptionAPI.createNewPrescription(PreferenceManager.getInstance(CreatePrescriptionActivity.this).getUserID(), consvisitList.getPatientId(), consvisitList.getVisitId(), test_ids.toString(), jsonArray.toString(), diagnosis);

        }
        DialogUtils.showProgressDialog(mActivity, "Please wait ...", false);
        createNewPrescriptionCall.enqueue(new Callback<CreateNewPrescription>() {
            @Override
            public void onResponse(Call<CreateNewPrescription> call, Response<CreateNewPrescription> response) {
                DialogUtils.hideProgressDialog();
                if (response.isSuccessful()) {
                    AndroidAppUtils.showLog(CreatePrescriptionActivity.class.getSimpleName(), response.body().toString());
                    DialogUtils.showAlertBox(CreatePrescriptionActivity.this, false, "", response.body().getData().getMessage(), R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                        @Override
                        public void dialogPositiveButton() {
                            startActivity(new Intent(CreatePrescriptionActivity.this, DoctorHomeActivity.class));
                            finishAffinity();
                        }

                        @Override
                        public void dialogNegativeButton() {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<CreateNewPrescription> call, Throwable t) {

            }
        });
    }

    @Override
    public void itemClickListener(int position, View view) {
        DialogUtils.dismissDialog();
        if (!selectedTestList.contains(testList.get(position))) {
            selectedTestList.add(testList.get(position));
            diagnosisAdapter.refreshList(selectedTestList);
        } else {
            AndroidAppUtils.showToast(CreatePrescriptionActivity.this, "Diagnosis Test Already Exists.");
        }
    }

    @Override
    public void itemMedicineClickListener(int medicinePosition, int intakePosition, int daysPosition) {
        MedicinesIds medicinesIds = new MedicinesIds();
        medicinesIds.setMedicine_id(medicineList.get(medicinePosition).getId());
        medicinesIds.setIntake_id(intakeList.get(intakePosition).getId());
        medicinesIds.setDuration(daysPosition + "");

        if (medicinesIdsList.size() == 0) {
            medicinesIdsList.add(medicinesIds);
            medicineAdaptor.refreshList(medicinesIdsList);
        } else {
            for (int i = 0; i < medicinesIdsList.size(); i++) {
                if (!medicinesIdsList.contains(medicinesIds)) {
                    medicinesIdsList.add(medicinesIds);
                    medicineAdaptor.refreshList(medicinesIdsList);
                }
            }
        }
    }

    @Override
    public void itemClickOfDropDown(int type, String title, boolean isChecked) {
    }

    @Override
    public void itemMedicince(String medicine, String intake, int daysPosition) {

    }

    @Override
    public void selectedItems(String selectedItems) {

    }

    @Override
    public void itemRemoveClickListener(int position, View view, int type) {
        switch (type) {
            case 0:
                if (selectedTestList != null && selectedTestList.size() > 0 && selectedTestList.size() > position) {
                    selectedTestList.remove(position);
                    diagnosisAdapter.refreshList(selectedTestList);
                }
                break;
            case 1:
                if (medicinesIdsList != null && medicinesIdsList.size() > 0 && medicinesIdsList.size() > position) {
                    medicinesIdsList.remove(position);
                    medicineAdaptor.refreshList(medicinesIdsList);
                }
                break;
        }
    }

    private void callConsultantDoctor() {
if (PreferenceManager.getInstance(mActivity).getDoctorType().equals("Consultant Doctor")){
    DialogUtils.showAlertBox(mActivity, false, "", "Coming soon ...", R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
        @Override
        public void dialogPositiveButton() {

        }

        @Override
        public void dialogNegativeButton() {

        }
    });
}else {
        Intent myIntent = new Intent(Intent.ACTION_DIAL);
        String phNum = "tel:" + consultantDoctorNumber;
        myIntent.setData(Uri.parse(phNum));
        startActivity(myIntent);
    }}

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(CreatePrescriptionActivity.this, DisplayImageActivity.class);
        switch (view.getId()) {
            case R.id.img_1:
                if (imageList.size() > 0) {
                    intent.putExtra("img_url", imageList.get(0));
                    startActivity(intent);
                }
                break;
            case R.id.img_2:
                if (imageList.size() > 1) {
                    intent.putExtra("img_url", imageList.get(1));
                    startActivity(intent);
                }
                break;
            case R.id.img_3:
                if (imageList.size() > 2) {
                    intent.putExtra("img_url", imageList.get(2));
                    startActivity(intent);
                }
                break;
            case R.id.img_4:
                if (imageList.size() > 3) {
                    intent.putExtra("img_url", imageList.get(3));
                    startActivity(intent);
                }
                break;
            case R.id.img_5:
                if (imageList.size() > 4) {
                    intent.putExtra("img_url", imageList.get(4));
                    startActivity(intent);
                }
                break;
            case R.id.img_6:
                if (imageList.size() > 5) {
                    intent.putExtra("img_url", imageList.get(5));
                    startActivity(intent);
                }
                break;
        }
    }
}
