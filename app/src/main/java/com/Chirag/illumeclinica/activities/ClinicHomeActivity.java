package com.Chirag.illumeclinica.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.fragments.DashBoardFragment;
import com.Chirag.illumeclinica.fragments.NavDrawerFragment;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;

public class ClinicHomeActivity extends AppCompatActivity implements View.OnClickListener {
    public static ClinicHomeActivity mInstance;
    public Fragment currentFragment;
    String username = "";
    private String TAG = ClinicHomeActivity.class.getSimpleName();
    private NavDrawerFragment navDrawerFragment;
    private DrawerLayout drawer;
    private Toolbar mToolbar;
    private TextView tvTitle;
    private ImageView tvLeftFirstIcon;
    private ActionBarDrawerToggle drawerToggle;
    private DashBoardFragment mDashBoardFragment;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mInstance = this;
        initViews();
        assignClick();
        initDrawer();

        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
    }

    private void initViews() {
        navDrawerFragment = (NavDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navfrag);
        drawer = findViewById(R.id.drawer);
        mToolbar = findViewById(R.id.toolbar);
        tvTitle = mToolbar.findViewById(R.id.tvTitle);

        //tvTitle.setText("Welcome!!.. " + PreferenceManager.getInstance(this).getUserName());
        tvTitle.setText("IllumeClinics");

        tvLeftFirstIcon = mToolbar.findViewById(R.id.tvLeftFirstIcon);
        mDashBoardFragment = new DashBoardFragment();
        addFragment(mDashBoardFragment);
    }

    private void addFragment(Fragment fragment) {
        currentFragment = fragment;

        AndroidAppUtils.showLog(TAG, "current fragment in addFragment() is:: " + currentFragment);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        if (!(currentFragment instanceof DashBoardFragment)) {
//            ft.addToBackStack(fragment.getClass().getName());
//        }
        AndroidAppUtils.showLog(TAG, "Size of BackStackTrace in addFragment() is:: " + getSupportFragmentManager().getBackStackEntryCount());
        ft.add(R.id.home_activity_frame, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    /*Method to replace fragment*/
    public void replaceFragment(Fragment fragment) {
        currentFragment = fragment;

        AndroidAppUtils.showLog(TAG, "current fragment in replaceFragment() is:: " + currentFragment);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        AndroidAppUtils.showLog(TAG, "Size of BackStackTrace in removeFragment() is:: " + getSupportFragmentManager().getBackStackEntryCount());
        ft.replace(R.id.home_activity_frame, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    private void assignClick() {
        tvLeftFirstIcon.setOnClickListener(this);
    }


    /**
     * initialise the navigation drawer
     */
    private void initDrawer() {
        navDrawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer));
        drawerToggle = new ActionBarDrawerToggle(this, drawer, mToolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.syncState();

        drawer.addDrawerListener(drawerToggle);
        drawer.post(new Runnable() {
            @Override
            public void run() {
                drawerToggle.syncState();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLeftFirstIcon:
                if (drawer != null) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
                break;
        }
    }

}
