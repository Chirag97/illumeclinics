package com.Chirag.illumeclinica.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;

public class SplashActivity extends Activity {
    TextView titl1e1, title2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidAppUtils.activateFullScreen(this);
        setContentView(R.layout.splash_activity);

        titl1e1 = findViewById(R.id.appName);
        titl1e1.setTypeface(ConstantClass.getFont(getApplicationContext()));
        title2 = findViewById(R.id.appType);
        title2.setTypeface(ConstantClass.getFont(getApplicationContext()
        ));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PreferenceManager.getInstance(SplashActivity.this).getLoginStatus()) {

                    if (PreferenceManager.getInstance(SplashActivity.this).getUserType().equalsIgnoreCase("doctor")) {
                        Intent intent = new Intent(SplashActivity.this, DoctorHomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashActivity.this, ClinicHomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                    finish();
                }
            }
        }, 3000);
    }
}
