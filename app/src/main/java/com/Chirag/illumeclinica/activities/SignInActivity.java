package com.Chirag.illumeclinica.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.LoginApi;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.models.LoginData;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends Activity implements View.OnClickListener {

    //    @BindView(R.id.tv_forget_password)
    TextView tv_forget_password;
    // @BindView(R.id.bt_sign_in)
    Button bt_sign_in;
    //@BindView(R.id.email_et)
    EditText email_et;
    //  @BindView(R.id.password_et)
    EditText password_et;
    // @BindView(R.id.emailWrapper)
    TextInputLayout emailWrapper;
    //  @BindView(R.id.passwordWrapper)
    TextInputLayout passwordWrapper;
    TextView title_2;
    TextView toolview;

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        tv_forget_password = findViewById(R.id.tv_forget_password);



        bt_sign_in = findViewById(R.id.bt_sign_in);
        bt_sign_in.setTypeface(ConstantClass.getFont(getApplicationContext()));


        email_et = findViewById(R.id.email_et);
        email_et.setTypeface(ConstantClass.getFont(getApplicationContext()));

        password_et = findViewById(R.id.password_et);
        password_et.setTypeface(ConstantClass.getFont(getApplicationContext()));

        emailWrapper = findViewById(R.id.emailWrapper);
        passwordWrapper = findViewById(R.id.passwordWrapper);

        toolview = findViewById(R.id.textview);
        toolview.setText("Sign In");

        ButterKnife.bind(this);
        mActivity = this;
        assignClick();
    }

    private void assignClick() {
        bt_sign_in.setOnClickListener(this);
        tv_forget_password.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_forget_password:
                break;
            case R.id.bt_sign_in:
                validate();
                break;
        }
    }

    /**
     * validate form credentials
     */
    public void validate() {

        String password = password_et.getText().toString().trim();
        String email = email_et.getText().toString().trim();

        if (email.isEmpty()) {
            emailWrapper.setError(getString(R.string.email_empty));
        } else if (password.isEmpty()) {
            emailWrapper.setError(null);
            passwordWrapper.setError(getString(R.string.pwd_empty));
        } else {
            emailWrapper.setError(null);
            passwordWrapper.setError(null);
            if (AndroidAppUtils.isOnline(this))
                performLogin(email, password);
            else {
                AndroidAppUtils.showToast(mActivity, getString(R.string.check_internet_connection));
            }
        }
//        if (email.isEmpty()) {
//            emailWrapper.setError(getString(R.string.email_empty));
//        }
// else if (!AndroidAppUtils.isEmailIDValidate(email) || email_et.getText().toString().contains(" ")) {
//            emailWrapper.setError(getString(R.string.valid_email));
//
//        } else if (password.isEmpty()) {
//            emailWrapper.setError(null);
//            passwordWrapper.setError(getString(R.string.pwd_empty));
//
//        } else if (password.length() < 6 || password.length() > 20) {
//            emailWrapper.setError(null);
//            passwordWrapper.setError(getString(R.string.pwd_length));
//
//
//        } else {
//            emailWrapper.setError(null);
//            passwordWrapper.setError(null);
//
//            tv_forget_password.setOnClickListener(null);
//        if (AndroidAppUtils.isOnline(this))
//            performLogin(email, password);
//        else {
//            AndroidAppUtils.showToast(mActivity, getString(R.string.check_internet_connection));
//        }

//        }
    }

    private void performLogin(String email, String password) {
        DialogUtils.showProgressDialog(mActivity, getString(R.string.please_wait), false);


        LoginApi loginApi = Utils.getLoginAPI();

        Call<LoginData> data = loginApi.login(email, password);
        data.enqueue(new Callback<LoginData>() {
            private static final String TAG = "illume";

            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if (response.isSuccessful() && response != null && response.body().getData() != null) {


                    DialogUtils.hideProgressDialog();
                    String doctorType="";
                    String userid = response.body().getData().getUserId();
                    String usertype = response.body().getData().getUserType();
                    String consultantdoctorname= response.body().getData().getConsultantDoctorName();
                    String consultantname= response.body().getData().getConsultant_DoctorName();
                    if (response.body().getData().getDoctor_type()!=null)
                     doctorType= response.body().getData().getDoctor_type();


                    String sessionToken = response.body().getData().getLogintoken();
                    Log.d(TAG, "Response " + response);
//                    Main Doctor
//                    Consultant Doctor
                    PreferenceManager.getInstance(mActivity).setUserID(userid);
                    PreferenceManager.getInstance(mActivity).setUserName(response.body().getData().getUserName());
                    PreferenceManager.getInstance(mActivity).setUserType(usertype);
                    PreferenceManager.getInstance(mActivity).setDoctorType(doctorType);
                    PreferenceManager.getInstance(mActivity).setLoginStatus(true);
                    PreferenceManager.getInstance(mActivity).setSessionToken(sessionToken);
                    PreferenceManager.getInstance(mActivity).setConsultantDoctor(consultantdoctorname);
                    PreferenceManager.getInstance(mActivity).setConsultantDoctorName(consultantname);
                    PreferenceManager.getInstance(mActivity).setConsultantDoctorNumber(response.body().getData().getConsultantDoctorPhoneNo());

                    //Sending User_ID to ADD visit Activity

                    if (PreferenceManager.getInstance(SignInActivity.this).getUserType().equalsIgnoreCase("doctor")) {
                        Intent intent = new Intent(SignInActivity.this, DoctorHomeActivity.class);
                        intent.putExtra("userid", userid);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SignInActivity.this, ClinicHomeActivity.class);
                        intent.putExtra("userid", userid);
                        startActivity(intent);
                        finish();
                    }

                } else {
                    DialogUtils.hideProgressDialog();
                    DialogUtils.showAlertBox(SignInActivity.this, false, "", getResources().getString(R.string.invalid_credentials), R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                        @Override
                        public void dialogPositiveButton() {

                        }
                        @Override
                        public void dialogNegativeButton() {

                        }
                    });
                }
            }

            @Override

            public void onFailure(Call<LoginData> call, Throwable t) {
                DialogUtils.hideProgressDialog();
                AndroidAppUtils.showToast(mActivity, "Error Occured" + t.getMessage());
            }
        });
    }

}
