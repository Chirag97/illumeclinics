package com.Chirag.illumeclinica.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.Networking.AddPatientProblemAPI;
import com.Chirag.illumeclinica.Networking.Allergy_API;
import com.Chirag.illumeclinica.Networking.ImageUploadAPI;
import com.Chirag.illumeclinica.Networking.Problems_API;
import com.Chirag.illumeclinica.Networking.Symptoms_API;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Prefrences.PreferenceManager;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.Tessting.CustomTextView;
import com.Chirag.illumeclinica.adapters.MyAdapter;
import com.Chirag.illumeclinica.iHelper.AppHelper;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.listeners.ItemClickListener;
import com.Chirag.illumeclinica.models.AddPatientProblem;
import com.Chirag.illumeclinica.models.AllergiesData;
import com.Chirag.illumeclinica.models.ImageModel;
import com.Chirag.illumeclinica.models.ProblemList;
import com.Chirag.illumeclinica.models.StateVO;
import com.Chirag.illumeclinica.models.SymptomsList;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientProblemActivity extends Activity implements ItemClickListener {

    private static final String TAG = PatientProblemActivity.class.getSimpleName();
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tool)
    AppBarLayout tool;
    @BindView(R.id.rr1)
    LinearLayout rr1;
    @BindView(R.id.problemSpinner)
    Spinner problemSpinner;
    @BindView(R.id.problemWrapper)
    LinearLayout problemWrapper;
    @BindView(R.id.symptopmSpinner)
    Spinner symptopmSpinner;
    @BindView(R.id.symptopmWrapper)
    LinearLayout symptopmWrapper;
    @BindView(R.id.allergiesSpinner)
    Spinner allergiesSpinner;
    @BindView(R.id.allergiesWrapper)
    LinearLayout allergiesWrapper;
    @BindView(R.id.durationSpinner)
    Spinner durationSpinner;
    @BindView(R.id.durationWrapper)
    LinearLayout durationWrapper;
    @BindView(R.id.runningtreatment)
    CustomTextView runningtreatment;
    @BindView(R.id.swtich)
    SwitchCompat swtich;
    @BindView(R.id.ll3)
    LinearLayout ll3;
    @BindView(R.id.from)
    MaterialEditText from;
    @BindView(R.id.treatmentDurationSpinner)
    Spinner treatmentDurationSpinner;
    @BindView(R.id.treatmentDurationWrapper)
    LinearLayout treatmentDurationWrapper;
    @BindView(R.id.medication)
    MaterialEditText medication;
    @BindView(R.id.ll6)
    LinearLayout ll6;
    @BindView(R.id.addproblemimage)
    ImageView addproblemimage;
    @BindView(R.id.add_photo)
    CustomTextView addPhoto;
    @BindView(R.id.listofphoto)
    CustomTextView listofphoto;
    @BindView(R.id.noPhoto)
    CustomTextView noPhoto;
    @BindView(R.id.ll4)
    LinearLayout ll4;
    @BindView(R.id.ll5)
    LinearLayout ll5;
    @BindView(R.id.submitProblem)
    Button submit;
    @BindView(R.id.mTvSymtoms)
    TextView mTvSymtoms;
    @BindView(R.id.mTvAlleries)
    TextView mTvAlleries;
    @BindView(R.id.img_1)
    ImageView img1;
    @BindView(R.id.img_2)
    ImageView img2;
    @BindView(R.id.img_3)
    ImageView img3;
    @BindView(R.id.img_4)
    ImageView img4;
    @BindView(R.id.img_5)
    ImageView img5;
    @BindView(R.id.img_6)
    ImageView img6;
    @BindView(R.id.tv_camera)
    TextView tvCamera;
    @BindView(R.id.tv_photo_library)
    TextView tvPhotoLibrary;
    @BindView(R.id.tv_camera_cancel_btn)
    TextView tvCameraCancelBtn;
    @BindView(R.id.rl_camera_options)
    RelativeLayout rlCameraOptions;
    String problemss = "";

    //For auto complete Text View
    @BindView(R.id.problemautotextview)
    AutoCompleteTextView autoCompleteTextView;
    ArrayList<String> Problems = new ArrayList<>();
    String ids = "";
    String oka = "";

    @BindView(R.id.symptomautocompletetextview)
    MultiAutoCompleteTextView multiAutoCompleteTextView;
    ArrayList<String> Symptoms = new ArrayList<>();

    @BindView(R.id.allergyautocompletetextview)
    MultiAutoCompleteTextView multitext;
    ArrayList<String> Allergy = new ArrayList<>();

    @BindView(R.id.iv1)
    ImageView iv1;
    @BindView(R.id.iv2)
    ImageView iv2;
    @BindView(R.id.iv3)
    ImageView iv3;


    @BindView(R.id.cross_1)
    ImageView cross_1;
    @BindView(R.id.cross_2)
    ImageView cross_2;
    @BindView(R.id.cross_3)
    ImageView cross_3;
    @BindView(R.id.cross_5)
    ImageView cross_5;
    @BindView(R.id.cross_6)
    ImageView cross_6;
    @BindView(R.id.cross_4)
    ImageView cross_4;
    @BindView(R.id.rr_img_1)
    RelativeLayout rr_img_1;
    @BindView(R.id.rr_img_2)
    RelativeLayout rr_img_2;
    @BindView(R.id.rr_img_3)
    RelativeLayout rr_img_3;
    @BindView(R.id.rr_img_4)
    RelativeLayout rr_img_4;
    @BindView(R.id.rr_img_5)
    RelativeLayout rr_img_5;
    @BindView(R.id.rr_img_6)
    RelativeLayout rr_img_6;


    String auto_problem = "", auto_symptom = "", auto_allergy = "";

    //for Duration
    String[] duration_data = {"Days", "Months", "Years"};
    String selected_duration = "";
    String sufferingFrom = "";
    //for user_id
    String user_id;
    @BindView(R.id.enterday)
    EditText enterday;
    private Problems_API problems_api;
    private Symptoms_API symptoms_api;
    private Allergy_API allergy_api;
    private AddPatientProblemAPI addPatientProblemAPI;
    private ProblemList problem;
    private Typeface typeface;
    private ArrayList<String> problems = new ArrayList<>();
    private ArrayList<String> symptoms = new ArrayList<>();
    private ArrayList<String> symptomList = new ArrayList<>();
    private ArrayList<String> allergies = new ArrayList<>();
    private ArrayList<String> allergiesList = new ArrayList<>();
    private String symtoms = "", allergie = "", problem_id = "";
    private String patient_id = "", visit_id = "", old_patientid = "";
    private String referered_by = "SELF", refered_to = "";
    private Activity mActivity;
    private File imageFile1 = null, imageFile2 = null, imageFile3 = null, imageFile4 = null, imageFile5 = null, imageFile6 = null;
    private ArrayList<File> fileArrayList = new ArrayList<>();
    private RequestOptions mImgUserOptions;
    private ImageUploadAPI imageUploadAPI;
    private String currentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_patient_problem_activity);
        hideSoftKeyboard();
        ButterKnife.bind(this);
        mActivity = PatientProblemActivity.this;
        imageUploadAPI = Utils.imageUploadAPI();
        getIntentData();

        multiAutoCompleteTextView.setTypeface(ConstantClass.getFont(getApplicationContext()));
        multitext.setTypeface(ConstantClass.getFont(getApplicationContext()));
        autoCompleteTextView.setTypeface(ConstantClass.getFont(getApplicationContext()));
        enterday.setTypeface(ConstantClass.getFont(getApplicationContext()));

        mImgUserOptions = new RequestOptions()
                .centerCrop()
                /*.placeholder(R.drawable.img_place_holder)*/
                .error(R.drawable.img_place_holder)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        addPatientProblemAPI = Utils.addPatientProblemAPI();
        user_id = PreferenceManager.getInstance(getApplicationContext()).getUserID();
        refered_to = PreferenceManager.getInstance(getApplicationContext()).getConsultantDoctor();


        problems_api = Utils.getProblesAPI();
        symptoms_api = Utils.getSymptomAPI();
        allergy_api = Utils.getAllergyAPI();

        submit.setTypeface(ConstantClass.getFont(getApplicationContext()));

//
//        problemAPICall();
//        getAllergies();

        durationsSpinner();


        problemAutoTextViewCall();
        allergiesAutoCompleteTextView();
        addPatientProblemButton();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PatientProblemActivity.this, ClinicHomeActivity.class));
                finishAffinity();
            }
        });
        problemSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                problemss = problemSpinner.getSelectedItem().toString();
                if (position != 0) {
                    problem_id = "";
                    problem_id = problem.getProblemData().getProblems().get(position - 1).getProblemId();

                    getSymptoms(problem.getProblemData().getProblems().get(position - 1).getProblemId());

                    mTvSymtoms.setText(symtoms);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent.hasExtra("patient_id")) {
            old_patientid = intent.getStringExtra("patient_id");
        }
        if (intent.hasExtra("oldpatient_id"))
            old_patientid = intent.getStringExtra("oldpatient_id");

    }

    private void getSymptoms(String problemId) {
        symptoms.clear();
        symptoms.add("Please Select");
        Call<SymptomsList> symptomsListCall = symptoms_api.symptom(problemId);
        symptomsListCall.enqueue(new Callback<SymptomsList>() {
            @Override
            public void onResponse(Call<SymptomsList> call, Response<SymptomsList> response) {
                if (response.isSuccessful()) {
                    SymptomsList symptomsList = response.body();
                    if (symptomsList.getSymptomData() != null && symptomsList.getSymptomData().getSymptom() != null && symptomsList.getSymptomData().getSymptom().size() > 0) {
                        for (int i = 0; i < symptomsList.getSymptomData().getSymptom().size(); i++) {
                            symptoms.add(symptomsList.getSymptomData().getSymptom().get(i).getSymptomName());
                        }
                        List<StateVO> systomsVOs = new ArrayList<>();
                        for (String aHowToWork : symptoms) {
                            StateVO stateVO = new StateVO();
                            stateVO.setTitle(aHowToWork);
                            stateVO.setSelected(false);
                            systomsVOs.add(stateVO);
                        }
                        MyAdapter sysmtomsAdapter = new MyAdapter(getBaseContext(), 0, 1, systomsVOs, PatientProblemActivity.this);
                        symptopmSpinner.setAdapter(sysmtomsAdapter);
                        symptopmSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                StateVO stateVO = (StateVO) symptopmSpinner.getSelectedItem();
                                symtoms = stateVO.getTitle();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<SymptomsList> call, Throwable t) {
            }
        });
    }

    private void problemAPICall() {
        problems.add("Please Select");
        Call<ProblemList> problemListCall = problems_api.getProblemlist();
        problemListCall.enqueue(new Callback<ProblemList>() {
            @Override
            public void onResponse(Call<ProblemList> call, final Response<ProblemList> response) {
                if (response.isSuccessful()) {
                    problem = response.body();

                    if (problem != null && problem.getProblemData() != null && problem.getProblemData().getProblems() != null && problem.getProblemData().getProblems().size() > 0) {
                        for (int i = 0; i < problem.getProblemData().getProblems().size(); i++) {
                            problems.add(problem.getProblemData().getProblems().get(i).getProblemName());
                        }
                        ArrayAdapter<String> problemAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, problems) {
                            public View getView(int position, View convertView, ViewGroup parent) {
                                typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                                TextView v = (TextView) super.getView(position, convertView, parent);
                                v.setTypeface(typeface);
                                return v;
                            }

                            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                TextView v = (TextView) super.getView(position, convertView, parent);
                                v.setTypeface(typeface);
                                return v;
                            }
                        };
                        problemAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                        problemSpinner.setAdapter(problemAdapter);

                    }
                }
            }

            @Override
            public void onFailure(Call<ProblemList> call, Throwable t) {
                Toast.makeText(PatientProblemActivity.this, "Error:  " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getAllergies() {
        allergies.add("Please Select");
        Call<AllergiesData> allergiesDataCall = allergy_api.allergydata();
        allergiesDataCall.enqueue(new Callback<AllergiesData>() {
            @Override
            public void onResponse(Call<AllergiesData> call, Response<AllergiesData> response) {
                if (response.isSuccessful()) {
                    AllergiesData allergiesData = response.body();
                    if (allergiesData.getData1() != null && allergiesData.getData1().getAllergies() != null && allergiesData.getData1().getAllergies().size() > 0) {
                        for (int i = 0; i < allergiesData.getData1().getAllergies().size(); i++) {
                            allergies.add(allergiesData.getData1().getAllergies().get(i).getAllergyName());
                        }
                        List<StateVO> systomsVOs = new ArrayList<>();
                        for (String aHowToWork : allergies) {
                            StateVO stateVO = new StateVO();
                            stateVO.setTitle(aHowToWork);
                            stateVO.setSelected(false);
                            systomsVOs.add(stateVO);
                        }
                        MyAdapter sysmtomsAdapter = new MyAdapter(getBaseContext(), 0, 2, systomsVOs, PatientProblemActivity.this);
                        allergiesSpinner.setAdapter(sysmtomsAdapter);
                        allergiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                StateVO stateVO = (StateVO) allergiesSpinner.getSelectedItem();
                                allergie = stateVO.getTitle();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }
                }
            }


            @Override
            public void onFailure(Call<AllergiesData> call, Throwable t) {
                Toast.makeText(PatientProblemActivity.this, "Something went wrong: " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public void itemClickListener(int position, View view) {

    }

    @Override
    public void itemMedicineClickListener(int medicinePosition, int intakePosition, int daysPosition) {

    }

    @Override
    public void itemClickOfDropDown(int type, String title, boolean isChecked) {
        switch (type) {
            case 1:
                if (isChecked) {
                    if (!symptomList.contains(title))
                        symptomList.add(title);
                } else {
                    if (symptomList.contains(title))
                        symptomList.remove(title);
                }
                symtoms = "";
                for (int i = 0; i < symptomList.size(); i++) {
                    symtoms = symtoms + symptomList.get(i);
                    if (i != symptomList.size() - 1)
                        symtoms = symtoms + ", ";
                }
                mTvSymtoms.setText(symtoms);
                break;
            case 2:
                if (isChecked) {
                    if (!allergiesList.contains(title))
                        allergiesList.add(title);
                } else {
                    if (allergiesList.contains(title))
                        allergiesList.remove(title);
                }
                allergie = "";
                for (int i = 0; i < allergiesList.size(); i++) {
                    allergie = allergie + allergiesList.get(i);
                    if (i != allergiesList.size() - 1)
                        allergie = allergie + ", ";
                }
                mTvAlleries.setText(allergie);
                break;
        }
    }

    @Override
    public void itemMedicince(String medicine, String intake, int daysPosition) {

    }

    @Override
    public void selectedItems(String selectedItems) {

    }

    private void durationsSpinner() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, duration_data) {

            public View getView(int position, View convertView, ViewGroup parent) {
                typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(typeface);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(typeface);
                return v;
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        durationSpinner.setAdapter(adapter);

        durationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_duration = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void addPatientProblemButton() {

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (oka.equals("") || auto_symptom.equals("") || auto_allergy.equals("")) {
                    DialogUtils.showAlertBox(PatientProblemActivity.this, false, "", "Please input all fields.", R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                        @Override
                        public void dialogPositiveButton() {

                        }

                        @Override
                        public void dialogNegativeButton() {

                        }
                    });
                } else {
                    DialogUtils.showProgressDialog(mActivity, mActivity.getResources().getString(R.string.please_wait), false);

                    if (!enterday.getText().toString().equals(""))
                        sufferingFrom = enterday.getText() + " "+ selected_duration;

//                    String[] allergy = TextUtils.split(auto_allergy.substring(0,auto_allergy.length()-2), ",");
//                    String[] symptom = TextUtils.split(auto_symptom.substring(0,auto_symptom.length()-2), ",");
//                    JSONArray   jsonArray=new JSONArray();
//                    for (int i=0;i<allergy.length;i++){
//                        jsonArray.put(allergy[i]);
//                    }
//                    JSONArray   symptomsJsonArray=new JSONArray();
//                    for (int i=0;i<symptom.length;i++){
//                        symptomsJsonArray.put(symptom[i]);
//                    }
                    Call<AddPatientProblem> addPatientDataCall = addPatientProblemAPI.AddPatientProblem(PreferenceManager.getInstance(PatientProblemActivity.this).getUserID(),
                            old_patientid, oka, auto_symptom.substring(0,auto_symptom.length()-2), sufferingFrom, auto_allergy.substring(0,auto_allergy.length()-2), "1", "", "", "",
                            referered_by, refered_to);
                    addPatientDataCall.enqueue(new Callback<AddPatientProblem>() {
                        @Override
                        public void onResponse(Call<AddPatientProblem> call, Response<AddPatientProblem> response) {
                            if (response.isSuccessful()) {
                                Log.d("illume", response.body().toString());
                                AddPatientProblem list = response.body();

                                fileArrayList.clear();
                                if (imageFile1 != null) {
                                    fileArrayList.add(imageFile1);
                                }
                                if (imageFile2 != null) {
                                    fileArrayList.add(imageFile2);
                                }
                                if (imageFile3 != null) {
                                    fileArrayList.add(imageFile3);
                                }
                                if (imageFile4 != null) {
                                    fileArrayList.add(imageFile4);
                                }
                                if (imageFile5 != null) {
                                    fileArrayList.add(imageFile5);
                                }
                                if (imageFile6 != null) {
                                    fileArrayList.add(imageFile6);
                                }

                                uploadImage(list.getData().getVisit_id());
                            }
                        }

                        @Override
                        public void onFailure(Call<AddPatientProblem> call, Throwable t) {
                            Toast.makeText(PatientProblemActivity.this, "Error", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });


    }


    /**
     * checking if storage permission is granted.
     *
     * @return
     */
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(PatientProblemActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {


                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppHelper.STORAGE_REQUEST);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    /**
     * get camera permission if not granted
     */
    @SuppressLint("NewApi")
    private boolean getCameraPermission() {
        if (ContextCompat.checkSelfPermission(PatientProblemActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    AppHelper.MY_PERMISSIONS_REQUEST_CAMERA);
            return false;
        }
    }

    /**
     * Open options on click of camera icon
     *
     * @return
     */
    private boolean openOptionsForCamera() {
        Animation bottomUp = AnimationUtils.loadAnimation(PatientProblemActivity.this, R.anim.bottom_to_up);
        rlCameraOptions.setAnimation(bottomUp);
        rlCameraOptions.setVisibility(View.VISIBLE);
        return true;
    }

    /**
     * Close options on click of camera icon
     *
     * @return
     */
    public boolean closeOptionsForCamera() {
        Animation anim = AnimationUtils.loadAnimation(PatientProblemActivity.this, R.anim.up_to_bottom);
        rlCameraOptions.setAnimation(anim);
        rlCameraOptions.setVisibility(View.GONE);
        return false;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        AndroidAppUtils.showLog(TAG, "Request code in onActivityResult is:: " + requestCode);

        if (requestCode == AppHelper.CAMERA_REQUEST && resultCode == RESULT_OK) {


            if (currentPhotoPath != null) {

                try {
//                    File compressedImage = new Compressor(mActivity)
//                            .setMaxWidth(640)
//                            .setMaxHeight(480)
//                            .setQuality(75)
//                            .setCompressFormat(Bitmap.CompressFormat.WEBP)
//                            .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
//                                    Environment.DIRECTORY_PICTURES).getAbsolutePath())
//                            .compressToFile(new File(currentPhotoPath));
//
                    File imageFile = new File(currentPhotoPath);

                    if (imageFile == null) {

                    } else {
                        int pos = -1;
                        if (imageFile1 == null) {
                            pos = 0;
                            imageFile1 = imageFile;
                        } else if (imageFile2 == null) {
                            pos = 1;
                            imageFile2 = imageFile;
                        } else if (imageFile3 == null) {
                            pos = 2;
                            imageFile3 = imageFile;
                        } else if (imageFile4 == null) {
                            pos = 3;
                            imageFile4 = imageFile;
                        } else if (imageFile5 == null) {
                            pos = 4;
                            imageFile5 = imageFile;
                        } else if (imageFile6 == null) {
                            pos = 5;
                            imageFile6 = imageFile;
                        }
                        if (pos < 3) {
                            ll4.setVisibility(View.VISIBLE);
                            noPhoto.setVisibility(View.GONE);
                        } else {
                            ll5.setVisibility(View.VISIBLE);
                        }
                        switch (pos) {
                            case 0:
                                Glide.with(mActivity).clear(img1);
                                Glide.with(mActivity)
                                        .load(imageFile)
                                        .apply(mImgUserOptions)
                                        .into(img1);
                                rr_img_1.setVisibility(View.VISIBLE);

                                break;
                            case 1:
                                Glide.with(mActivity).clear(img2);
                                Glide.with(mActivity)
                                        .load(imageFile)
                                        .apply(mImgUserOptions)
                                        .into(img2);
                                rr_img_2.setVisibility(View.VISIBLE);
                                break;
                            case 2:
                                Glide.with(mActivity).clear(img3);
                                Glide.with(mActivity)
                                        .load(imageFile)
                                        .apply(mImgUserOptions)
                                        .into(img3);
                                rr_img_3.setVisibility(View.VISIBLE);
                                break;
                            case 3:
                                Glide.with(mActivity).clear(img4);
                                Glide.with(mActivity)
                                        .load(imageFile)
                                        .apply(mImgUserOptions)
                                        .into(img4);
                                rr_img_4.setVisibility(View.VISIBLE);
                                break;
                            case 4:
                                Glide.with(mActivity).clear(img5);
                                Glide.with(mActivity)
                                        .load(imageFile)
                                        .apply(mImgUserOptions)
                                        .into(img5);
                                rr_img_5.setVisibility(View.VISIBLE);
                                break;
                            case 5:
                                Glide.with(mActivity).clear(img6);
                                Glide.with(mActivity)
                                        .load(imageFile)
                                        .apply(mImgUserOptions)
                                        .into(img6);
                                rr_img_6.setVisibility(View.VISIBLE);
                                break;
                        }

                        currentPhotoPath = imageFile.getAbsolutePath();
                    }
                } catch (Exception e) {
                    e.printStackTrace();


                }
            } else {
                AndroidAppUtils.showLog(TAG, "Current photo is empty:: ");

            }


//            Bitmap photo = (Bitmap) data.getExtras().get(AppHelper.DATA);
//
//            AndroidAppUtils.showLog(TAG, "Camera request image:: " + photo.toString());
//
////            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
//            Uri tempUri = getImageUri(mActivity, photo);
////
////            // CALL THIS METHOD TO GET THE ACTUAL PATH
//            if (tempUri.equals("")) {
//                AndroidAppUtils.showToast(mActivity, "Please select valid image");
//            } else {
//                File imageFile = new File(getRealPathFromURI(tempUri));
//                if (imageFile == null) {
//                } else {
//
//                    int pos = -1;
//                    if (imageFile1 == null) {
//                        pos = 0;
//                        imageFile1 = imageFile;
//                    } else if (imageFile2 == null) {
//                        pos = 1;
//                        imageFile2 = imageFile;
//                    } else if (imageFile3 == null) {
//                        pos = 2;
//                        imageFile3 = imageFile;
//                    } else if (imageFile4 == null) {
//                        pos = 3;
//                        imageFile4 = imageFile;
//                    } else if (imageFile5 == null) {
//                        pos = 4;
//                        imageFile5 = imageFile;
//                    } else if (imageFile6 == null) {
//                        pos = 5;
//                        imageFile6 = imageFile;
//                    }
//                    if (pos < 3) {
//                        ll4.setVisibility(View.VISIBLE);
//                        noPhoto.setVisibility(View.GONE);
//                    } else {
//                        ll5.setVisibility(View.VISIBLE);
//                    }
//                    switch (pos) {
//                        case 0:
//                            Glide.with(mActivity).clear(img1);
//                            Glide.with(mActivity)
//                                    .load(imageFile)
//                                    .apply(mImgUserOptions)
//                                    .into(img1);
//                            rr_img_1.setVisibility(View.VISIBLE);
//
//                            break;
//                        case 1:
//                            Glide.with(mActivity).clear(img2);
//                            Glide.with(mActivity)
//                                    .load(imageFile)
//                                    .apply(mImgUserOptions)
//                                    .into(img2);
//                            rr_img_2.setVisibility(View.VISIBLE);
//                            break;
//                        case 2:
//                            Glide.with(mActivity).clear(img3);
//                            Glide.with(mActivity)
//                                    .load(imageFile)
//                                    .apply(mImgUserOptions)
//                                    .into(img3);
//                            rr_img_3.setVisibility(View.VISIBLE);
//                            break;
//                        case 3:
//                            Glide.with(mActivity).clear(img4);
//                            Glide.with(mActivity)
//                                    .load(imageFile)
//                                    .apply(mImgUserOptions)
//                                    .into(img4);
//                            rr_img_4.setVisibility(View.VISIBLE);
//                            break;
//                        case 4:
//                            Glide.with(mActivity).clear(img5);
//                            Glide.with(mActivity)
//                                    .load(imageFile)
//                                    .apply(mImgUserOptions)
//                                    .into(img5);
//                            rr_img_5.setVisibility(View.VISIBLE);
//                            break;
//                        case 5:
//                            Glide.with(mActivity).clear(img6);
//                            Glide.with(mActivity)
//                                    .load(imageFile)
//                                    .apply(mImgUserOptions)
//                                    .into(img6);
//                            rr_img_6.setVisibility(View.VISIBLE);
//                            break;
//                    }
//                }
//                AndroidAppUtils.showLog(TAG, "Path of image is:: " + imageFile.getPath().toString());
//            }

        } else if (requestCode == AppHelper.SELECT_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            Bitmap bitmap = null;
            AndroidAppUtils.showLog(TAG, "Selected image URI:: " + uri.toString());

            if (uri.equals("")) {
                AndroidAppUtils.showToast(mActivity, "Please select valid image");
            } else {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), uri);
                    AndroidAppUtils.showLog(TAG, "Selected image bitmap:: " + bitmap.toString());

                    Uri tempUri = getImageUri(mActivity, bitmap);
                    AndroidAppUtils.showLog(TAG, "Selected image tempUri:: " + bitmap.toString());

//             CALL THIS METHOD TO GET THE ACTUAL PATH
                    if (tempUri.equals("")) {
                        AndroidAppUtils.showToast(mActivity, "Please select valid image");
                    } else {
                        File imageFile = new File(getRealPathFromURI(tempUri));

                        if (imageFile == null) {
                        } else {

                            int pos = -1;
                            if (imageFile1 == null) {
                                pos = 0;
                                imageFile1 = imageFile;
                            } else if (imageFile2 == null) {
                                pos = 1;
                                imageFile2 = imageFile;
                            } else if (imageFile3 == null) {
                                pos = 2;
                                imageFile3 = imageFile;
                            } else if (imageFile4 == null) {
                                pos = 3;
                                imageFile4 = imageFile;
                            } else if (imageFile5 == null) {
                                pos = 4;
                                imageFile5 = imageFile;
                            } else if (imageFile6 == null) {
                                pos = 5;
                                imageFile6 = imageFile;
                            }
                            if (pos < 3) {
                                ll4.setVisibility(View.VISIBLE);
                                noPhoto.setVisibility(View.GONE);
                            } else {
                                ll5.setVisibility(View.VISIBLE);
                            }
                            switch (pos) {
                                case 0:
                                    Glide.with(mActivity).clear(img1);
                                    Glide.with(mActivity)
                                            .load(imageFile)
                                            .apply(mImgUserOptions)
                                            .into(img1);
                                    rr_img_1.setVisibility(View.VISIBLE);

                                    break;
                                case 1:
                                    Glide.with(mActivity).clear(img2);
                                    Glide.with(mActivity)
                                            .load(imageFile)
                                            .apply(mImgUserOptions)
                                            .into(img2);
                                    rr_img_2.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    Glide.with(mActivity).clear(img3);
                                    Glide.with(mActivity)
                                            .load(imageFile)
                                            .apply(mImgUserOptions)
                                            .into(img3);
                                    rr_img_3.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    Glide.with(mActivity).clear(img4);
                                    Glide.with(mActivity)
                                            .load(imageFile)
                                            .apply(mImgUserOptions)
                                            .into(img4);
                                    rr_img_4.setVisibility(View.VISIBLE);
                                    break;
                                case 4:
                                    Glide.with(mActivity).clear(img5);
                                    Glide.with(mActivity)
                                            .load(imageFile)
                                            .apply(mImgUserOptions)
                                            .into(img5);
                                    rr_img_5.setVisibility(View.VISIBLE);
                                    break;
                                case 5:
                                    Glide.with(mActivity).clear(img6);
                                    Glide.with(mActivity)
                                            .load(imageFile)
                                            .apply(mImgUserOptions)
                                            .into(img6);
                                    rr_img_6.setVisibility(View.VISIBLE);
                                    break;
                            }
                        }

                        AndroidAppUtils.showLog(TAG, "Image path in gallery is:: " + imageFile.getAbsolutePath());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * get image URI from bitmap image
     *
     * @param inContext
     * @param inImage
     * @return
     */
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        if (inImage != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
            return Uri.parse(path);
        } else {
            AndroidAppUtils.showToast(PatientProblemActivity.this, "Please select valid image");
            return Uri.parse("");
        }
    }

    /**
     * get actual image path from URI
     *
     * @param uri
     * @return
     */
    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (mActivity.getContentResolver() != null) {
            Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        } else {
            AndroidAppUtils.showErrorLog(TAG, "Content Resolver is null:: ");
        }
        return path;
    }


    @OnClick({R.id.tv_camera, R.id.tv_photo_library, R.id.tv_camera_cancel_btn, R.id.addproblemimage, R.id.cross_1, R.id.cross_2, R.id.cross_3,
            R.id.cross_4, R.id.cross_5, R.id.cross_6, R.id.img_1, R.id.img_2, R.id.img_3, R.id.img_4, R.id.img_5, R.id.img_6})
    public void onViewClicked(View view) {
        Intent intent = new Intent(PatientProblemActivity.this, DisplayImageActivity.class);
        switch (view.getId()) {
            case R.id.tv_camera:
                dispatchTakePictureIntent();
                closeOptionsForCamera();
                break;
            case R.id.tv_photo_library:
                openGallery();
                closeOptionsForCamera();
                break;
            case R.id.tv_camera_cancel_btn:
                closeOptionsForCamera();
                break;
            case R.id.addproblemimage:
                fileArrayList.clear();
                if (imageFile1 != null) {
                    fileArrayList.add(imageFile1);
                }
                if (imageFile2 != null) {
                    fileArrayList.add(imageFile2);
                }
                if (imageFile3 != null) {
                    fileArrayList.add(imageFile3);
                }
                if (imageFile4 != null) {
                    fileArrayList.add(imageFile4);
                }
                if (imageFile5 != null) {
                    fileArrayList.add(imageFile5);
                }
                if (imageFile6 != null) {
                    fileArrayList.add(imageFile6);
                }
                if (fileArrayList.size() < 6) {
                    if (isStoragePermissionGranted()) {
                        if (getCameraPermission()) {
                            openOptionsForCamera();
                        }
                    }
                } else {
                    AndroidAppUtils.showToast(mActivity, "You only select 6 images.");
                }
                break;
            case R.id.cross_1:
                imageFile1 = null;
                rr_img_1.setVisibility(View.INVISIBLE);
                break;
            case R.id.cross_2:
                imageFile2 = null;
                rr_img_2.setVisibility(View.INVISIBLE);
                break;
            case R.id.cross_3:
                imageFile3 = null;
                rr_img_3.setVisibility(View.INVISIBLE);
                break;
            case R.id.cross_4:
                imageFile4 = null;
                rr_img_4.setVisibility(View.INVISIBLE);
                break;
            case R.id.cross_5:
                imageFile5 = null;
                rr_img_5.setVisibility(View.INVISIBLE);
                break;
            case R.id.cross_6:
                imageFile6 = null;
                rr_img_6.setVisibility(View.INVISIBLE);
                break;
            case R.id.img_1:
                intent.putExtra("img", imageFile1);
                startActivity(intent);
                break;
            case R.id.img_2:
                intent.putExtra("img", imageFile2);
                startActivity(intent);
                break;
            case R.id.img_3:
                intent.putExtra("img", imageFile3);
                startActivity(intent);
                break;
            case R.id.img_4:
                intent.putExtra("img", imageFile4);
                startActivity(intent);
                break;
            case R.id.img_5:
                intent.putExtra("img", imageFile5);
                startActivity(intent);
                break;
            case R.id.img_6:
                intent.putExtra("img", imageFile6);
                startActivity(intent);
                break;


        }
    }

    /**
     * open camera
     */
    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        startActivityForResult(intent, AppHelper.CAMERA_REQUEST);
    }


    /**
     * dispatch intent to open camera and store image on selected URI
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            AndroidAppUtils.showLog(TAG, "image path is:: " + photoFile.getAbsolutePath());
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(mActivity,
                        "com.Chirag.illumeclinica.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, AppHelper.CAMERA_REQUEST);
            }
        }
    }


    /**
     * create file to store image
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    /**
     * open gallery from Intent
     */
    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppHelper.SELECT_IMAGE);
    }

    private void uploadImage(final Integer visit_id) {
        if (fileArrayList.size() > 0) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileArrayList.get(0));
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("image", fileArrayList.get(0).getName(), reqFile);

            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), visit_id + "");

            Call<ImageModel> req = imageUploadAPI.postImage(body, id);
            req.enqueue(new Callback<ImageModel>() {
                @Override
                public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                    if (response.isSuccessful()) {
                        AndroidAppUtils.showLog(TAG, "message : " + response.message());
                        fileArrayList.remove(0);
                        uploadImage(visit_id);
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<ImageModel> call, Throwable t) {
                    AndroidAppUtils.showLog(TAG, "postImage  error : " + t.getMessage());

                }
            });

        } else {
            AndroidAppUtils.showLog(TAG, "file list is empty");
            DialogUtils.hideProgressDialog();
            startActivity(new Intent(PatientProblemActivity.this, ClinicHomeActivity.class));
            finishAffinity();
        }
    }

    private void problemAutoTextViewCall() {

        final Call<ProblemList> listCall = problems_api.getProblemlist();
        listCall.enqueue(new Callback<ProblemList>() {
            @Override
            public void onResponse(Call<ProblemList> call, Response<ProblemList> response) {

                if (response.isSuccessful()) {

                    final ProblemList problemList = response.body();
                    for (int i = 0; i < problemList.getProblemData().getProblems().size(); i++) {
                        Problems.add(problemList.getProblemData().getProblems().get(i).getProblemName());
                        ids = problemList.getProblemData().getProblems().get(i).getProblemId();
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, Problems) {
                            public View getView(int position, View convertView, ViewGroup parent) {
                                typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                                TextView v = (TextView) super.getView(position, convertView, parent);
                                v.setTypeface(typeface);
                                return v;
                            }

                            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                TextView v = (TextView) super.getView(position, convertView, parent);
                                v.setTypeface(typeface);
                                return v;
                            }
                        };
                        autoCompleteTextView.setAdapter(adapter);

                        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                auto_problem = autoCompleteTextView.getText().toString();
                                oka = problemList.getProblemData().getProblems().get(position).getProblemId();
                                symptomsAutoTextViewCall();


                            }
                        });

                        iv1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                autoCompleteTextView.showDropDown();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<ProblemList> call, Throwable t) {

            }
        });
    }


    private void symptomsAutoTextViewCall() {
        DialogUtils.showProgressDialog(this, "Please wait...", false);

        Call<SymptomsList> symptomsListCall = symptoms_api.symptom(oka);
        symptomsListCall.enqueue(new Callback<SymptomsList>() {
            @Override
            public void onResponse(Call<SymptomsList> call, Response<SymptomsList> response) {
                DialogUtils.hideProgressDialog();
                if (response.isSuccessful()) {
                    SymptomsList symptomsList = response.body();

                    if (symptomsList.getSymptomData() != null && symptomsList.getSymptomData().getSymptom() != null && symptomsList.getSymptomData().getSymptom().size() > 0) {

                        for (int i = 0; i < symptomsList.getSymptomData().getSymptom().size(); i++) {
                            Symptoms.add(symptomsList.getSymptomData().getSymptom().get(i).getSymptomName());

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, Symptoms) {
                                public View getView(int position, View convertView, ViewGroup parent) {
                                    typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                                    TextView v = (TextView) super.getView(position, convertView, parent);
                                    v.setTypeface(typeface);
                                    return v;
                                }

                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                    TextView v = (TextView) super.getView(position, convertView, parent);
                                    v.setTypeface(typeface);
                                    return v;
                                }
                            };
                            multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                            multiAutoCompleteTextView.setAdapter(adapter);
                            multiAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    auto_symptom = multiAutoCompleteTextView.getText().toString();
                                    auto_symptom.substring(0, auto_symptom.length() - 2);

                                }
                            });
                            iv2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    multiAutoCompleteTextView.showDropDown();
                                }
                            });
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<SymptomsList> call, Throwable t) {
                Toast.makeText(mActivity, "t: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void allergiesAutoCompleteTextView() {
        Call<AllergiesData> allergiesDataCall = allergy_api.allergydata();
        allergiesDataCall.enqueue(new Callback<AllergiesData>() {
            @Override
            public void onResponse(Call<AllergiesData> call, Response<AllergiesData> response) {
                if (response.isSuccessful()) {
                    AllergiesData allergiesData = response.body();
                    if (allergiesData.getData1() != null && allergiesData.getData1().getAllergies() != null && allergiesData.getData1().getAllergies().size() > 0) {
                        for (int i = 0; i < allergiesData.getData1().getAllergies().size(); i++) {
                            Allergy.add(allergiesData.getData1().getAllergies().get(i).getAllergyName());
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, Allergy) {
                                public View getView(int position, View convertView, ViewGroup parent) {
                                    typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                                    TextView v = (TextView) super.getView(position, convertView, parent);
                                    v.setTypeface(typeface);
                                    return v;
                                }

                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                    TextView v = (TextView) super.getView(position, convertView, parent);
                                    v.setTypeface(typeface);
                                    return v;
                                }
                            };
                            multitext.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                            multitext.setAdapter(adapter);

                            multitext.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    auto_allergy = multitext.getText().toString();
                                }
                            });
                            iv3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    multitext.showDropDown();
                                }
                            });
                        }

                    }
                }
            }


            @Override
            public void onFailure(Call<AllergiesData> call, Throwable t) {
                Toast.makeText(PatientProblemActivity.this, "Something went wrong: " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}

