package com.Chirag.illumeclinica.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Chirag.illumeclinica.Networking.GetMedicineAPI;
import com.Chirag.illumeclinica.Networking.InvoiceAPI;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.fragments.DashBoardFragment;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.models.InvoiceModel;
import com.Chirag.illumeclinica.models.MedicineModel;
import com.Chirag.illumeclinica.utils.AndroidAppUtils;
import com.Chirag.illumeclinica.utils.DialogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceActivity extends Activity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tool)
    AppBarLayout tool;
    @BindView(R.id.rr1)
    LinearLayout rr1;
    @BindView(R.id.print)
    Button print;
    @BindView(R.id.rr2)
    RelativeLayout rr2;
    @BindView(R.id.webView)
    WebView webview;
    private InvoiceAPI invoiceAPI;
    private String visit_id = "";
    private String url = "";
    private ProgressDialog pDialog;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invoice_activity);
        ButterKnife.bind(this);

        invoiceAPI = Utils.invoice_API();
        print.setVisibility(View.GONE);

        if (getIntent().hasExtra("visit_id")) {
            visit_id = getIntent().getStringExtra("visit_id");
        }
        DialogUtils.showProgressDialog(this, "Please wait ...", false);
        Call<InvoiceModel> invoice = invoiceAPI.invoiceAPI(visit_id);
        invoice.enqueue(new retrofit2.Callback<InvoiceModel>() {
            @Override
            public void onResponse(Call<InvoiceModel> call, Response<InvoiceModel> response) {
                DialogUtils.hideProgressDialog();
                if (response.isSuccessful()) {
                    url = response.body().getData().getImage();
                    init();
                    listener();
                } else {
                    AndroidAppUtils.showLog(InvoiceActivity.class.getSimpleName(), "error response :");
                }
            }

            @Override
            public void onFailure(Call<InvoiceModel> call, Throwable t) {

            }
        });

    }

    private void init() {
        webview.getSettings().setJavaScriptEnabled(true);

        pDialog = new ProgressDialog(InvoiceActivity.this);
        pDialog.setTitle("Invoice");
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);

    }

    private void listener() {
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (pDialog!=null)
                    pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (pDialog!=null)
                    pDialog.dismiss();
            }
        });
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }

    @OnClick({R.id.back, R.id.print})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.print:
                DialogUtils.showAlertBox(InvoiceActivity.this, false, "", "Comming Soon.", R.string.ok, R.string.empty_text, new ClickListenerForDialogBox() {
                    @Override
                    public void dialogPositiveButton() {
                        finish();
                    }

                    @Override
                    public void dialogNegativeButton() {

                    }
                });
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        pDialog.dismiss();
        pDialog=null;
        finish();
    }
}
