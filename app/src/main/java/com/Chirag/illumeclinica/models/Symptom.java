
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Symptom {

    @SerializedName("symptom_id")
    @Expose
    private String symptomId;
    @SerializedName("symptom_name")
    @Expose
    private String symptomName;

    public String getSymptomId() {
        return symptomId;
    }

    public void setSymptomId(String symptomId) {
        this.symptomId = symptomId;
    }

    public String getSymptomName() {
        return symptomName;
    }

    public void setSymptomName(String symptomName) {
        this.symptomName = symptomName;
    }

    @Override
    public String toString() {
        return "Symptom{" +
                "symptomId='" + symptomId + '\'' +
                ", symptomName='" + symptomName + '\'' +
                '}';
    }
}
