package com.Chirag.illumeclinica.models;

public class MedicinesIds {

    /**
     * medicine_id : 1
     * intake_id : 1
     * duration : 1
     */

    private String medicine_id;
    private String intake_id;
    private String duration;

    public String getMedicine_id() {
        return medicine_id;
    }

    public void setMedicine_id(String medicine_id) {
        this.medicine_id = medicine_id;
    }

    public String getIntake_id() {
        return intake_id;
    }

    public void setIntake_id(String intake_id) {
        this.intake_id = intake_id;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
