package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientProblemData {

    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("problem_id")
    @Expose
    private String problemId;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("allergy_ids")
    @Expose
    private List<String> allergyIds = null;
    @SerializedName("sypmtom_ids")
    @Expose
    private List<String> sypmtomIds = null;
    @SerializedName("visit_status")
    @Expose
    private Integer visitStatus;
    @SerializedName("visit_id")
    @Expose
    private Integer visit_id;
    @SerializedName("runningTreatmentStatus")
    @Expose
    private String runningTreatmentStatus;
    @SerializedName("runningTreatmentDuration")
    @Expose
    private String runningTreatmentDuration;
    @SerializedName("runningTreatmentDoctor")
    @Expose
    private String runningTreatmentDoctor;
    @SerializedName("runningTreatmentMedicine")
    @Expose
    private String runningTreatmentMedicine;
    @SerializedName("fk_userid")
    @Expose
    private String fkUserid;

    public Integer getVisit_id() {
        return visit_id;
    }

    public void setVisit_id(Integer visit_id) {
        this.visit_id = visit_id;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getProblemId() {
        return problemId;
    }

    public void setProblemId(String problemId) {
        this.problemId = problemId;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<String> getAllergyIds() {
        return allergyIds;
    }

    public void setAllergyIds(List<String> allergyIds) {
        this.allergyIds = allergyIds;
    }

    public List<String> getSypmtomIds() {
        return sypmtomIds;
    }

    public void setSypmtomIds(List<String> sypmtomIds) {
        this.sypmtomIds = sypmtomIds;
    }

    public Integer getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(Integer visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getRunningTreatmentStatus() {
        return runningTreatmentStatus;
    }

    public void setRunningTreatmentStatus(String runningTreatmentStatus) {
        this.runningTreatmentStatus = runningTreatmentStatus;
    }

    public String getRunningTreatmentDuration() {
        return runningTreatmentDuration;
    }

    public void setRunningTreatmentDuration(String runningTreatmentDuration) {
        this.runningTreatmentDuration = runningTreatmentDuration;
    }

    public String getRunningTreatmentDoctor() {
        return runningTreatmentDoctor;
    }

    public void setRunningTreatmentDoctor(String runningTreatmentDoctor) {
        this.runningTreatmentDoctor = runningTreatmentDoctor;
    }

    public String getRunningTreatmentMedicine() {
        return runningTreatmentMedicine;
    }

    public void setRunningTreatmentMedicine(String runningTreatmentMedicine) {
        this.runningTreatmentMedicine = runningTreatmentMedicine;
    }

    public String getFkUserid() {
        return fkUserid;
    }

    public void setFkUserid(String fkUserid) {
        this.fkUserid = fkUserid;
    }

    @Override
    public String toString() {
        return "PatientProblemData{" +
                "patientId='" + patientId + '\'' +
                ", problemId='" + problemId + '\'' +
                ", duration='" + duration + '\'' +
                ", allergyIds=" + allergyIds +
                ", sypmtomIds=" + sypmtomIds +
                ", visitStatus=" + visitStatus +
                ", runningTreatmentStatus='" + runningTreatmentStatus + '\'' +
                ", runningTreatmentDuration='" + runningTreatmentDuration + '\'' +
                ", runningTreatmentDoctor='" + runningTreatmentDoctor + '\'' +
                ", runningTreatmentMedicine='" + runningTreatmentMedicine + '\'' +
                ", fkUserid='" + fkUserid + '\'' +
                '}';
    }
}
