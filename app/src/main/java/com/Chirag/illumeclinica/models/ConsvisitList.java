
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConsvisitList implements Serializable {

    @SerializedName("visit_id")
    @Expose
    private String visitId;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("problem_name")
    @Expose
    private String problemName;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("consultant_doctor")
    @Expose
    private String consultantDoctor;
    @SerializedName("symptom")
    @Expose
    private String symptom;
    @SerializedName("visit_status")
    @Expose
    private String visitStatus;
    @SerializedName("invoice_date_time")
    @Expose
    private String invoiceDateTime;
    @SerializedName("clinic_hospital_name")
    @Expose
    private String clinicHospitalName;

    private String allergy;
    private String SufferingFrom;

    public String getAllergy() {
        return allergy;
    }

    public void setAllergy(String allergy) {
        this.allergy = allergy;
    }

    public String getSufferingFrom() {
        return SufferingFrom;
    }

    public void setSufferingFrom(String sufferingFrom) {
        SufferingFrom = sufferingFrom;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getConsultantDoctor() {
        return consultantDoctor;
    }

    public void setConsultantDoctor(String consultantDoctor) {
        this.consultantDoctor = consultantDoctor;
    }
    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(String visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getInvoiceDateTime() {
        return invoiceDateTime;
    }

    public void setInvoiceDateTime(String invoiceDateTime) {
        this.invoiceDateTime = invoiceDateTime;
    }

    public String getClinicHospitalName() {
        return clinicHospitalName;
    }

    public void setClinicHospitalName(String clinicHospitalName) {
        this.clinicHospitalName = clinicHospitalName;
    }

    @Override
    public String toString() {
        return "ConsvisitList{" +
                "visitId='" + visitId + '\'' +
                ", patientId='" + patientId + '\'' +
                ", problemName='" + problemName + '\'' +
                ", patientName='" + patientName + '\'' +
                ", gender='" + gender + '\'' +
                ", age='" + age + '\'' +
                ", consultantDoctor=" + consultantDoctor +
                ", symptom='" + symptom + '\'' +
                ", visitStatus='" + visitStatus + '\'' +
                ", invoiceDateTime='" + invoiceDateTime + '\'' +
                ", clinicHospitalName='" + clinicHospitalName + '\'' +
                '}';
    }
}
