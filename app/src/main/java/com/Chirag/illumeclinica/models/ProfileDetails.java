
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileDetails {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("fk_loggedid")
    @Expose
    private String fkLoggedid;
    @SerializedName("auth_token")
    @Expose
    private String authToken;
    @SerializedName("user_createdate")
    @Expose
    private String userCreatedate;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("clinic_id")
    @Expose
    private String clinicId;
    @SerializedName("clinic_hospital_name")
    @Expose
    private String clinicHospitalName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("contact_person")
    @Expose
    private String contactPerson;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("reg_no")
    @Expose
    private String regNo;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("chk_demo")
    @Expose
    private String chkDemo;
    @SerializedName("fk_doctorid")
    @Expose
    private Object fkDoctorid;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_date")
    @Expose
    private String createdDate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFkLoggedid() {
        return fkLoggedid;
    }

    public void setFkLoggedid(String fkLoggedid) {
        this.fkLoggedid = fkLoggedid;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUserCreatedate() {
        return userCreatedate;
    }

    public void setUserCreatedate(String userCreatedate) {
        this.userCreatedate = userCreatedate;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getClinicHospitalName() {
        return clinicHospitalName;
    }

    public void setClinicHospitalName(String clinicHospitalName) {
        this.clinicHospitalName = clinicHospitalName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getChkDemo() {
        return chkDemo;
    }

    public void setChkDemo(String chkDemo) {
        this.chkDemo = chkDemo;
    }

    public Object getFkDoctorid() {
        return fkDoctorid;
    }

    public void setFkDoctorid(Object fkDoctorid) {
        this.fkDoctorid = fkDoctorid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}
