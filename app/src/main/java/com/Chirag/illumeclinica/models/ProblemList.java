
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProblemList {

    @SerializedName("error")
    @Expose
    private Object error;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("authfailure")
    @Expose
    private Boolean authfailure;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ProblemData data;

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Boolean getAuthfailure() {
        return authfailure;
    }

    public void setAuthfailure(Boolean authfailure) {
        this.authfailure = authfailure;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProblemData getProblemData() {
        return data;
    }

    public void setProblemData(ProblemData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ProblemList{" +
                "error=" + error +
                ", status=" + status +
                ", statusCode='" + statusCode + '\'' +
                ", authfailure=" + authfailure +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
