
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashboardData {

    @SerializedName("visit_list")
    @Expose
    private List<VisitList> visitList = null;

    public List<VisitList> getVisitList() {
        return visitList;
    }

    public void setVisitList(List<VisitList> visitList) {
        this.visitList = visitList;
    }

    @Override
    public String toString() {
        return "DashboardData{" +
                "visitList=" + visitList +
                '}';
    }
}
