
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicineList {

    @SerializedName("medecine_name")
    @Expose
    private String medecineName;

    public String getMedecineName() {
        return medecineName;
    }

    public void setMedecineName(String medecineName) {
        this.medecineName = medecineName;
    }

}
