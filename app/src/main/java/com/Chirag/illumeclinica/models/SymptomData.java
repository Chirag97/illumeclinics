
package com.Chirag.illumeclinica.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SymptomData {

    @SerializedName("Symptom")
    @Expose
    private List<Symptom> symptom = null;

    public List<Symptom> getSymptom() {
        return symptom;
    }

    public void setSymptom(List<Symptom> symptom) {
        this.symptom = symptom;
    }

    @Override
    public String toString() {
        return "SymptomData{" +
                "symptom=" + symptom +
                '}';
    }
}
