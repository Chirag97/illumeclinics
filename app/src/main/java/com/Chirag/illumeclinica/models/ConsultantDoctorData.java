
package com.Chirag.illumeclinica.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConsultantDoctorData {

    @SerializedName("consvisit_list")
    @Expose
    private List<ConsvisitList> consvisitList = null;

    public List<ConsvisitList> getConsvisitList() {
        return consvisitList;
    }

    public void setConsvisitList(List<ConsvisitList> consvisitList) {
        this.consvisitList = consvisitList;
    }

}
