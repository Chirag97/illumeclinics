
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Problem {

    @SerializedName("problem_id")
    @Expose
    private String problemId;
    @SerializedName("problem_name")
    @Expose
    private String problemName;

    public String getProblemId() {
        return problemId;
    }

    public void setProblemId(String problemId) {
        this.problemId = problemId;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    @Override
    public String toString() {
        return "Problem{" +
                "problemId='" + problemId + '\'' +
                ", problemName='" + problemName + '\'' +
                '}';
    }
}
