package com.Chirag.illumeclinica.models;

import java.util.List;

public class AddPatientData {

    /**
     * error : null
     * status : true
     * status_code : 200
     * authfailure : false
     * message : Patient problem added Successfully.
     * data : {"patient_id":"1","visit_id":"1","problem_id":"1","duration":"1","allergy_ids":["Dandruff","SKIN"],"sypmtom_ids":["vomitting","ABC","CDF"],"runningTreatmentStatus":"1","runningTreatmentDuration":"","runningTreatmentDoctor":"","runningTreatmentMedicine":"","fk_userid":"1"}
     */


    private Object error;
    private boolean status;
    private String status_code;
    private boolean authfailure;
    private String message;
    private DataBean data;

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public boolean isAuthfailure() {
        return authfailure;
    }

    public void setAuthfailure(boolean authfailure) {
        this.authfailure = authfailure;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * patient_id : 1
         * visit_id : 1
         * problem_id : 1
         * duration : 1
         * allergy_ids : ["Dandruff","SKIN"]
         * sypmtom_ids : ["vomitting","ABC","CDF"]
         * runningTreatmentStatus : 1
         * runningTreatmentDuration :
         * runningTreatmentDoctor :
         * runningTreatmentMedicine :
         * fk_userid : 1
         */

        private String patient_id;
        private String visit_id;
        private String problem_id;
        private String duration;
        private String runningTreatmentStatus;
        private String runningTreatmentDuration;
        private String runningTreatmentDoctor;
        private String runningTreatmentMedicine;
        private String fk_userid;
        private List<String> allergy_ids;
        private List<String> sypmtom_ids;

        public String getPatient_id() {
            return patient_id;
        }

        public void setPatient_id(String patient_id) {
            this.patient_id = patient_id;
        }

        public String getVisit_id() {
            return visit_id;
        }

        public void setVisit_id(String visit_id) {
            this.visit_id = visit_id;
        }

        public String getProblem_id() {
            return problem_id;
        }

        public void setProblem_id(String problem_id) {
            this.problem_id = problem_id;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getRunningTreatmentStatus() {
            return runningTreatmentStatus;
        }

        public void setRunningTreatmentStatus(String runningTreatmentStatus) {
            this.runningTreatmentStatus = runningTreatmentStatus;
        }

        public String getRunningTreatmentDuration() {
            return runningTreatmentDuration;
        }

        public void setRunningTreatmentDuration(String runningTreatmentDuration) {
            this.runningTreatmentDuration = runningTreatmentDuration;
        }

        public String getRunningTreatmentDoctor() {
            return runningTreatmentDoctor;
        }

        public void setRunningTreatmentDoctor(String runningTreatmentDoctor) {
            this.runningTreatmentDoctor = runningTreatmentDoctor;
        }

        public String getRunningTreatmentMedicine() {
            return runningTreatmentMedicine;
        }

        public void setRunningTreatmentMedicine(String runningTreatmentMedicine) {
            this.runningTreatmentMedicine = runningTreatmentMedicine;
        }

        public String getFk_userid() {
            return fk_userid;
        }

        public void setFk_userid(String fk_userid) {
            this.fk_userid = fk_userid;
        }

        public List<String> getAllergy_ids() {
            return allergy_ids;
        }

        public void setAllergy_ids(List<String> allergy_ids) {
            this.allergy_ids = allergy_ids;
        }

        public List<String> getSypmtom_ids() {
            return sypmtom_ids;
        }

        public void setSypmtom_ids(List<String> sypmtom_ids) {
            this.sypmtom_ids = sypmtom_ids;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "patient_id='" + patient_id + '\'' +
                    ", visit_id='" + visit_id + '\'' +
                    ", problem_id='" + problem_id + '\'' +
                    ", duration='" + duration + '\'' +
                    ", runningTreatmentStatus='" + runningTreatmentStatus + '\'' +
                    ", runningTreatmentDuration='" + runningTreatmentDuration + '\'' +
                    ", runningTreatmentDoctor='" + runningTreatmentDoctor + '\'' +
                    ", runningTreatmentMedicine='" + runningTreatmentMedicine + '\'' +
                    ", fk_userid='" + fk_userid + '\'' +
                    ", allergy_ids=" + allergy_ids +
                    ", sypmtom_ids=" + sypmtom_ids +
                    '}';
        }
    }


    @Override
    public String toString() {
        return "AddPatientData{" +
                "error=" + error +
                ", status=" + status +
                ", status_code='" + status_code + '\'' +
                ", authfailure=" + authfailure +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
