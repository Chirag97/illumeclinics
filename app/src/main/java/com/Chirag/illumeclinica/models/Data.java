package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("logintoken")
    @Expose
    private String logintoken;

    @SerializedName("consultant_doctor_name")
    @Expose
    private String consultantDoctorName;


    @SerializedName("doctor_name")
    @Expose
    private String consultant_DoctorName;

    @SerializedName("doctor_type")
    @Expose
    private String doctor_type;


    @SerializedName("clinic_name")
    @Expose
    private String clinicName;
    @SerializedName("clinic_id")
    @Expose
    private String clinicId;
    @SerializedName("clinic_address")
    @Expose
    private String clinicAddress;
    @SerializedName("contact_person")
    @Expose
    private String contactPerson;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("clinic_registration_no")
    @Expose
    private String clinicRegistrationNo;
    @SerializedName("clinic_phone_no")
    @Expose
    private String clinicPhoneNo;
    @SerializedName("consultant_doctor_phone_no")
    @Expose
    private String consultantDoctorPhoneNo;
    @SerializedName("consultant_doctor_email_id")
    @Expose
    private String consultantDoctorEmailId;
    @SerializedName("consultant_doctor_address")
    @Expose
    private String consultantDoctorAddress;
    @SerializedName("consultant_doctor_registration_no")
    @Expose
    private String consultantDoctorRegistrationNo;
    @SerializedName("consultant_doctor_speciality")
    @Expose
    private String consultantDoctorSpeciality;



    public String getLogintoken() {
        return logintoken;
    }

    public void setLogintoken(String logintoken) {
        this.logintoken = logintoken;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(String clinicAddress) {
        this.clinicAddress = clinicAddress;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getClinicRegistrationNo() {
        return clinicRegistrationNo;
    }

    public void setClinicRegistrationNo(String clinicRegistrationNo) {
        this.clinicRegistrationNo = clinicRegistrationNo;
    }

    public String getClinicPhoneNo() {
        return clinicPhoneNo;
    }

    public void setClinicPhoneNo(String clinicPhoneNo) {
        this.clinicPhoneNo = clinicPhoneNo;
    }

    public String getConsultantDoctorPhoneNo() {
        return consultantDoctorPhoneNo;
    }

    public void setConsultantDoctorPhoneNo(String consultantDoctorPhoneNo) {
        this.consultantDoctorPhoneNo = consultantDoctorPhoneNo;
    }

    public String getConsultantDoctorEmailId() {
        return consultantDoctorEmailId;
    }

    public void setConsultantDoctorEmailId(String consultantDoctorEmailId) {
        this.consultantDoctorEmailId = consultantDoctorEmailId;
    }

    public String getConsultantDoctorAddress() {
        return consultantDoctorAddress;
    }

    public void setConsultantDoctorAddress(String consultantDoctorAddress) {
        this.consultantDoctorAddress = consultantDoctorAddress;
    }

    public String getConsultantDoctorRegistrationNo() {
        return consultantDoctorRegistrationNo;
    }

    public void setConsultantDoctorRegistrationNo(String consultantDoctorRegistrationNo) {
        this.consultantDoctorRegistrationNo = consultantDoctorRegistrationNo;
    }

    public String getConsultantDoctorSpeciality() {
        return consultantDoctorSpeciality;
    }

    public void setConsultantDoctorSpeciality(String consultantDoctorSpeciality) {
        this.consultantDoctorSpeciality = consultantDoctorSpeciality;
    }

    public String getConsultantDoctorName() {
        return consultantDoctorName;
    }

    public void setConsultantDoctorName(String consultantDoctorName) {
        this.consultantDoctorName = consultantDoctorName;
    }

    public String getConsultant_DoctorName() {
        return consultant_DoctorName;
    }

    public void setConsultant_DoctorName(String consultant_DoctorName) {
        this.consultant_DoctorName = consultant_DoctorName;
    }
    public String getDoctor_type() {
        return doctor_type;
    }

    public void setDoctor_type(String doctor_type) {
        this.doctor_type = doctor_type;
    }

    @Override
    public String toString() {
        return "Data{" +
                "userName='" + userName + '\'' +
                ", userId='" + userId + '\'' +
                ", password='" + password + '\'' +
                ", userType='" + userType + '\'' +
                ", message='" + message + '\'' +
                ", logintoken='" + logintoken + '\'' +
                ", consultantDoctorName='" + consultantDoctorName + '\'' +
                ", consultant_DoctorName='" + consultant_DoctorName + '\'' +
                ", clinicName='" + clinicName + '\'' +
                ", clinicId='" + clinicId + '\'' +
                ", clinicAddress='" + clinicAddress + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                ", emailId='" + emailId + '\'' +
                ", clinicRegistrationNo='" + clinicRegistrationNo + '\'' +
                ", clinicPhoneNo='" + clinicPhoneNo + '\'' +
                ", consultantDoctorPhoneNo='" + consultantDoctorPhoneNo + '\'' +
                ", consultantDoctorEmailId='" + consultantDoctorEmailId + '\'' +
                ", consultantDoctorAddress='" + consultantDoctorAddress + '\'' +
                ", consultantDoctorRegistrationNo='" + consultantDoctorRegistrationNo + '\'' +
                ", consultantDoctorSpeciality='" + consultantDoctorSpeciality + '\'' +
                '}';
    }
}
