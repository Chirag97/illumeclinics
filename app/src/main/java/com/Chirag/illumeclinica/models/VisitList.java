package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VisitList implements Serializable {

    @SerializedName("visit_id")
    @Expose
    private String visitId;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("problem_name")
    @Expose
    private String problemName;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("consultant_doctor")
    @Expose
    private String consultantDoctor;
    @SerializedName("symptom")
    @Expose
    private String symptom;
    @SerializedName("visit_status")
    @Expose
    private String invoiceStatus;
    @SerializedName("invoice_date_time")
    @Expose
    private String invoiceDateTime;

    public String getClinic_hospital_name() {
        return clinic_hospital_name;
    }

    public void setClinic_hospital_name(String clinic_hospital_name) {
        this.clinic_hospital_name = clinic_hospital_name;
    }

    @SerializedName("clinic_hospital_name")
    @Expose
    private String clinic_hospital_name;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getConsultantDoctor() {
        return consultantDoctor;
    }

    public void setConsultantDoctor(String consultantDoctor) {
        this.consultantDoctor = consultantDoctor;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getInvoiceDateTime() {
        return invoiceDateTime;
    }

    public void setInvoiceDateTime(String invoiceDateTime) {
        this.invoiceDateTime = invoiceDateTime;
    }

    @Override
    public String toString() {
        return "VisitList{" +
                "patientId='" + patientId + '\'' +
                ", problemName=" + problemName +
                ", patientName='" + patientName + '\'' +
                ", consultantDoctor='" + consultantDoctor + '\'' +
                ", symptom=" + symptom +
                ", invoiceStatus='" + invoiceStatus + '\'' +
                ", invoiceDateTime='" + invoiceDateTime + '\'' +
                '}';
    }
}
