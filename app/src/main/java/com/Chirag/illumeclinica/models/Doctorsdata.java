
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Doctorsdata {

    @SerializedName("profile_details")
    @Expose
    private DoctorProfileDetails profileDetails;

    public DoctorProfileDetails getProfileDetails() {
        return profileDetails;
    }

    public void setProfileDetails(DoctorProfileDetails profileDetails) {
        this.profileDetails = profileDetails;
    }

}
