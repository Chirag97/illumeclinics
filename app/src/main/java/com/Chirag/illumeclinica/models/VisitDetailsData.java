
package com.Chirag.illumeclinica.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitDetailsData {

    @SerializedName("visit_list")
    @Expose
    private List<VisitDetailsList> visitList = null;
    @SerializedName("image_list")
    @Expose
    private List<ImageList> imageList = null;
    @SerializedName("test_list")
    @Expose
    private List<TestList> testList = null;
    @SerializedName("medicine_list")
    @Expose
    private List<MedicineList> medicineList = null;

    public List<VisitDetailsList> getVisitList() {
        return visitList;
    }

    public void setVisitList(List<VisitDetailsList> visitList) {
        this.visitList = visitList;
    }

    public List<ImageList> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImageList> imageList) {
        this.imageList = imageList;
    }

    public List<TestList> getTestList() {
        return testList;
    }

    public void setTestList(List<TestList> testList) {
        this.testList = testList;
    }

    public List<MedicineList> getMedicineList() {
        return medicineList;
    }

    public void setMedicineList(List<MedicineList> medicineList) {
        this.medicineList = medicineList;
    }

    @Override
    public String toString() {
        return "VisitDetailsData{" +
                "visitList=" + visitList +
                ", imageList=" + imageList +
                ", testList=" + testList +
                ", medicineList=" + medicineList +
                '}';
    }
}
