package com.Chirag.illumeclinica.models;

import java.util.List;

public class MedicineModel {
    /**
     * error : null
     * status : true
     * status_code : 200
     * authfailure : false
     * message : Data retreived successfully.
     * data : {"test_list":[{"id":"5","test_name":"Blood Test"},{"id":"1","test_name":"LFT"}],"intake_list":[{"id":"10","intake_name":"1+0+0"},{"id":"9","intake_name":"0+0+1"},{"id":"8","intake_name":"1+0+1"},{"id":"7","intake_name":"1+1+1"}],"medicine_list":[{"id":"2","medecine_name":"Paracetamol","salt_name":"Salt1"},{"id":"2","medecine_name":"Paracetamol","salt_name":"Salt1"},{"id":"1","medecine_name":"Calpol","salt_name":"MASDH"}],"diagnosis_list":[{"diagnosis_id":"3","diagnosis":"test"}]}
     */

    private Object error;
    private boolean status;
    private String status_code;
    private boolean authfailure;
    private String message;
    private DataBean data;

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public boolean isAuthfailure() {
        return authfailure;
    }

    public void setAuthfailure(boolean authfailure) {
        this.authfailure = authfailure;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<TestListBean> test_list;
        private List<IntakeListBean> intake_list;
        private List<MedicineListBean> medicine_list;
        private List<DiagnosisListBean> diagnosis_list;

        public List<TestListBean> getTest_list() {
            return test_list;
        }

        public void setTest_list(List<TestListBean> test_list) {
            this.test_list = test_list;
        }

        public List<IntakeListBean> getIntake_list() {
            return intake_list;
        }

        public void setIntake_list(List<IntakeListBean> intake_list) {
            this.intake_list = intake_list;
        }

        public List<MedicineListBean> getMedicine_list() {
            return medicine_list;
        }

        public void setMedicine_list(List<MedicineListBean> medicine_list) {
            this.medicine_list = medicine_list;
        }

        public List<DiagnosisListBean> getDiagnosis_list() {
            return diagnosis_list;
        }

        public void setDiagnosis_list(List<DiagnosisListBean> diagnosis_list) {
            this.diagnosis_list = diagnosis_list;
        }

        public static class TestListBean {
            /**
             * id : 5
             * test_name : Blood Test
             */

            private String id;
            private String test_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTest_name() {
                return test_name;
            }

            public void setTest_name(String test_name) {
                this.test_name = test_name;
            }
        }

        public static class IntakeListBean {
            /**
             * id : 10
             * intake_name : 1+0+0
             */

            private String id;
            private String intake_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIntake_name() {
                return intake_name;
            }

            public void setIntake_name(String intake_name) {
                this.intake_name = intake_name;
            }
        }

        public static class MedicineListBean {
            /**
             * id : 2
             * medecine_name : Paracetamol
             * salt_name : Salt1
             */

            private String id;
            private String medecine_name;
            private String salt_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMedecine_name() {
                return medecine_name;
            }

            public void setMedecine_name(String medecine_name) {
                this.medecine_name = medecine_name;
            }

            public String getSalt_name() {
                return salt_name;
            }

            public void setSalt_name(String salt_name) {
                this.salt_name = salt_name;
            }
        }

        public static class DiagnosisListBean {
            /**
             * diagnosis_id : 3
             * diagnosis : test
             */

            private String diagnosis_id;
            private String diagnosis;

            public String getDiagnosis_id() {
                return diagnosis_id;
            }

            public void setDiagnosis_id(String diagnosis_id) {
                this.diagnosis_id = diagnosis_id;
            }

            public String getDiagnosis() {
                return diagnosis;
            }

            public void setDiagnosis(String diagnosis) {
                this.diagnosis = diagnosis;
            }
        }
    }

//    /**
//     * error : null
//     * status : true
//     * status_code : 200
//     * authfailure : false
//     * message : Data retreived successfully.
//     * data : {"test_list":[{"id":"9","test_name":"jamammal"},{"id":"5","test_name":"Blood Test"},{"id":"1","test_name":"LFT"}],"intake_list":[{"id":"3","intake_name":"AGH"},{"id":"2","intake_name":"SAM"},{"id":"1","intake_name":"Three Time in a day"}],"medicine_list":[{"id":"2","medecine_name":"Paraceitamol"},{"id":"1","medecine_name":"Coplol"}]}
//     */
//
//    private Object error;
//    private boolean status;
//    private String status_code;
//    private boolean authfailure;
//    private String message;
//    private DataBean data;
//
//    public Object getError() {
//        return error;
//    }
//
//    public void setError(Object error) {
//        this.error = error;
//    }
//
//    public boolean isStatus() {
//        return status;
//    }
//
//    public void setStatus(boolean status) {
//        this.status = status;
//    }
//
//    public String getStatus_code() {
//        return status_code;
//    }
//
//    public void setStatus_code(String status_code) {
//        this.status_code = status_code;
//    }
//
//    public boolean isAuthfailure() {
//        return authfailure;
//    }
//
//    public void setAuthfailure(boolean authfailure) {
//        this.authfailure = authfailure;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public DataBean getData() {
//        return data;
//    }
//
//    public void setData(DataBean data) {
//        this.data = data;
//    }
//
//    public static class DataBean {
//        private List<TestListBean> test_list;
//        private List<IntakeListBean> intake_list;
//        private List<MedicineListBean> medicine_list;
//
//        public List<TestListBean> getTest_list() {
//            return test_list;
//        }
//
//        public void setTest_list(List<TestListBean> test_list) {
//            this.test_list = test_list;
//        }
//
//        public List<IntakeListBean> getIntake_list() {
//            return intake_list;
//        }
//
//        public void setIntake_list(List<IntakeListBean> intake_list) {
//            this.intake_list = intake_list;
//        }
//
//        public List<MedicineListBean> getMedicine_list() {
//            return medicine_list;
//        }
//
//        public void setMedicine_list(List<MedicineListBean> medicine_list) {
//            this.medicine_list = medicine_list;
//        }
//
//        public static class TestListBean {
//            /**
//             * id : 9
//             * test_name : jamammal
//             */
//
//            private String id;
//            private String test_name;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTest_name() {
//                return test_name;
//            }
//
//            public void setTest_name(String test_name) {
//                this.test_name = test_name;
//            }
//        }
//
//        public static class IntakeListBean {
//            /**
//             * id : 3
//             * intake_name : AGH
//             */
//
//            private String id;
//            private String intake_name;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getIntake_name() {
//                return intake_name;
//            }
//
//            public void setIntake_name(String intake_name) {
//                this.intake_name = intake_name;
//            }
//        }
//
//        public static class MedicineListBean {
//            /**
//             * id : 2
//             * medecine_name : Paraceitamol
//             */
//
//            private String id;
//            private String medecine_name;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getMedecine_name() {
//                return medecine_name;
//            }
//
//            public void setMedecine_name(String medecine_name) {
//                this.medecine_name = medecine_name;
//            }
//        }
//    }
}
