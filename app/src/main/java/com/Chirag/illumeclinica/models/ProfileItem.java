package com.Chirag.illumeclinica.models;


public class ProfileItem {
    private String doc_name;
    private String doc_education;
    private String doc_experience;
    private String doc_descrition;
    private String doc_favour;

    public ProfileItem(String doc_name, String doc_education, String doc_experience, String doc_descrition, String doc_favour) {
        this.doc_name = doc_name;
        this.doc_education = doc_education;
        this.doc_experience = doc_experience;
        this.doc_descrition = doc_descrition;
        this.doc_favour = doc_favour;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getDoc_education() {
        return doc_education;
    }

    public void setDoc_education(String doc_education) {
        this.doc_education = doc_education;
    }

    public String getDoc_experience() {
        return doc_experience;
    }

    public void setDoc_experience(String doc_experience) {
        this.doc_experience = doc_experience;
    }

    public String getDoc_descrition() {
        return doc_descrition;
    }

    public void setDoc_descrition(String doc_descrition) {
        this.doc_descrition = doc_descrition;
    }

    public String getDoc_favour() {
        return doc_favour;
    }

    public void setDoc_favour(String doc_favour) {
        this.doc_favour = doc_favour;
    }
}

