
package com.Chirag.illumeclinica.models;

import java.util.List;

public class VisitDetails {
    /**
     * error : null
     * status : true
     * status_code : 200
     * authfailure : false
     * message : Data retreived successfully.
     * data : {"visit_list":[{"id":"169","patient_id":"145","gender":"Male","age":"11","problem_name":"Liver Problems","patient_name":"122","main_doctor":null,"symptom":"infection","visit_status":"2","visitcreated_date_time":"2019-04-17 23:55:25","clinic_hospital_name":null}],"image_list":[{"image_path":"http://admin.illumeclinics.com/illumeclinics/upload/5cb76f96358393946.jpg"},{"image_path":"http://admin.illumeclinics.com/illumeclinics/upload/5cb76f967eb363447.jpg"},{"image_path":"http://admin.illumeclinics.com/illumeclinics/upload/5cb76f97357779069.jpg"}],"test_list":[{"test_name":"LFT"},{"test_name":"LFT"},{"test_name":"LFT"},{"test_name":null},{"test_name":null},{"test_name":null},{"test_name":null},{"test_name":null},{"test_name":null}],"medicine_list":[{"medecine_name":"Coplol","duration":"1","intake_name":"Three Time in a day"},{"medecine_name":"Coplol","duration":"1","intake_name":"Three Time in a day"},{"medecine_name":"Paraceitamol","duration":"2","intake_name":"SAM"},{"medecine_name":"Paraceitamol","duration":"2","intake_name":"SAM"},{"medecine_name":"Paraceitamol","duration":"4","intake_name":"AGH"},{"medecine_name":"Paraceitamol","duration":"4","intake_name":"AGH"}]}
     */

    private Object error;
    private boolean status;
    private String status_code;
    private boolean authfailure;
    private String message;
    private DataBean data;

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public boolean isAuthfailure() {
        return authfailure;
    }

    public void setAuthfailure(boolean authfailure) {
        this.authfailure = authfailure;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<VisitListBean> visit_list;
        private List<ImageListBean> image_list;
        private List<TestListBean> test_list;
        private List<MedicineListBean> medicine_list;

        public List<VisitListBean> getVisit_list() {
            return visit_list;
        }

        public void setVisit_list(List<VisitListBean> visit_list) {
            this.visit_list = visit_list;
        }

        public List<ImageListBean> getImage_list() {
            return image_list;
        }

        public void setImage_list(List<ImageListBean> image_list) {
            this.image_list = image_list;
        }

        public List<TestListBean> getTest_list() {
            return test_list;
        }

        public void setTest_list(List<TestListBean> test_list) {
            this.test_list = test_list;
        }

        public List<MedicineListBean> getMedicine_list() {
            return medicine_list;
        }

        public void setMedicine_list(List<MedicineListBean> medicine_list) {
            this.medicine_list = medicine_list;
        }

        public static class VisitListBean {
            /**
             * id : 169
             * patient_id : 145
             * gender : Male
             * age : 11
             * problem_name : Liver Problems
             * patient_name : 122
             * main_doctor : null
             * symptom : infection
             * visit_status : 2
             * visitcreated_date_time : 2019-04-17 23:55:25
             * clinic_hospital_name : null
             */

            private String id;
            private String patient_id;
            private String gender;
            private String age;
            private String problem_name;
            private String patient_name;
            private Object main_doctor;
            private String symptom;
            private String visit_status;
            private String visitcreated_date_time;
            private Object clinic_hospital_name;
            private String allergy;
            private String diagnosis;
            private String mobile_no;
            private String consultant_doctor_name;

            public String getSuffering_from() {
                return suffering_from;
            }

            public void setSuffering_from(String suffering_from) {
                this.suffering_from = suffering_from;
            }

            private String suffering_from;

            public String getMobile_no() {
                return mobile_no;
            }

            public void setMobile_no(String mobile_no) {
                this.mobile_no = mobile_no;
            }

            public String getConsultant_doctor_name() {
                return consultant_doctor_name;
            }

            public void setConsultant_doctor_name(String consultant_doctor_name) {
                this.consultant_doctor_name = consultant_doctor_name;
            }

            public String getDiagnosis() {
                return diagnosis;
            }

            public void setDiagnosis(String diagnosis) {
                this.diagnosis = diagnosis;
            }

            public String getAllergy() {
                return allergy;
            }

            public void setAllergy(String allergy) {
                this.allergy = allergy;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPatient_id() {
                return patient_id;
            }

            public void setPatient_id(String patient_id) {
                this.patient_id = patient_id;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getProblem_name() {
                return problem_name;
            }

            public void setProblem_name(String problem_name) {
                this.problem_name = problem_name;
            }

            public String getPatient_name() {
                return patient_name;
            }

            public void setPatient_name(String patient_name) {
                this.patient_name = patient_name;
            }

            public Object getMain_doctor() {
                return main_doctor;
            }

            public void setMain_doctor(Object main_doctor) {
                this.main_doctor = main_doctor;
            }

            public String getSymptom() {
                return symptom;
            }

            public void setSymptom(String symptom) {
                this.symptom = symptom;
            }

            public String getVisit_status() {
                return visit_status;
            }

            public void setVisit_status(String visit_status) {
                this.visit_status = visit_status;
            }

            public String getVisitcreated_date_time() {
                return visitcreated_date_time;
            }

            public void setVisitcreated_date_time(String visitcreated_date_time) {
                this.visitcreated_date_time = visitcreated_date_time;
            }

            public Object getClinic_hospital_name() {
                return clinic_hospital_name;
            }

            public void setClinic_hospital_name(Object clinic_hospital_name) {
                this.clinic_hospital_name = clinic_hospital_name;
            }
        }

        public static class ImageListBean {
            /**
             * image_path : http://admin.illumeclinics.com/illumeclinics/upload/5cb76f96358393946.jpg
             */

            private String image_path;

            public String getImage_path() {
                return image_path;
            }

            public void setImage_path(String image_path) {
                this.image_path = image_path;
            }
        }

        public static class TestListBean {
            /**
             * test_name : LFT
             */

            private String test_name;

            public String getTest_name() {
                return test_name;
            }

            public void setTest_name(String test_name) {
                this.test_name = test_name;
            }
        }

        public static class MedicineListBean {
            /**
             * medecine_name : Coplol
             * duration : 1
             * intake_name : Three Time in a day
             */

            private String medecine_name;
            private String duration;
            private String intake_name;

            public String getMedecine_name() {
                return medecine_name;
            }

            public void setMedecine_name(String medecine_name) {
                this.medecine_name = medecine_name;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            public String getIntake_name() {
                return intake_name;
            }

            public void setIntake_name(String intake_name) {
                this.intake_name = intake_name;
            }
        }
    }
//    /**
//     * error : null
//     * status : true
//     * status_code : 200
//     * authfailure : false
//     * message : Data retreived successfully.
//     * data : {"visit_list":[{"patient_id":"46","gender":"male","age":"23","problem_name":"Liver infection","patient_name":"qaa","main_doctor":"Mohit","symptom":"[]","visit_status":"3","visitcreated_date_time":"2019-03-31 13:28:01"}],"image_list":[],"test_list":[{"test_name":"LFT"},{"test_name":"LFT"},{"test_name":"LFT"},{"test_name":null},{"test_name":null},{"test_name":null},{"test_name":null},{"test_name":null},{"test_name":null}],"medicine_list":[{"medecine_name":"Coplol","duration":"1","intake_name":"Three Time in a day"},{"medecine_name":"Coplol","duration":"1","intake_name":"Three Time in a day"},{"medecine_name":"Paraceitamol","duration":"2","intake_name":"SAM"},{"medecine_name":"Paraceitamol","duration":"2","intake_name":"SAM"},{"medecine_name":"Paraceitamol","duration":"4","intake_name":"AGH"},{"medecine_name":"Paraceitamol","duration":"4","intake_name":"AGH"}]}
//     */
//
//    private Object error;
//    private boolean status;
//    private String status_code;
//    private boolean authfailure;
//    private String message;
//    private DataBean data;
//
//    public Object getError() {
//        return error;
//    }
//
//    public void setError(Object error) {
//        this.error = error;
//    }
//
//    public boolean isStatus() {
//        return status;
//    }
//
//    public void setStatus(boolean status) {
//        this.status = status;
//    }
//
//    public String getStatus_code() {
//        return status_code;
//    }
//
//    public void setStatus_code(String status_code) {
//        this.status_code = status_code;
//    }
//
//    public boolean isAuthfailure() {
//        return authfailure;
//    }
//
//    public void setAuthfailure(boolean authfailure) {
//        this.authfailure = authfailure;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public DataBean getData() {
//        return data;
//    }
//
//    public void setData(DataBean data) {
//        this.data = data;
//    }
//
//    public static class DataBean {
//        private List<VisitListBean> visit_list;
//        private List<String> image_list;
//        private List<TestListBean> test_list;
//        private List<MedicineListBean> medicine_list;
//
//        public List<VisitListBean> getVisit_list() {
//            return visit_list;
//        }
//
//        public void setVisit_list(List<VisitListBean> visit_list) {
//            this.visit_list = visit_list;
//        }
//
//        public List<String> getImage_list() {
//            return image_list;
//        }
//
//        public void setImage_list(List<String> image_list) {
//            this.image_list = image_list;
//        }
//
//        public List<TestListBean> getTest_list() {
//            return test_list;
//        }
//
//        public void setTest_list(List<TestListBean> test_list) {
//            this.test_list = test_list;
//        }
//
//        public List<MedicineListBean> getMedicine_list() {
//            return medicine_list;
//        }
//
//        public void setMedicine_list(List<MedicineListBean> medicine_list) {
//            this.medicine_list = medicine_list;
//        }
//
//        public static class VisitListBean {
//            /**
//             * patient_id : 46
//             * gender : male
//             * age : 23
//             * problem_name : Liver infection
//             * patient_name : qaa
//             * main_doctor : Mohit
//             * symptom : []
//             * visit_status : 3
//             * visitcreated_date_time : 2019-03-31 13:28:01
//             */
//
//            private String patient_id;
//            private String gender;
//            private String age;
//            private String problem_name;
//            private String patient_name;
//            private String main_doctor;
//            private String symptom;
//            private String visit_status;
//            private String visitcreated_date_time;
//
//            public String getPatient_id() {
//                return patient_id;
//            }
//
//            public void setPatient_id(String patient_id) {
//                this.patient_id = patient_id;
//            }
//
//            public String getGender() {
//                return gender;
//            }
//
//            public void setGender(String gender) {
//                this.gender = gender;
//            }
//
//            public String getAge() {
//                return age;
//            }
//
//            public void setAge(String age) {
//                this.age = age;
//            }
//
//            public String getProblem_name() {
//                return problem_name;
//            }
//
//            public void setProblem_name(String problem_name) {
//                this.problem_name = problem_name;
//            }
//
//            public String getPatient_name() {
//                return patient_name;
//            }
//
//            public void setPatient_name(String patient_name) {
//                this.patient_name = patient_name;
//            }
//
//            public String getMain_doctor() {
//                return main_doctor;
//            }
//
//            public void setMain_doctor(String main_doctor) {
//                this.main_doctor = main_doctor;
//            }
//
//            public String getSymptom() {
//                return symptom;
//            }
//
//            public void setSymptom(String symptom) {
//                this.symptom = symptom;
//            }
//
//            public String getVisit_status() {
//                return visit_status;
//            }
//
//            public void setVisit_status(String visit_status) {
//                this.visit_status = visit_status;
//            }
//
//            public String getVisitcreated_date_time() {
//                return visitcreated_date_time;
//            }
//
//            public void setVisitcreated_date_time(String visitcreated_date_time) {
//                this.visitcreated_date_time = visitcreated_date_time;
//            }
//        }
//
//        public static class TestListBean {
//            /**
//             * test_name : LFT
//             */
//
//            private String test_name;
//
//            public String getTest_name() {
//                return test_name;
//            }
//
//            public void setTest_name(String test_name) {
//                this.test_name = test_name;
//            }
//        }
//
//        public static class MedicineListBean {
//            /**
//             * medecine_name : Coplol
//             * duration : 1
//             * intake_name : Three Time in a day
//             */
//
//            private String medecine_name;
//            private String duration;
//            private String intake_name;
//
//            public String getMedecine_name() {
//                return medecine_name;
//            }
//
//            public void setMedecine_name(String medecine_name) {
//                this.medecine_name = medecine_name;
//            }
//
//            public String getDuration() {
//                return duration;
//            }
//
//            public void setDuration(String duration) {
//                this.duration = duration;
//            }
//
//            public String getIntake_name() {
//                return intake_name;
//            }
//
//            public void setIntake_name(String intake_name) {
//                this.intake_name = intake_name;
//            }
//        }
//    }
//
////    @SerializedName("error")
////    @Expose
////    private Object error;
////    @SerializedName("status")
////    @Expose
////    private Boolean status;
////    @SerializedName("status_code")
////    @Expose
////    private String statusCode;
////    @SerializedName("authfailure")
////    @Expose
////    private Boolean authfailure;
////    @SerializedName("message")
////    @Expose
////    private String message;
////    @SerializedName("data")
////    @Expose
////    private VisitDetailsData data;
////
////    public Object getError() {
////        return error;
////    }
////
////    public void setError(Object error) {
////        this.error = error;
////    }
////
////    public Boolean getStatus() {
////        return status;
////    }
////
////    public void setStatus(Boolean status) {
////        this.status = status;
////    }
////
////    public String getStatusCode() {
////        return statusCode;
////    }
////
////    public void setStatusCode(String statusCode) {
////        this.statusCode = statusCode;
////    }
////
////    public Boolean getAuthfailure() {
////        return authfailure;
////    }
////
////    public void setAuthfailure(Boolean authfailure) {
////        this.authfailure = authfailure;
////    }
////
////    public String getMessage() {
////        return message;
////    }
////
////    public void setMessage(String message) {
////        this.message = message;
////    }
////
////    public VisitDetailsData getVisitDetailsData() {
////        return data;
////    }
////
////    public void setVisitDetailsData(VisitDetailsData data) {
////        this.data = data;
////    }
////
////    @Override
////    public String toString() {
////        return "VisitDetails{" +
////                "error=" + error +
////                ", status=" + status +
////                ", statusCode='" + statusCode + '\'' +
////                ", authfailure=" + authfailure +
////                ", message='" + message + '\'' +
////                ", data=" + data +
////                '}';
////    }


}
