
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DashboardDoctorVisitListData {

    @SerializedName("visit_list")
    @Expose
    private List<DoctorVisitList> visitList = null;

    public List<DoctorVisitList> getVisitList() {
        return visitList;
    }

    public void setVisitList(List<DoctorVisitList> visitList) {
        this.visitList = visitList;
    }

    @Override
    public String toString() {
        return "DashboardDoctorVisitListData{" +
                "visitList=" + visitList +
                '}';
    }
}

