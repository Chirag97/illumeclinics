
package com.Chirag.illumeclinica.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VisitDetailsList {

    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("problem_name")
    @Expose
    private String problemName;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("main_doctor")
    @Expose
    private String mainDoctor;
    @SerializedName("symptom")
    @Expose
    private String symptom;
    @SerializedName("visit_status")
    @Expose
    private String visitStatus;
    @SerializedName("visitcreated_date_time")
    @Expose
    private String visitcreatedDateTime;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getMainDoctor() {
        return mainDoctor;
    }

    public void setMainDoctor(String mainDoctor) {
        this.mainDoctor = mainDoctor;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(String visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getVisitcreatedDateTime() {
        return visitcreatedDateTime;
    }

    public void setVisitcreatedDateTime(String visitcreatedDateTime) {
        this.visitcreatedDateTime = visitcreatedDateTime;
    }

    @Override
    public String toString() {
        return "VisitDetailsList{" +
                "patientId='" + patientId + '\'' +
                ", gender='" + gender + '\'' +
                ", age='" + age + '\'' +
                ", problemName='" + problemName + '\'' +
                ", patientName='" + patientName + '\'' +
                ", mainDoctor='" + mainDoctor + '\'' +
                ", symptom='" + symptom + '\'' +
                ", visitStatus='" + visitStatus + '\'' +
                ", visitcreatedDateTime='" + visitcreatedDateTime + '\'' +
                '}';
    }
}
