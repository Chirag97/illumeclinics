
package com.Chirag.illumeclinica.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProblemData {

    @SerializedName("Problems")
    @Expose
    private List<Problem> problems = null;

    public List<Problem> getProblems() {
        return problems;
    }

    public void setProblems(List<Problem> problems) {
        this.problems = problems;
    }

    @Override
    public String toString() {
        return "ProblemData{" +
                "problems=" + problems +
                '}';
    }
}
