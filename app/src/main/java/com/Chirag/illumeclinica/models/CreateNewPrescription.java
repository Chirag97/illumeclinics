package com.Chirag.illumeclinica.models;

public class CreateNewPrescription {
    /**
     * error : null
     * status : true
     * status_code : 200
     * authfailure : false
     * data : {"visit_id":"6","message":"Prescription generated sucessfully."}
     */

    private Object error;
    private boolean status;
    private String status_code;
    private boolean authfailure;
    private DataBean data;


    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public boolean isAuthfailure() {
        return authfailure;
    }

    public void setAuthfailure(boolean authfailure) {
        this.authfailure = authfailure;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * visit_id : 6
         * message : Prescription generated sucessfully.
         */

        private String visit_id;
        private String message;

        public String getVisit_id() {
            return visit_id;
        }

        public void setVisit_id(String visit_id) {
            this.visit_id = visit_id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
