
package com.Chirag.illumeclinica.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OldVisitData {

    @SerializedName("Patient_Details")
    @Expose
    private List<PatientDetail> patientDetails = null;

    public List<PatientDetail> getPatientDetails() {
        return patientDetails;
    }

    public void setPatientDetails(List<PatientDetail> patientDetails) {
        this.patientDetails = patientDetails;
    }

    @Override
    public String toString() {
        return "OldVisitData{" +
                "patientDetails=" + patientDetails +
                '}';
    }
}
