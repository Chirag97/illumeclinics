package com.Chirag.illumeclinica;

import android.content.Context;
import android.graphics.Typeface;

public class ConstantClass {
    public static Typeface getFont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "garamond.ttf");
        return typeface;
    }

    public static Typeface getItalicfont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "garamonditalic.ttf");
        return typeface;
    }

    public static Typeface getBoldfont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "garamondbold.ttf");
        return typeface;
    }

    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
// Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }
        return builder.toString();
    }
}

