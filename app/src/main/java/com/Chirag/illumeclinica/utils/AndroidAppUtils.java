package com.Chirag.illumeclinica.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidAppUtils {

    private static final boolean IS_DEBUGGING = true;
    private static final String TAG = AndroidAppUtils.class.getSimpleName();
    private static final String EMAIL_ID_PATTERN = "^[A-Za-z0-9_\\+]+(\\.[A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static Toast toast;

    /**
     * Show debug Message into logcat
     *
     * @param TAG
     * @param msg
     */
    public static void showLog(String TAG, String msg) {
        if (IS_DEBUGGING) {
            Log.d(TAG, msg);
        }

    }

    /**
     * Show debug Error Message into logcat
     *
     * @param TAG
     * @param msg
     */
    public static void showErrorLog(String TAG, String msg) {
        if (IS_DEBUGGING) {
            Log.e(TAG, msg);
        }

    }

    /**
     * Show Toast
     *
     * @param mActivity
     * @param msg
     */
    public static void showToast(final Activity mActivity, final String msg) {
        if (toast != null) {
            toast.cancel();
        }
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (mActivity != null && msg != null && msg.length() > 0) {
                        mActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (!isAppIsInBackground(mActivity)) {
                                    toast = Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * For Checking App is in background or not
     *
     * @param context
     * @return boolean
     */
    private static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    /**
     * To set full screen layout with no title bar
     *
     * @param activity
     */
    public static void activateFullScreen(Activity activity) {
        AndroidAppUtils.showLog(TAG, "To activate full screen...");
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = activity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    /**
     * Check device have internet connection or not
     *
     * @param activity
     * @return
     */
    public static boolean isOnline(Activity activity) {
        if (activity != null) {
            boolean haveConnectedWifi = false;
            boolean haveConnectedMobile = false;

            ConnectivityManager cm = (ConnectivityManager) activity
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                    if (ni.isConnected()) {
                        haveConnectedWifi = true;
                        showLog(TAG, "WIFI CONNECTION : AVAILABLE");
                    } else {
                        showLog(TAG, "WIFI CONNECTION : NOT AVAILABLE");
                    }
                }
                if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                    if (ni.isConnected()) {
                        haveConnectedMobile = true;
                        showLog(TAG,
                                "MOBILE INTERNET CONNECTION : AVAILABLE");
                    } else {
                        showLog(TAG,
                                "MOBILE INTERNET CONNECTION : NOT AVAILABLE");
                    }
                }
            }
            return haveConnectedWifi || haveConnectedMobile;
        } else {
            return false;
        }
    }

    /**
     * Validate Email Id of user
     *
     * @param emailID
     * @return
     */
    public static boolean isEmailIDValidate(String emailID) {
        Pattern pat = Pattern.compile(EMAIL_ID_PATTERN,
                Pattern.CASE_INSENSITIVE);
        Matcher match = pat.matcher(emailID);
        return match.matches();
    }

    public static String dateTimeFormat(String oldDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = sdf.parse(oldDate);

            SimpleDateFormat sd1f = new SimpleDateFormat("dd MMM, yyyy  hh:mm a");
            return sd1f.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
