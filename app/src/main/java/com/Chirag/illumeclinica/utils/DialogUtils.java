package com.Chirag.illumeclinica.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.Chirag.illumeclinica.ConstantClass;
import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.adapters.DiagnosisListAdapter;
import com.Chirag.illumeclinica.listeners.ClickListenerForDialogBox;
import com.Chirag.illumeclinica.listeners.ItemClickListener;
import com.Chirag.illumeclinica.models.MedicineModel;

import java.util.ArrayList;
import java.util.List;


public class DialogUtils {

    private static int medicinePosition = 0;
    private static Dialog mDialog;
    private static ProgressDialog mProgressDialog;
    private static String TAG = DialogUtils.class.getSimpleName();
    public static Typeface typeface;

    /**
     * Showing progress dialog
     *
     * @param msg
     */
    public static void showProgressDialog(final Activity mActivity,
                                          final String msg, final boolean isCancelable) {
        try {
            if (mActivity != null && mProgressDialog != null
                    && mProgressDialog.isShowing()) {
                try {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            mProgressDialog = null;
            if (mProgressDialog == null && mActivity != null) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
                        mProgressDialog.setMessage(msg);
                        mProgressDialog.setCancelable(isCancelable);
                    }
                });

            }
            if (mActivity != null && mProgressDialog != null
                    && !mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide progress dialog
     */
    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            try {
                mProgressDialog.dismiss();
                mProgressDialog = null;

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        } else {
            AndroidAppUtils.showErrorLog(TAG, "mProgressDialog is null");
        }
    }

    /**
     * show default alert box with two buttons
     */
    public static void showAlertBox(final Activity context, boolean cancelable, final String title, final String message,
                                    final int positiveButton, final int negativeButton, final ClickListenerForDialogBox listener) {
        if (!context.isFinishing()) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            } else {
                builder = new AlertDialog.Builder(context);
            }

            builder.setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete

                            if (listener != null)
                                listener.dialogPositiveButton();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                            if (listener != null)
                                listener.dialogNegativeButton();
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(cancelable)
                    .show();
        }
    }

    public static void showSelectDiagnosisPopUp(final Activity mActivity, final List<MedicineModel.DataBean.TestListBean> testList, final ItemClickListener itemClickListener) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mDialog != null) {
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                }
                final List<MedicineModel.DataBean.TestListBean> tempList = new ArrayList<>();
                mDialog = new Dialog(mActivity);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.setContentView(R.layout.select_diagnosis_popup);
                mDialog.setCancelable(false);
                final RecyclerView diagnosis_recycler_view = mDialog.findViewById(R.id.diagnosis_recycler_view);
                Button cancel = mDialog.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                diagnosis_recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));

                final DiagnosisListAdapter diagnosisAdapter = new DiagnosisListAdapter(mActivity, testList, itemClickListener);
                diagnosis_recycler_view.setAdapter(diagnosisAdapter);

                EditText tv_filter = mDialog.findViewById(R.id.tv_filter);
                tv_filter.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        tempList.clear();
                        for (int j = 0; j < testList.size(); j++) {
                            if (testList.get(j).getTest_name().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                tempList.add(testList.get(j));
                            }
                        }
                        diagnosisAdapter.RefreshList(tempList);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                mDialog.show();
            }
        });
    }

    public static void showSelectMedicinePopUp(final Activity mActivity, final List<MedicineModel.DataBean.MedicineListBean> medicineList, final List<MedicineModel.DataBean.IntakeListBean> intakeList, final ItemClickListener itemClickListener) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mDialog != null) {
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                    }
                }
                mDialog = new Dialog(mActivity);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.setContentView(R.layout.select_medicine_popup);
                mDialog.setCancelable(false);
                final Spinner medicineSpinner = mDialog.findViewById(R.id.medicineSpinner);
                final Spinner intakeSpinner = mDialog.findViewById(R.id.intakeSpinner);
                final Spinner daysSpinner = mDialog.findViewById(R.id.daysSpinner);

                final List<String> medicine = new ArrayList<String>();
                medicine.add("Please Select");
                for (int i = 0; i < medicineList.size(); i++) {
                    medicine.add(medicineList.get(i).getMedecine_name());
                }

                ArrayAdapter<String> medicineArrayAdapter = new ArrayAdapter<String>(
                        mActivity, android.R.layout.simple_dropdown_item_1line, medicine){
                    public View getView(int position, View convertView, ViewGroup parent) {
                        typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                        TextView v = (TextView) super.getView(position, convertView, parent);
                        v.setTypeface(typeface);
                        return v;
                    }

                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        TextView v = (TextView) super.getView(position, convertView, parent);
                        v.setTypeface(typeface);
                        return v;
                    }
                };
                medicineArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                medicineSpinner.setAdapter(medicineArrayAdapter);

                medicineSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position != 0) {
                            medicinePosition = position;
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                final List<String> intake = new ArrayList<String>();
                intake.add("Please Select");
                for (int i = 0; i < intakeList.size(); i++) {
                    intake.add(intakeList.get(i).getIntake_name());
                }

                ArrayAdapter<String> intakeArrayAdapter = new ArrayAdapter<String>(
                        mActivity, android.R.layout.simple_dropdown_item_1line, intake){
                    public View getView(int position, View convertView, ViewGroup parent) {
                        typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                        TextView v = (TextView) super.getView(position, convertView, parent);
                        v.setTypeface(typeface);
                        return v;
                    }

                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        TextView v = (TextView) super.getView(position, convertView, parent);
                        v.setTypeface(typeface);
                        return v;
                    }
                };
                intakeArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

                intakeSpinner.setAdapter(intakeArrayAdapter);

                final List age = new ArrayList<Integer>();
                age.add("Please Select");
                for (int i = 1; i <= 30; i++) {
                    if (i == 1) {
                        age.add(Integer.toString(i) + " Day");
                    } else
                        age.add(Integer.toString(i) + " Days");
                }
                ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(
                        mActivity, android.R.layout.simple_dropdown_item_1line, age){
                    public View getView(int position, View convertView, ViewGroup parent) {
                        typeface = Typeface.createFromAsset(getContext().getAssets(), "garamond.ttf");
                        TextView v = (TextView) super.getView(position, convertView, parent);
                        v.setTypeface(typeface);
                        return v;
                    }

                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        TextView v = (TextView) super.getView(position, convertView, parent);
                        v.setTypeface(typeface);
                        return v;
                    }

                };
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

                daysSpinner.setAdapter(spinnerArrayAdapter);

                Button cancel = mDialog.findViewById(R.id.cancel);
                Button ok = mDialog.findViewById(R.id.ok);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (medicineSpinner.getSelectedItemPosition() != 0 && intakeSpinner.getSelectedItemPosition() != 0 && daysSpinner.getSelectedItemPosition() != 0) {
                            mDialog.dismiss();
                            if (itemClickListener != null)
                                itemClickListener.itemMedicineClickListener(medicineSpinner.getSelectedItemPosition() - 1, intakeSpinner.getSelectedItemPosition() - 1, daysSpinner.getSelectedItemPosition());
                        } else {
                            AndroidAppUtils.showToast(mActivity, "Please select the required fields.");
                        }
                    }
                });
                mDialog.show();
            }
        });
    }

    public static void dismissDialog() {
        mDialog.dismiss();
    }
}
