package com.Chirag.illumeclinica.Prefrences;

public interface PreferenceHelper {
    // Shared Preferences file name
    String PREFERENCE_NAME = "ILLUME_CLINIC";
    // Shared Preferences mode
    int PRIVATE_MODE = 0;

    String SESSION_TOKEN = "session_token";
    String USER_ID = "user_id";
    String USER_TYPE = "user_type";
    String DOCTOR_TYPE = "doctor_type";
    String USER_NAME = "user_name";
    String LOGIN_STATUS = "login_status";
    String CONSULTANT_DOCTOR_NAME = "consultant_doctor_name";
    String CONSULTANTDOCTOR_NAME = "doctor_name";
    String CONSULTANT_DOCTOR_NUMBER = "consultant_doctor_number";

}
