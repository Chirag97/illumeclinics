package com.Chirag.illumeclinica.Prefrences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {
    // make private static instance of Session manager class
    private static PreferenceManager preferenceManager;
    // Shared Preferences
    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;
    // Context
    private Context mContext;

    // Constructor
    @SuppressLint("CommitPrefEdits")
    private PreferenceManager(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PreferenceHelper.PREFERENCE_NAME,
                PreferenceHelper.PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * getInstance method is used to initialize PreferenceManager singelton
     * instance
     *
     * @param context context instance
     * @return Singelton session manager instance
     */
    public static PreferenceManager getInstance(Context context) {
        if (preferenceManager == null) {
            preferenceManager = new PreferenceManager(context);
        }
        return preferenceManager;
    }

    /**
     * get session token from preferences
     *
     * @return
     */
    public String getSessionToken() {
        return pref.getString(PreferenceHelper.SESSION_TOKEN, "");
    }

    /**
     * set session token
     *
     * @param token
     */
    public void setSessionToken(String token) {
        editor.putString(PreferenceHelper.SESSION_TOKEN, token);
        editor.commit();
    }

    /**
     * get user id from preferences
     *
     * @return
     */
    public String getUserID() {
        return pref.getString(PreferenceHelper.USER_ID, "");
    }

    /**
     * set user id
     *
     * @param id
     */
    public void setUserID(String id) {
        editor.putString(PreferenceHelper.USER_ID, id);
        editor.commit();
    }

    /**
     * get user type from preferences
     *
     * @return
     */
    public String getUserType() {
        return pref.getString(PreferenceHelper.USER_TYPE, "");
    }

    /**
     * set user type
     *
     * @param userType
     */
    public void setUserType(String userType) {
        editor.putString(PreferenceHelper.USER_TYPE, userType);
        editor.commit();
    }

    public String getUsername() {
        return pref.getString(PreferenceHelper.USER_NAME, "");
    }

    /**
     * set user type
     *
     * @param userType
     */
    public void setDoctorType(String userType) {
        editor.putString(PreferenceHelper.DOCTOR_TYPE, userType);
        editor.commit();
    }

    public String getDoctorType() {
        return pref.getString(PreferenceHelper.DOCTOR_TYPE, "");
    }

    /**
     * set user name
     *
     * @param name
     */
    public void setUsername(String name) {
        editor.putString(PreferenceHelper.USER_NAME, name);
        editor.commit();
    }




    /**
     * get user name from preferences
     *
     * @return
     */
    public String getUserName() {
        return pref.getString(PreferenceHelper.USER_NAME, "");
    }

    /**
     * set user name
     *
     * @param name
     */
    public void setUserName(String name) {
        editor.putString(PreferenceHelper.USER_NAME, name);
        editor.commit();
    }

    /**
     * get user name from preferences
     *
     * @return
     */
    public boolean getLoginStatus() {
        return pref.getBoolean(PreferenceHelper.LOGIN_STATUS, false);
    }

    /**
     * set user name
     *
     * @param status
     */
    public void setLoginStatus(boolean status) {
        editor.putBoolean(PreferenceHelper.LOGIN_STATUS, status);
        editor.commit();
    }


    public String getConsultantDoctor() {
        return pref.getString(PreferenceHelper.CONSULTANT_DOCTOR_NAME, "");
    }

    /**
     * set consultant doctor
     *
     * @param consultantDoctor
     */
    public void setConsultantDoctor(String consultantDoctor) {
        editor.putString(PreferenceHelper.CONSULTANT_DOCTOR_NAME, consultantDoctor);
        editor.commit();
    }

    /**
     * get user type from preferences
     *
     * @return
     */

    public String getConsultantDoctorNumber() {
        return pref.getString(PreferenceHelper.CONSULTANT_DOCTOR_NUMBER, "");
    }

    /**
     * set consultant doctor
     *
     * @param consultantDoctorNumber
     */
    public void setConsultantDoctorNumber(String consultantDoctorNumber) {
        editor.putString(PreferenceHelper.CONSULTANT_DOCTOR_NUMBER, consultantDoctorNumber);
        editor.commit();
    }

     /**
     * get user type from preferences
     *
     * @return
     */



     public String getConsultantDoctorName() {
         return pref.getString(PreferenceHelper.CONSULTANTDOCTOR_NAME, "");
     }

    /**
     * set consultant doctor
     *
     * @param consultantDoctorN
     */
    public void setConsultantDoctorName(String consultantDoctorN) {
        editor.putString(PreferenceHelper.CONSULTANTDOCTOR_NAME, consultantDoctorN);
        editor.commit();
    }

    /**
     * get user type from preferences
     *
     * @return
     */



    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

}
