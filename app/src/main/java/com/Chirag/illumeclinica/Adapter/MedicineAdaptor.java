package com.Chirag.illumeclinica.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.Tessting.CustomTextView;
import com.Chirag.illumeclinica.activities.VisitDetailsActivity;
import com.Chirag.illumeclinica.listeners.ItemRemoveClickListener;
import com.Chirag.illumeclinica.models.MedicineModel;
import com.Chirag.illumeclinica.models.MedicinesIds;

import java.util.List;

public class MedicineAdaptor extends RecyclerView.Adapter<MedicineAdaptor.ViewHolder> {

    private Activity context;
    private ItemRemoveClickListener itemRemoveClickListener;
    private List<MedicinesIds> medicinesIds;
    private List<MedicineModel.DataBean.IntakeListBean> intakeList;
    private List<MedicineModel.DataBean.MedicineListBean> medicineList;

    public MedicineAdaptor(Activity context, ItemRemoveClickListener itemRemoveClickListener, List<MedicineModel.DataBean.IntakeListBean> intakeList, List<MedicineModel.DataBean.MedicineListBean> medicineList) {
        this.context = context;
        this.itemRemoveClickListener = itemRemoveClickListener;
        this.intakeList = intakeList;
        this.medicineList = medicineList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.medicine_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        MedicinesIds medicineIds = medicinesIds.get(position);
        for (int i = 0; i < intakeList.size(); i++) {
            if (medicineIds.getIntake_id().equals(intakeList.get(i).getId())) {
                viewHolder.intakeTime.setText(intakeList.get(i).getIntake_name());
                break;
            }
        }
        for (int i = 0; i < medicineList.size(); i++) {
            if (medicineIds.getMedicine_id().equals(medicineList.get(i).getId())) {
                viewHolder.medicineName.setText(medicineList.get(i).getMedecine_name());
                break;
            }
        }
        if (medicineIds.getDuration().equals("1"))
            viewHolder.days.setText(medicineIds.getDuration()+" Day");
        else viewHolder.days.setText(medicineIds.getDuration()+" Days");
    }

    public void refreshList(List<MedicinesIds> medicinesIds) {
        this.medicinesIds = medicinesIds;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (medicinesIds != null && medicinesIds.size() > 0)
            return medicinesIds.size();
        else return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView deletion;
        CustomTextView medicineName, intakeTime, days;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            medicineName = itemView.findViewById(R.id.medicineName);
            intakeTime = itemView.findViewById(R.id.intakeTime);
            days = itemView.findViewById(R.id.days);
            deletion = itemView.findViewById(R.id.delete);
            deletion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemRemoveClickListener != null)
                        itemRemoveClickListener.itemRemoveClickListener(getAdapterPosition(), itemView, 1);
                }
            });

        }
    }
}
