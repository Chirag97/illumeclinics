package com.Chirag.illumeclinica.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.Allergy;


import java.util.List;

public class AllergyAdapter extends RecyclerView.Adapter<AllergyAdapter.ViehHolder> {

    private Context context;
    private List<Allergy> list;

    public AllergyAdapter(Context context, List<Allergy> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViehHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.allergy_recycler_data_item, viewGroup, false);
        return new ViehHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViehHolder viehHolder, int i) {
        final Allergy allergy = list.get(i);
        viehHolder.allergyname.setText(allergy.getAllergyName());
        viehHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "allergy name: " + allergy.getAllergyName(), Toast.LENGTH_SHORT).show();
          //      Intent intent = new Intent();
        //        intent.putExtra("allergyname", allergy.getAllergyName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViehHolder extends RecyclerView.ViewHolder {

        TextView allergyname;

        public ViehHolder(@NonNull View itemView) {
            super(itemView);
            allergyname = itemView.findViewById(R.id.allergyitem);
        }
    }
}
