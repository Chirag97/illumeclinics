package com.Chirag.illumeclinica.Adapter;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.activities.VisitDetailsActivity;
import com.Chirag.illumeclinica.listeners.ItemRemoveClickListener;
import com.Chirag.illumeclinica.models.MedicineModel;

import java.util.List;

public class DiagnosisAdapter extends RecyclerView.Adapter<DiagnosisAdapter.ViewHolder> {

    private Activity context;
    private List<MedicineModel.DataBean.TestListBean> testList;
    private ItemRemoveClickListener itemRemoveClickListener;

    public DiagnosisAdapter(Activity context, ItemRemoveClickListener itemRemoveClickListener) {
        this.context = context;
        this.itemRemoveClickListener = itemRemoveClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.diagnose_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        MedicineModel.DataBean.TestListBean listBean = testList.get(i);
        viewHolder.diagnoseName.setText(listBean.getTest_name());
    }

    @Override
    public int getItemCount() {
        if (testList != null && testList.size() > 0)
            return testList.size();
        else return 0;
    }

    public void refreshList(List<MedicineModel.DataBean.TestListBean> testList) {
        this.testList = testList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView diagnoseName;
        ImageView delete;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            diagnoseName = itemView.findViewById(R.id.diagnoseName);
            delete = itemView.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemRemoveClickListener!=null)
                        itemRemoveClickListener.itemRemoveClickListener(getAdapterPosition(),itemView,0);
                }
            });


        }
    }


}


