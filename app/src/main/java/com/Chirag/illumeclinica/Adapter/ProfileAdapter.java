package com.Chirag.illumeclinica.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Chirag.illumeclinica.R;
import com.Chirag.illumeclinica.models.ProfileItem;

import java.util.List;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    private Context context;
    private List<ProfileItem> list;

    public ProfileAdapter(Context context, List<ProfileItem> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.activity_profile_data, viewGroup,false);
        return new ProfileAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
       final ProfileItem profileItem= list.get(i);

        viewHolder.docname.setText(profileItem.getDoc_name());
        viewHolder.doceducation.setText(profileItem.getDoc_education());
        viewHolder.docexperience.setText( profileItem.getDoc_experience());
        viewHolder.docfavour.setText(profileItem.getDoc_favour());
        viewHolder.docdescription.setText(profileItem.getDoc_descrition());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        TextView docname, docexperience,doceducation, docfavour, docdescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            docname=itemView.findViewById(R.id.doctorname);
            docexperience=itemView.findViewById(R.id.experience);
            doceducation=itemView.findViewById(R.id.education);
            docfavour=itemView.findViewById(R.id.favour);
            docdescription=itemView.findViewById(R.id.description);


        }
    }
}


