package com.Chirag.illumeclinica;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.Chirag.illumeclinica.Networking.LoginApi;
import com.Chirag.illumeclinica.Networking.Utils;
import com.Chirag.illumeclinica.Ui.AddVisitsActivity;
import com.Chirag.illumeclinica.models.LoginData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    Button signin;
    TextView toolview;
    EditText UserID, UserPass;
    LoginApi loginApi;

    ProgressBar progressBar;

    String id;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

    }

    void init() {
        signin = findViewById(R.id.signinbutton);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//               gotoAddVisits();
//                finish();
                login();
            }
        });
        UserID = findViewById(R.id.userid);
        UserPass = findViewById(R.id.userpassword);
        progressBar = findViewById(R.id.progress);
        toolview = findViewById(R.id.textview);
        toolview.setText("Sign In");
    }

    private void login() {
        id = UserID.getText().toString().trim();
        password = UserPass.getText().toString();
        if (id.isEmpty()) {
            UserID.setError("Enter id first");
            UserID.requestFocus();
            return;
        }
        if (password.length() < 3) {
            UserPass.setError("Password lenght is 3");
            UserPass.requestFocus();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        loginApi = Utils.getLoginAPI();


        Call<LoginData> data = loginApi.login(id, password);
        data.enqueue(new Callback<LoginData>() {
            private static final String TAG = "illume";

            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);

//                    Toast.makeText(getApplicationContext(), "Successful", Toast.LENGTH_SHORT).show();
//                    String userid = response.body().getData().getUserId();
//                    Log.d(TAG, "Response " + userid);


                    //Sending User_ID to ADD visit Activity
                    Intent intent = new Intent(LoginActivity.this, AddVisitsActivity.class);
//                    intent.putExtra("userid", userid);
                    startActivity(intent);
                } else {
                    Toast.makeText(LoginActivity.this, "Please check your details", Toast.LENGTH_SHORT).show();
                }
            }

            @Override

            public void onFailure(Call<LoginData> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Error Occured" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
//        Call<ResponseBody> call = RetrofitConnectivity.getInstance().getApi().login(id,password,userType);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                try {
//                    if(response.code()==200) {
//                        String s = response.body().string();
//                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
//                    }else{
//                        String s = response.body().string();
//                        Toast.makeText(getApplicationContext(),"Error Occured",Toast.LENGTH_SHORT).show();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(getApplicationContext(),"Error!   "+t.getMessage(),Toast.LENGTH_SHORT).show();
//            }
//        });
    }
}
